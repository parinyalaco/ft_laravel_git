<?php

use App\Models\StampDateFormats;
use Illuminate\Database\Seeder;

class StampDateFormatSeeder extends Seeder
{

    public function run()
    {
        $data = [
            [
                'name' => "yy.mm.dd",
                'stamp_date_format' => "y.m.d",
            ],
            [
                'name' => "yyyy.mm.dd",
                'stamp_date_format' => "Y.m.d",
            ],
            [
                'name' => "yy.dd.mm",
                'stamp_date_format' => "y.d.m",
            ],
            [
                'name' => "yyyy.dd.mm",
                'stamp_date_format' => "Y.d.m",
            ],
            [
                'name' => "mm.dd.yy",
                'stamp_date_format' => "m.d.y",
            ],
            [
                'name' => "mm.dd.yyyy",
                'stamp_date_format' => "m.d.Y",
            ],
            [
                'name' => "mm.yy.dd",
                'stamp_date_format' => "m.y.d",
            ],
            [
                'name' => "mm.yyyy.dd",
                'stamp_date_format' => "d.Y.d",
            ],
            [
                'name' => "dd.mm.yy",
                'stamp_date_format' => "d.m.y",
            ],
            [
                'name' => "dd.mm.yyyy",
                'stamp_date_format' => "d.m.Y",
            ],
            [
                'name' => "mm.yy",
                'stamp_date_format' => "m.y",
            ],
            [
                'name' => "mm.yyyy",
                'stamp_date_format' => "m.Y",
            ],
            [
                'name' => "yy.mm",
                'stamp_date_format' => "y.m",
            ],
            [
                'name' => "yyyy.mm",
                'stamp_date_format' => "Y.m",
            ],


            [
                'name' => "yy-mm-dd",
                'stamp_date_format' => "y-m-d",
            ],
            [
                'name' => "yyyy-mm-dd",
                'stamp_date_format' => "Y-m-d",
            ],
            [
                'name' => "yy.dd.mm",
                'stamp_date_format' => "y-d-m",
            ],
            [
                'name' => "yyyy.dd.mm",
                'stamp_date_format' => "Y-d-m",
            ],
            [
                'name' => "mm-dd-yy",
                'stamp_date_format' => "m-d-y",
            ],
            [
                'name' => "mm-dd-yyyy",
                'stamp_date_format' => "m-d-Y",
            ],
            [
                'name' => "mm-yy-dd",
                'stamp_date_format' => "m-y-d",
            ],
            [
                'name' => "mm-yyyy-dd",
                'stamp_date_format' => "m-Y-d",
            ],
            [
                'name' => "dd-mm-yy",
                'stamp_date_format' => "d-m-y",
            ],
            [
                'name' => "dd-mm-yyyy",
                'stamp_date_format' => "d-m-Y",
            ],
            [
                'name' => "mm-yy",
                'stamp_date_format' => "m-y",
            ],
            [
                'name' => "mm-yyyy",
                'stamp_date_format' => "m-Y",
            ],
            [
                'name' => "yy-mm",
                'stamp_date_format' => "y-m",
            ],
            [
                'name' => "yyyy-mm",
                'stamp_date_format' => "Y-m",
            ],  
            
        ];

        StampDateFormats::insert($data);
    }
}




