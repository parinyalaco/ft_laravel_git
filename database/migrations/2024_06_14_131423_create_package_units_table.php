<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageUnitsTable extends Migration
{

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('units', function (Blueprint $table) {

            $table->id();
            $table->string('name');
            $table->string('desc');
            $table->timestamps();
        });
    }

 
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
