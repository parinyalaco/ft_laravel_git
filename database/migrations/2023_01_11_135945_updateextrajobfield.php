<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Updateextrajobfield extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'extra_jobs',
            function (Blueprint $table) {
                $table->string('status',50)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('extra_jobs', 'status')) {
            Schema::table('extra_jobs', function (Blueprint $table) {
                $table->dropColumn('status');
            });
        }
    }
}
