<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryPlanTable extends Migration
{
    protected $connection = 'sqlpackagesrv';

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('delivery_plans', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id');
            $table->string('booking_no')->nullable();
            $table->string('orders');
            $table->string('customerPo_no')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('delivery_plans');
    }
}
