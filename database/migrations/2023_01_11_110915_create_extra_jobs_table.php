<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shift_id');
            $table->string('dep', 50);
            $table->string('name');
            $table->string('typegroup', 50);
            $table->text('desc')->nullable();
            $table->integer('default_plan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_jobs');
    }
}
