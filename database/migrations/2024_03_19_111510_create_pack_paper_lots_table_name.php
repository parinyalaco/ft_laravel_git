<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackPaperLotsTableName extends Migration
{

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('pack_paper_lots', function (Blueprint $table) {
            $table->id();
            $table->integer('pack_paper_id');
            $table->integer('packaging_id');
            $table->integer('quantity_boxes')->nullable();
            $table->integer('quantity_bags')->nullable();
            $table->string('sequence_boxes')->nullable();

            $table->float('weight_per_piece')->nullable();
            $table->float('weight_per_full')->nullable();
            $table->float('number_of_pallets')->nullable();
            $table->float('last_pallet_boxes')->nullable();
            
            $table->string('mfg_date')->nullable();

            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('pack_paper_lots');
    }
}
