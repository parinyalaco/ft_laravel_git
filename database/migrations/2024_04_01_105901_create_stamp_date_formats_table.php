<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampDateFormatsTable extends Migration
{
    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('stamp_date_formats', function (Blueprint $table) {
            $table->id();
            $table->string('stamp_date_format')->nullable();
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stamp_date_formats');
    }
}
