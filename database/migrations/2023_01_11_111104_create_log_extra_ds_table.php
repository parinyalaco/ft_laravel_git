<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogExtraDsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_extra_ds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('log_extra_m_id');
            $table->integer('extra_job_id');
            $table->integer('plan_num');
            $table->integer('act_num');
            $table->string('ref_note')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_extra_ds');
    }
}
