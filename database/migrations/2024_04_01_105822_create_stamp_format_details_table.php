<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampFormatDetailsTable extends Migration
{
    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('stamp_format_details', function (Blueprint $table) {
            $table->id();
            $table->integer('stamp_date_format_id')->nullable();
            $table->string('front_text')->nullable();
            $table->string('date_era_format')->nullable();
            $table->string('lot')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('stamp__date_format_details');
    }
}
