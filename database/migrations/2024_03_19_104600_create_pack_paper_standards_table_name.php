<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackPaperStandardsTableName extends Migration
{

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('pack_paper_standards', function (Blueprint $table) {
            $table->id();
            $table->string('exp_month')->nullable();
            $table->string('products_per_lot')->nullable();
            $table->string('weight_with_bag')->nullable();
            $table->string('cable')->nullable();

            $table->integer('pallet_base');
            $table->integer('pallet_low');
            $table->integer('pallet_height');
            $table->string('stamp_format_id')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('pack_paper_standards');
    }
}
