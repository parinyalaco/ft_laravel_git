<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagingPackageImgTableName extends Migration
{

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('packaging_package_imgs', function (Blueprint $table) {
            $table->id();
            $table->string('packaging_package_id');
            $table->string('name')->nullable();
            $table->string('main_img_path')->nullable();
            $table->string('stamp_img_paht')->nullable();
            $table->string('stamp_format')->nullable();
            $table->string('detail')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('packaging_package_imgs');
    }
}
