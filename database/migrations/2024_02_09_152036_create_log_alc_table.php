<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogAlcTable extends Migration
{

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('log_alc', function (Blueprint $table) {
            $table->id();
            $table->dateTime('production_date')->nullable();
            $table->string('onshift')->nullable();
            $table->string('department')->nullable();
            $table->string('note')->nullable();
            $table->string('type')->nullable();
            $table->integer('amount_used')->nullable();
            $table->integer('amount_broked')->nullable();
            $table->string('details')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('log_alc');
    }
}
