<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BreakdownMoreimagePkPack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'log_select_ds',
            function (Blueprint $table) {
                $table->float('breakdown_min')->nullable();
                $table->float('breakdown')->nullable();
                $table->string('img_path1')->nullable();
                $table->string('img_path2')->nullable();
                $table->string('img_path3')->nullable();
                $table->string('img_path4')->nullable();
                $table->string('img_path5')->nullable();
                $table->string('img_path6')->nullable();
                $table->string('img_path7')->nullable();
                $table->string('img_path8')->nullable();
            }
        );

        Schema::table(
            'log_pack_ds',
            function (Blueprint $table) {
                $table->float('breakdown_min')->nullable();
                $table->float('breakdown')->nullable();
                $table->string('img_path1')->nullable();
                $table->string('img_path2')->nullable();
                $table->string('img_path3')->nullable();
                $table->string('img_path4')->nullable();
                $table->string('img_path5')->nullable();
                $table->string('img_path6')->nullable();
                $table->string('img_path7')->nullable();
                $table->string('img_path8')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('log_select_ds', 'breakdown_min')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('breakdown_min');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'breakdown')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('breakdown');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path1')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path1');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path2')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path2');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path3')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path3');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path4')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path4');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path5')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path5');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path6')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path6');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path7')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path7');
            });
        }
        if (Schema::hasColumn('log_select_ds', 'img_path8')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('img_path8');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'breakdown_min')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('breakdown_min');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'breakdown')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('breakdown');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path1')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path1');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path2')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path2');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path3')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path3');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path4')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path4');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path5')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path5');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path6')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path6');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path7')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path7');
            });
        }
        if (Schema::hasColumn('log_pack_ds', 'img_path8')) {
            Schema::table('log_pack_ds', function (Blueprint $table) {
                $table->dropColumn('img_path8');
            });
        }
    }
}
