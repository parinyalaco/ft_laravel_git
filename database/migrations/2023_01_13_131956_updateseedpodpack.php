<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Updateseedpodpack extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'seed_drop_packs',
            function (Blueprint $table) {
                $table->float('input')->nullable();
                $table->float('output')->nullable();
            }
        );

        Schema::table(
            'log_select_ds',
            function (Blueprint $table) {
                $table->float('reselected')->nullable();
                $table->text('reselected_case')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('seed_drop_packs', 'input')) {
            Schema::table('seed_drop_packs', function (Blueprint $table) {
                $table->dropColumn('input');
            });
        }

        if (Schema::hasColumn('seed_drop_packs', 'output')) {
            Schema::table('seed_drop_packs', function (Blueprint $table) {
                $table->dropColumn('output');
            });
        }

        if (Schema::hasColumn('log_select_ds', 'reselected')) {
            Schema::table('log_select_ds', function (Blueprint $table) {
                $table->dropColumn('reselected');
            });
        }

        if (Schema::hasColumn('log_select_ds', 'reselected_case')) {
            Schema::table('seed_drop_packs', function (Blueprint $table) {
                $table->dropColumn('reselected_case');
            });
        }
    }
}
