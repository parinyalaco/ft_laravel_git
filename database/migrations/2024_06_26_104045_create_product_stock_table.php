<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStockTable extends Migration
{
    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('product_stocks', function (Blueprint $table) {

            $table->id();
            $table->integer('product_id');
            $table->integer('seq');
            $table->string('month');
            $table->string('year');
            $table->timestamps();
        });
    }

 
    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('product_stocks');
    }
}
