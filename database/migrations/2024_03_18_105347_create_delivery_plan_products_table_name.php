<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryPlanProductsTableName extends Migration
{

    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('delivery_plan_products', function (Blueprint $table) {
            $table->id();
            $table->string('delivery_plan_id');
            $table->integer('product_id');
            $table->string('product_name');
            $table->string('weight');
            $table->string('quantity');
            $table->string('unit');
            $table->string('remark');
            $table->string('product_stock_id');
            $table->date('loading_date');
            $table->date('packing_date');
            $table->date('production_date');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('delivery_plan_products');
    }
}
