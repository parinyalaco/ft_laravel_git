<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('packages_imgs', function (Blueprint $table) {
            $table->id();
            $table->int('package_id')->nullable();
            $table->string('name')->nullable();
            $table->string('main_img_path')->nullable();
            $table->string('stamp_img_paht')->nullable();
            $table->string('stamp_format')->nullable();
            $table->string('detail')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages_imgs');

    }
}
