<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackPapersTableName extends Migration
{


    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('pack_papers', function (Blueprint $table) {
            $table->id();
            $table->integer('delivery_plan_product_id');
            $table->integer('pack_paper_standard_id');
            $table->date('exp_date')->nullable();
            $table->integer('quantity');
            $table->integer('packaging_id');

            $table->string('status');
            $table->integer('create_by');
            $table->integer('cancel_by');
            $table->integer('remark');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('pack_papers');
    }
}
