<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampFormatsTable extends Migration
{
    public function up()
    {
        Schema::connection('sqlpackagesrv')->create('stamp_formats', function (Blueprint $table) {
            $table->id();
            $table->integer('mfg_stamp_format_detail_id')->nullable();
            $table->integer('exp_stamp_format_detail_id')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('sqlpackagesrv')->dropIfExists('stamp_date_formats');
    }
}
