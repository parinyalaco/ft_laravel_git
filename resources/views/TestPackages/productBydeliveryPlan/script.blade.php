<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    new DataTable('#deliveryPlanTable', {

    });

    $(document).ready(function() {
        $('.first-select2').select2();

        $(".add_input").click(function() {
            var id = $(this).data('id');
            var count = $(".datainputs[data-id='" + id + "'] .required_inp").length;

            var req_input = $(".datainputs[data-id='" + id + "']");
            req_input.append(
                '<div class="required_inp" id="field_' + count + '">' +

                '<label>รูปหลัก</label>' +
                '<input name="packaging_package_main_imgs[]" type="file" accept="image/jpeg, image/png">' +

                '<label>รูป Stamp </label>' +
                '<input name="packaging_package_stamp_imgs[]" type="file" accept="image/jpeg, image/png">' +

                '<label>ตำแหน่ง</label>' +
                '<input type="text" name="name[]" placeholder="ตำแหน่ง" class="form-control">' +

                '<label>Stamp Format</label>' +
                '<input type="text" name="stamp_format[]" placeholder="ตำแหน่ง" class="form-control"> ' +

                '<label>รายละเอียดเพิ่มเติม</label>' +
                '<input type="text" name="detail[]" placeholder="รายละเอียด" class="form-control"> ' +

                '<input type="button" class="inputRemove" value="ลบ" data-target="' + count +
                '"> </div>'
            );
        });

        $('body').on('click', '.inputRemove', function() {
            var targetId = $(this).data('target');
            $(this).parent('div.required_inp').remove();
        });

    });
</script>


<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                // ใช้ data-id เพื่อค้นหา img ที่ต้องการเปลี่ยนแปลง
                var dataId = $(input).data('id');
                $('img[data-id="' + dataId + '"]').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
