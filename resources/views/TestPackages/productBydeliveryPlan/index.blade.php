@extends('TestPackages.layouts.layout')

@push('styles')
    <style>
        .datainputs input {
            font-size: 14px;
            line-height: 21px;
            color: #3D3D3D;
            height: 30px;
            padding: 0px 10px;
            border: 1px solid #ddd;
            box-shadow: 0px 0px 1px #D8D8D8;
            margin: 0px 30px 20px 0px !important;
        }

        .inputRemove {
            display: inline-block;
            color: #3d3d3d;
            text-align: center;
            text-decoration: none;
            width: auto;
            height: 40px;
            line-height: 40px;
            border: 2px solid #3d3d3d;
            padding: 0px 15px;
            border-radius: 5px;
        }

        .inputRemove {
            cursor: pointer;
        }
    </style>
@endpush


@section('content')
    @include('TestPackages.productBydeliveryPlan.addNewProduct')

    <hr>

    <div class="m-2">
        {{--  id="deliveryPlanTable"  --}}
        <table class="table" style="font-size: 14px">
            <thead>
                <tr class="table-info">
                    <th>Product name</th>
                    <th>Product Fac</th>
                    <th>Version</th>
                    <th>Package Name</th>
                    <th style="text-align: center">Inner weight</th>
                    <th style="text-align: center">Number per pack</th>
                    <th style="text-align: center">Outer weight</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
                    <tr>
                        <td>{{ $delivery_plan_product->product->name }}</td>
                        <td>{{ $delivery_plan_product->product->product_fac or 'ไม่ได้ตั้ง' }}</td>


                        @foreach ($delivery_plan_product->product->packagings as $packaging)
                            @if (count($packaging->package) > 0 && $packaging->status == 'Active')
                                <th>{{ $packaging->version }} </th>
                                <td>
                                    @foreach ($packaging->packaging_packages as $packaging_package)
                                        <a href="{{ route('viewPackage', $packaging_package->package->id) }}"
                                            title="ดูรายละเอียด" target="_blank">
                                            {{ $packaging_package->package->name or '' }}</a>
                                        /
                                    @endforeach

                                </td>

                                <td style="text-align: center">
                                    @if (is_numeric($packaging->inner_weight_g))
                                        {{ number_format($packaging->inner_weight_g, 2) }}
                                    @else
                                        {{ $packaging->inner_weight_g }}
                                    @endif

                                </td>
                                <td style="text-align: center">
                                    @if (is_numeric($packaging->number_per_pack))
                                        {{ number_format($packaging->number_per_pack, 2) }}
                                    @else
                                        {{ $packaging->number_per_pack }}
                                    @endif

                                </td>
                                <td style="text-align: center">
                                    @if (is_numeric($packaging->number_per_pack))
                                        {{ number_format($packaging->number_per_pack, 2) }} Kg
                                    @else
                                        {{ $packaging->number_per_pack }} Kg
                                    @endif
                                </td>

                                <td>
                                    @if (isset($delivery_plan_product->pack_paper))
                                        @if ($delivery_plan_product->pack_paper->status == 'Active')
                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                                                data-target="#pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                                                ใบแจ้งบรรจุ เลขที่ {{ $delivery_plan_product->pack_paper->id }}
                                            </button>
                                        @else
                                            <button class="btn btn-danger btn-sm"
                                                onclick="return alert('{{ $delivery_plan_product->pack_paper->remark }}')">ใบแจ้งบรรจุเลขที่
                                                {{ $delivery_plan_product->pack_paper->id }}
                                                ถูกยกเลิก </button>
                                        @endif


                                        <div class="modal" id="pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form
                                                        action="{{ route('test_pack_paper_doc', [$delivery_plan_product->pack_paper->id]) }}"
                                                        method="post" enctype="multipart/form-data">

                                                        <div class="modal-header">
                                                            <h5 class="modal-title">รายละเอียด วันที่ผลิต ของใบแจ้งบรรจุ
                                                            </h5>
                                                            <button type="button" class="close btn-close "
                                                                data-dismiss="modal"></button>
                                                        </div>

                                                        <div class="modal-body">

                                                            {{ csrf_field() }}
                                                            @foreach ($delivery_plan_product->pack_paper->pack_paper_lots as $pack_paper_lot)
                                                                <label>วันที่ผลิตของLot.
                                                                    <strong>{{ $pack_paper_lot->lot }}</strong></label>

                                                                <div class="input-group mb-3">

                                                                    <input type="date"
                                                                        name="mfg_month[{{ $pack_paper_lot->id }}]"
                                                                        class="form-control form-control-sm"
                                                                        value="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}"
                                                                        min="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}">


                                                                    <input type="text"
                                                                        name="remark[{{ $pack_paper_lot->id }}]"
                                                                        value="{{ $pack_paper_lot->remark }}"
                                                                        class="form-control form-control-sm"
                                                                        placeholder="หมายเหตุ">
                                                                </div>
                                                            @endforeach
                                                            <input type="text" name="packPaperRemark"
                                                                class="form-control form-control-sm"
                                                                value="{{ $delivery_plan_product->pack_paper->remark }}">

                                                        </div>

                                                        <div class="modal-footer">

                                                            <button type="button" class="btn btn-danger"
                                                                data-toggle="modal"
                                                                data-target="#cancelModal-{{ $delivery_plan_product->pack_paper->id }}">
                                                                ยกเลิกใบแจ้งบรรจุ
                                                            </button>
                                                            <button type="submit" class="btn btn-success"
                                                                onclick="return confirm('ไปหน้า ใบบรรจุ')">ใบแจ้งบรรจุ
                                                                เลขที่
                                                                {{ $delivery_plan_product->pack_paper->id }}</button>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade"
                                            id="cancelModal-{{ $delivery_plan_product->pack_paper->id }}" tabindex="-1"
                                            aria-labelledby="nestedModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="nestedModalLabel">
                                                            ยกเลิกใบแจ้งบรรจุเลขที่
                                                            {{ $delivery_plan_product->pack_paper->id }}</h5>
                                                        <button type="button" class="close btn" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">X</span>
                                                        </button>
                                                    </div>
                                                    <form
                                                        action="{{ route('test_cancel_pack_paper', [$delivery_plan_product->pack_paper->id]) }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="modal-body">
                                                            <label>รายละเอียด</label>
                                                            <input type="text" name="remark"
                                                                class="form-control form-control-sm">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit"
                                                                class="btn btn-danger my-2">บันทึก</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                            data-target="#makePack_paper_standard-{{ $delivery_plan_product->id }}-{{ $packaging->id }}">
                                            สร้างใบแจ้งบรรจุ
                                        </button>

                                        <div class="modal"
                                            id="makePack_paper_standard-{{ $delivery_plan_product->id }}-{{ $packaging->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form
                                                        action="{{ route('createPackPaper', [$delivery_plan_product->id, $packaging->id]) }}"
                                                        method="post" enctype="multipart/form-data">

                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ตั้งค่า ใบแจ้งบรรจุ </h4>
                                                            <button type="button" class="close btn-close"
                                                                data-dismiss="modal"></button>
                                                        </div>

                                                        <div class="modal-body">

                                                            {{ csrf_field() }}

                                                            <label>จำนวนกล่อง แต่ละ Lot</label>
                                                            <input type="number" name="products_per_lot"
                                                                class="form-control form-control-sm" required>

                                                            <label>สายรัด</label>
                                                            <input type="text" name="cable"
                                                                class="form-control form-control-sm">

                                                            <label>ฐานเรียง</label>
                                                            <input type="number" name="pallet_low"
                                                                class="form-control form-control-sm" required>

                                                            <label>จำนวนชั้น</label>
                                                            <input type="number" name="pallet_height"
                                                                class="form-control form-control-sm" required>

                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">ปิด</button>
                                                            <button type="submit" class="btn btn-primary"
                                                                onclick="return confirm('ไปหน้า ใบบรรจุ')">ใบแจ้งบรรจุ</button>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @include('TestPackages.productBydeliveryPlan.script')
@endsection
