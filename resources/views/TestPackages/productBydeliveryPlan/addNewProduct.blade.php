<div class="m-2">
    <a href="{{ route('testsDeliveryPlan') }}" class="btn btn-primary my-2"> Delivery Lists</a>
    <hr>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif


    <div class="row">
        <div class="col-3 my-2">
            <h5 class="card-title">Delivery Plan Detail</h5>

            <table class="table">
                <tr>
                    <th style="width: 150px;color:white" class="bg-secondary">Customer</th>
                    <td class="bg-light"> {{ $deliveryPlan->customer->name or ' ' }}</td>
                </tr>
                <tr>
                    <th style="width: 150px;color:white" class="bg-secondary">Booking No.</th>
                    <td class="bg-light"> {{ $deliveryPlan->booking_no or '-' }}</td>
                </tr>
                <tr>
                    <th style="width: 150px;color:white" class="bg-secondary">Orders</th>
                    <td class="bg-light"> {{ $deliveryPlan->orders }}</td>
                </tr>
                <tr>
                    <th style="width: 150px;color:white" class="bg-secondary">Customer PO No.</th>
                    <td class="bg-light"> {{ $deliveryPlan->customer_po_no }}</td>
                </tr>

            </table>
            <br>
        </div>
        <div class="col">
            <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseAddNewProduct"
                aria-expanded="false" aria-controls="collapseExample">
                เพิ่ม Product
            </button>

            <div class="collapse mt-2" id="collapseAddNewProduct">
                <div class="card card-body">
                    <form action="{{ route('addTestDeliveryPlan') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="text" value="{{ $deliveryPlan->id }}" hidden name="delivery_plan_id">

                        <div class="row">
                            <div class="col">
                                <label>Product</label><br>
                                <select name="product_id" class=" first-select2" style="width: 100%" name="field[]">
                                    @foreach ($products as $product_id => $product_name)
                                        <option value="{{ $product_id }}">{{ $product_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col">
                                <label>Weight</label>
                                <input type="text" name="weight" class="form-control form-control-sm" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <label>Loading Date</label>
                                <input type="date" name="loading_date" class="form-control form-control-sm" required>
                            </div>
                            <div class="col">
                                <label>วันแจ้งผลิต</label>
                                <input type="date" name="production_date" class="form-control form-control-sm"
                                    required>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col">
                                <br>
                                <button type="submit" class="btn btn-success btn-sm w-100">เพิ่ม Product</button>
                            </div>
                        </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>


