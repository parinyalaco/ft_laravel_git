@extends('TestPackages.layouts.layout')

@section('content')
    @include('TestPackages.top')

    <h5>Packages by Product ของ {{ $product->name }}</h5>

    <table class="table ">
        <thead>
            <tr>
                <th>Pakage</th>
                <th>description </th>
                <th>Size</th>
                <th>สถานะ</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($product->packagings->groupBy('product_id') as $productId => $packagings)
                @foreach ($product->packagings as $packaging)
                    <tr>
                        <th colspan="4" class="bg-secondary text-white">Version :
                            {{ $packaging->version }}
                        </th>
                        <td>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#detailModal-{{ $productId }}-{{ $packaging->version }}">
                                เพิ่ม Package
                            </button>
                        </td>
                    </tr>


                    <div class="modal fade" id="detailModal-{{ $productId }}-{{ $packaging->version }}" tabindex="-1"
                        aria-labelledby="detailModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="detailModalLabel">เพิ่ม Package ของ
                                        {{ $packaging->version }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>

                                <div class="modal-body">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="tabA" data-bs-toggle="tab"
                                                data-bs-target="#contentA-{{ $productId }}-{{ $packaging->version }}"
                                                type="button" role="tab" aria-controls="contentA"
                                                aria-selected="true">เพิ่ม
                                                Package ใหม่</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="tabB" data-bs-toggle="tab"
                                                data-bs-target="#contentB-{{ $productId }}-{{ $packaging->version }}"
                                                type="button" role="tab" aria-controls="contentB"
                                                aria-selected="false">เลือก
                                                package</button>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content mt-2">
                                        <div class="tab-pane fade show active"
                                            id="contentA-{{ $productId }}-{{ $packaging->version }}">
                                            <h5>เพิ่ม Package ใหม่</h5>

                                            <form
                                                action="{{ route('addNewPackage', [$productId, $packaging->id, $packaging->version]) }}"
                                                method="POST">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col">
                                                        <label>Package Type </label>
                                                        <select name="package_type_id" class="form-control">
                                                            @foreach ($package_types as $package_type)
                                                                <option value="{{ $package_type->id }}">
                                                                    {{ $package_type->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col">

                                                        <label>Package Name</label>
                                                        <input type="text" name="package-name" class="form-control">
                                                    </div>
                                                </div>

                                                <label>Package Description</label>
                                                <input type="text" name="desc" class="form-control">

                                                <div class="row">

                                                    <div class="col">
                                                        <label>Package Size</label>
                                                        <input type="text" name="package-size" class="form-control">
                                                    </div>
                                                    <div class="col">
                                                        <label>Stamp Format</label>
                                                        <input type="text" name="stamp_format" class="form-control">

                                                    </div>
                                                </div>



                                                <label>Sap Note</label>
                                                <input type="text" name="sapnote" class="form-control">

                                                <hr>

                                                <div class="row">
                                                    <div class="col">

                                                        <label>Inner Weight g (กรัม)</label>
                                                        <input type="text" name="inner_weight_g" class="form-control">

                                                    </div>
                                                    <div class="col">
                                                        <label>Outer Weight KG(กิโลกรัม)</label>
                                                        <input type="text" name="outer_weight_kg" class="form-control">
                                                    </div>
                                                    <div class="col">
                                                        <label>Number Per Pack</label>
                                                        <input type="text" name="number_per_pack" class="form-control">
                                                    </div>

                                                </div>

                                                <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                                            </form>
                                        </div>

                                        <div class="tab-pane fade"
                                            id="contentB-{{ $productId }}-{{ $packaging->version }}">
                                            <h5>เลือกจาก Package เดิม</h5>
                                            -{{ $packaging->version }}
                                            <form
                                                action="{{ route('addPackagebySelect', [$productId, $packaging->id, $packaging->version]) }}"
                                                method="POST">
                                                {{ csrf_field() }}
                                                <select class="form-select">
                                                    @foreach ($packages as $packagesList)
                                                        <option value="{{ $packagesList->id }}">
                                                            {{ $packagesList->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="row">
                                                    <div class="col">

                                                        <label>Inner Weight g (กรัม)</label>
                                                        <input type="text" name="inner_weight_g" class="form-control">

                                                    </div>
                                                    <div class="col">
                                                        <label>Outer Weight KG(กิโลกรัม)</label>
                                                        <input type="text" name="outer_weight_kg"
                                                            class="form-control">
                                                    </div>
                                                    <div class="col">
                                                        <label>Number Per Pack</label>
                                                        <input type="text" name="number_per_pack"
                                                            class="form-control">
                                                    </div>

                                                </div>
                                                <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    @foreach ($packaging->packaging_packages as $packaging_package)
                        @if ($packaging_package->package)
                            <tr>
                                <td>{{ $packaging_package->package->name }}</td>
                                <td>{{ $packaging_package->package->desc }}</td>
                                <td>{{ $packaging_package->package->size }}</td>
                                @if ($packaging_package->package->status == 'Active')
                                    <td style="color: green"> <i
                                            class='bx bxs-circle'></i><strong>{{ $packaging_package->package->status }}</strong>
                                    </td>
                                @else
                                    <td style="color: red"><i
                                            class='bx bxs-circle'></i><strong>{{ $packaging_package->package->status }}</strong>
                                    </td>
                                @endif
                                <td>
                                    <div class="btn-group">
                                        <button class="btn" data-bs-toggle="modal"
                                            data-bs-target="#changePackage-{{ $packaging_package->package->id }}">
                                            <i class='bx bx-edit-alt bx-tada-hover bx-sm'> </i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="changePackage-{{ $packaging_package->package->id }}"
                                            aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5>เปลี่ยน Package {{ $packaging_package->package->name }}</h5>

                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <form
                                                            action="{{ route('changePackage', $packaging_package->id) }}"
                                                            method="POST" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            {{ method_field('PUT') }}

                                                            <label>Package</label>
                                                            <select name="package" class="changePackage-select2"
                                                                style="width: 100%"
                                                                data-package-id="{{ $packaging_package->package->id }}">
                                                                @foreach ($packages as $packagesList)
                                                                    <option value="{{ $packagesList->id }}"
                                                                        @if ($packaging_package->package->id == $packagesList->id) selected @endif>
                                                                        {{ $packagesList->name }}</option>
                                                                @endforeach

                                                            </select>
                                                            <button class="btn btn-warning mt-2 w-100" type="submit"
                                                                onclick="return confirm(&quot;ต้องการเปลี่ยน Package ?&quot;)">เปลี่ยน</button>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <form action="{{ route('deletePackageInPackaging', $packaging_package->id) }}"
                                            method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn"
                                                onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                                                    class='bx bx-trash bx-tada-hover bx-sm'></i>

                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            @endforeach


        </tbody>

    </table>
    {{-- {{ $packages->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }} --}}

    @include('TestPackages.script')
@endsection
