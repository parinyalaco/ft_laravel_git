<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Image Auto Slider</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="2000">
    <div class="carousel-inner">
        @php $active = 'active'; @endphp
        @foreach ($pack_paper->delivery_plan_product->product->packaging->packaging_packages as $packaging_package)
            @if ($packaging_package->package)
                @if (count($packaging_package->package->packagesImgs) > 0)
                    @foreach ($packaging_package->package->packagesImgs as $packagesImgs)
                        <div class="carousel-item {{ $active }}">
                            <img src="{{ asset('storage/' . $packagesImgs->main_img_path) }}"
                                 class="d-block w-100"
                                 alt="{{ $packagesImgs->name }}"
                                 style="max-height: 350px; height: auto;">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>{{ $packagesImgs->name }}</h5>
                                <p>{{ $packagesImgs->detail }}</p>
                            </div>
                        </div>
                        @php $active = ''; @endphp
                    @endforeach
                @else
                    <div class="carousel-item active">
                        <div class="d-block w-100" style="text-align: center; padding: 100px; background-color: #eee;">
                            <h2>ไม่มีรูป</h2>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

</body>
</html>
