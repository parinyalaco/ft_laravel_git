@php
    $packagings = $pack_paper->delivery_plan_product->product->packagings->where('status', 'Active');
@endphp

<table class="center">
    {{-- 1 --}}
    <tr>
        <td rowspan="2" style="border: none"></td>
        <th rowspan="2">ผลิตภัณฑ์</th>
        <th rowspan="2">ลูกค้า</th>
        <th rowspan="2">วันที่บรรจุ</th>
        <th rowspan="2">ORDER NUMBER</th>
        <th colspan="{{ count($pack_paper->delivery_plan_product->product->packaging->packaging_packages) }}">
            ชนิดของถุงและกล่อง</th>
        <th colspan="3">ขนาดบรรจุ</th>
        <th>สีสายรัด</th>
    </tr>
    {{-- 2 --}}
    <tr>
        @foreach ($pack_paper->delivery_plan_product->product->packaging->packaging_packages as $packaging_package)
            <th>
                @if ($packaging_package->package)
                    @if (!empty(config('myconfig.head_column.' . $packaging_package->package->packagetype->name)))
                        {{ config('myconfig.head_column.' . $packaging_package->package->packagetype->name) }}
                    @else
                        {{ $packaging_package->package->packagetype->name }}
                    @endif
                @endif
            </th>
        @endforeach
        <th>น้ำหนัก / ถุง</th>
        <th>จำนวนถุง / กล่อง</th>
        <th>น้ำหนัก / กล่อง</th>
        <th>{{ $pack_paper->pack_paper_standard->cable }}</th>
    </tr>
    {{-- 3 --}}
   <tr>
        <td style="border: none">std.</td>
        <th>{{ $pack_paper->delivery_plan_product->product->name }}</th>
        <th>{{ $pack_paper->delivery_plan_product->product->customer->desc or '' }}</th>
        <th>ว/ด/ป</th>
        <th>ตามใบแจ้งโหลด</th>




        @foreach ($packagings as $packaging)
            @foreach ($packaging->packaging_packages as $packaging_package)
                <th>
                    {{ $packaging_package->package->name }}
                </th>
            @endforeach
        @endforeach


        <th>
            @if (is_numeric($pack_paper->delivery_plan_product->product->packaging->inner_weight_g))
                {{ number_format($pack_paper->delivery_plan_product->product->packaging->inner_weight_g, 2) }} กรัม
            @else
                {{ $pack_paper->delivery_plan_product->product->packaging->inner_weight_g }} กรัม
            @endif
        </th>
        <th>
            @if (is_numeric($pack_paper->delivery_plan_product->product->packaging->number_per_pack))
                {{ number_format($pack_paper->delivery_plan_product->product->packaging->number_per_pack, 2) }}
            @else
                {{ $pack_paper->delivery_plan_product->product->packaging->number_per_pack }}
            @endif

        </th>
        <th>
            @if (is_numeric($pack_paper->delivery_plan_product->product->packaging->outer_weight_kg))
                {{ number_format($pack_paper->delivery_plan_product->product->packaging->outer_weight_kg, 2) }} กก.
            @else
                {{ $pack_paper->delivery_plan_product->product->packaging->outer_weight_kg }} กก.
            @endif

        </th>
        <th>{{ $pack_paper->pack_paper_standard->cable }}</th>
    </tr>  
    {{-- 4 --}}
    <tr>
        <td style="border: none">รุ่น / Lot. </td>
        <th>-</th>
        <th>-</th>
        <th>-</th>
        <th>-</th>


        @foreach ($packagings as $packaging)
            @foreach ($packaging->packaging_packages as $packaging_package)
                <td>
                    @if ($packaging_package->package)
                        {{ $packaging_package->package->desc }}
                    @endif
                </td>
            @endforeach
        @endforeach
        <td>น้ำหนักต่อถุง</td>
        <th>-</th>
        <th>-</th>
        <th>-</th>
    </tr>

    {{-- 5 --}}
    @php
        $dates = $pack_paper->pack_paper_lots->groupBy('mfg_date');
        $countRowSpan = $dates->count();
        $firstDate = $dates->keys()->first(); // เอาวันที่แรก
        $remainingDates = $dates->slice(1); // เอาวันที่ที่เหลือ
    @endphp
    <tr>
        <td rowspan="{{ $countRowSpan }}" style="border: none"></td>
        <td rowspan="{{ $countRowSpan }}">{{ $pack_paper->delivery_plan_product->product->name }}</td>
        <td rowspan="{{ $countRowSpan }}">{{ $pack_paper->delivery_plan_product->product->customer->desc or '' }}</td>

        <!-- แสดงวันที่แรกในแถวแรก -->
        <td>{{ date('d/m/Y', strtotime($firstDate)) }}</td>



        <td rowspan="{{ $countRowSpan }}">{{ $pack_paper->delivery_plan_product->delivery_plan->orders }}</td>

        @foreach ($packagings as $packaging)
            @foreach ($packaging->packaging_packages as $packaging_package)
                @if ($packaging_package->package)
                    <td rowspan="{{ $countRowSpan }}"> {{ $packaging_package->package->name }}</td>
                @endif
            @endforeach
        @endforeach

        <th rowspan="{{ $countRowSpan }}">
            {{ $pack_paper->delivery_plan_product->product->packaging->inner_weight_g_es }}</th>
        <th rowspan="{{ $countRowSpan }}">
            @if (is_numeric($pack_paper->delivery_plan_product->product->packaging->number_per_pack))
                {{ number_format($pack_paper->delivery_plan_product->product->packaging->number_per_pack, 2) }}
            @else
                {{ $pack_paper->delivery_plan_product->product->packaging->number_per_pack }}
            @endif
        </th>
        <th rowspan="{{ $countRowSpan }}">
            @if (is_numeric($pack_paper->delivery_plan_product->product->packaging->outer_weight_kg))
                {{ number_format($pack_paper->delivery_plan_product->product->packaging->outer_weight_kg, 2) }} กก.
            @else
                {{ $pack_paper->delivery_plan_product->product->packaging->outer_weight_kg }} กก.
            @endif
        </th>
        <td rowspan="{{ $countRowSpan }}">{{ $pack_paper->pack_paper_standard->cable }}</td>
    </tr>

    <!-- แสดงวันที่ที่เหลือในแถวต่อ ๆ ไป -->
    @foreach ($remainingDates as $mfg_date => $pack_paper_lots)
        <tr>
            <td>{{ date('d/m/Y', strtotime($mfg_date)) }}</td>
        </tr>
    @endforeach

</table>
