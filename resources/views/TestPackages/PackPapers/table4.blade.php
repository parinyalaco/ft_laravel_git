<table class="center">
    <tr>
        <td>Customer</td>
        <td>ORDER</td>
        {{-- <td>#SI</td> --}}
        <td>Product Fac</td>
        <td>Product Name</td>
        <td>WEIGHT (kg)</td>
        <td>QUANTITY ({{ $pack_paper->delivery_plan_product->product->unit->name }})</td>
        <td>LOADING</td>
        <td>Remark</td>
    </tr>
    <tr>
        <th>{{ $pack_paper->delivery_plan_product->delivery_plan->customer->name }}</th>
        <th>{{ $pack_paper->delivery_plan_product->delivery_plan->orders }}</th>
        {{-- <th> </th> --}}
        <th>{{ $pack_paper->delivery_plan_product->product->product_fac }}</th>
        <th>{{ $pack_paper->delivery_plan_product->product->name }}</th>
        <th>{{ number_format($pack_paper->delivery_plan_product->weight) }}</th>
        <th>{{ number_format($pack_paper->delivery_plan_product->quantity) }}</th>
        <th>
            @if ($pack_paper->delivery_plan_product->productStock)
                Stock {{ $pack_paper->delivery_plan_product->productStock->seq }},

                {{ \Carbon\Carbon::createFromFormat('m', $pack_paper->delivery_plan_product->productStock->month)->format('F') }}
                {{ $pack_paper->delivery_plan_product->productStock->year }}
            @else
                {{ date('d-F-y', strtotime($pack_paper->delivery_plan_product->loading_date)) }}
            @endif
        </th>
        <th>{{ $pack_paper->remark or '' }} </th>
    </tr>
</table>
