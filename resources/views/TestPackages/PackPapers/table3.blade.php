<table class="center">
    <tr>
        <th>วันที่หมดอายุ</th>
        <th>Lot</th>
        <th>NO .กล่อง</th>
        <th>จำนวนกล่อง</th>
        <th>จำนวนถุง</th>
        <th>น้ำหนักผลิตภัณฑ์ /P</th>
        <th>น้ำหนักผลิตภัณฑ์ /F</th>
        <th>จำนวนพาเลท</th>
        <th>จำนวนกล่องพาเลทสุดท้าย</th>
        <th>หมายเหตุ</th>
    </tr>
    @foreach ($pack_paper->pack_paper_lots as $pack_paper_lot)
        <tr>
            <td>

                @php

                    $exp_date_standard = $pack_paper->pack_paper_standard->exp_month;
                    $mfg_month = date('Y-m-d', strtotime($pack_paper_lot->mfg_date));
                    $manufacture_date = $mfg_month;

                    $manufacture_date_cal = date('Y-m', strtotime($manufacture_date));

                    $expiry_date = date('Y-m-d', strtotime($manufacture_date . ' +' . $exp_date_standard . 'months'));
                    $expiry_date_cal = date('Y-m',strtotime($manufacture_date_cal . ' +' . $exp_date_standard . 'months'));

                    $last_day_of_manufacture_month = date('t', strtotime($manufacture_date_cal));

                    $last_day_of_expiry_month = date('t', strtotime($expiry_date_cal));
                   
                 

                    if ($last_day_of_manufacture_month > $last_day_of_expiry_month) {
                     
                        if ($last_day_of_manufacture_month == date('d', strtotime($manufacture_date))) {
                            $expiry_date = date('Y-m-d', strtotime($manufacture_date . ' +18 months -1 day'));
                           
                        } else {
                            $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . 'months'));
                        }
                    }

                    if ($last_day_of_expiry_month < 30) {
                        
                        if ($last_day_of_expiry_month == 28) {
                   
                            if (date('d', strtotime($manufacture_date)) == 31) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months' . ' -3 day'));
                            }
                            if (date('d', strtotime($manufacture_date)) == 30) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months' . ' -2 day'));
                            }
                            if (date('d', strtotime($manufacture_date)) == 29) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months' . ' -1 day'));
                            }
                            
                        }
                        if ($last_day_of_expiry_month == 29) {
                            if (date('d', strtotime($manufacture_date)) == 31) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months' . ' -2 day'));
                            }
                            if (date('d', strtotime($manufacture_date)) == 30) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months' . ' -1 day'));
                            }
                          
                        }
                    }

                @endphp

                {{ date('Y.m.d', strtotime($expiry_date)) }}

            <td>{{ $pack_paper_lot->lot }}</td>
            <td>{{ $pack_paper_lot->sequence_boxes }}</td>
            <td>{{ number_format($pack_paper_lot->quantity_boxes) }}</td>
            <td>{{ number_format($pack_paper_lot->quantity_bags) }}</td>
            <td>{{ number_format($pack_paper->delivery_plan_product->product->packaging->outer_weight_kg * $pack_paper_lot->quantity_boxes * 0.95, 0) }}
            </td>
            <td>{{ number_format($pack_paper->delivery_plan_product->product->packaging->outer_weight_kg * $pack_paper_lot->quantity_boxes, 0) }}
            </td>
            <td style="width: 150px">{{ $pack_paper_lot->number_of_pallets }}</td>
            <td style="width: 150px">{{ $pack_paper_lot->last_pallet_boxes }}</td>
            <td>{{ $pack_paper_lot->remark }}</td>
        </tr>
    @endforeach
</table>
