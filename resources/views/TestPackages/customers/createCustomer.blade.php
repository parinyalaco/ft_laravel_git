<!-- Button to trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createNewUnit">
    <i class='bx bx-plus-circle'></i> เพิ่ม Customer ใหม่
</button>

<!-- Modal -->
<div class="modal fade" id="createNewUnit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">สร้าง Unit ใหม่</h5>
                <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <form action="{{ route('storePacgakesCustomer') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="modal-body">
                    <label>ชื่อ</label>
                    <input type="text" name="name" class="form-control" required placeholder="ตัวอย่าง : Carton">

                    <label>รายละเอียด</label>
                    <input type="text" name="desc" class="form-control" required
                        placeholder="ตัวอย่าง : Carton is a light box or container">

                    <label>สถานะ</label>
                    <select class="form-select" name='status'>
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                    </select>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                    <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>
