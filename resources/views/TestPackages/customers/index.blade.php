@extends('TestPackages.layouts.layout')

@section('content')
    <div class="m-2">
        <h4>Customer</h4>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    @include('TestPackages.customers.createCustomer')
    <form action="{{ route('testsPackagesCustomer') }}" method="get">
        {{ csrf_field() }}

        <div class="input-group my-3">
            <input type="text" class="form-control" name="searchCustomer" placeholder="ค้นหา Customer. . . " required
                value="{{ $searchCustomer }}">
            <a href="{{ route('testsPackagesCustomer') }}" class="btn btn-warning">คืนค่า</a>
            <button type="submit" class="btn btn-primary">ค้นหา</button>
        </div>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th>ชื่อ</th>
                <th>รายละเอียด</th>
                <th>สถานะ</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($customers as $customer)
                <tr>
                    <td>{{ $customer->name }}</td>
                    <td>{{ $customer->desc }}</td>
                    <td>{{ $customer->status }}</td>
                    <td>
                        @include('TestPackages.customers.action')
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $customers->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}
@endsection
