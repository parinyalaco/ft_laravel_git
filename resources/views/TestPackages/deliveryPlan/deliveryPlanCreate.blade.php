<h4>Delivery plan</h4>

<button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
    aria-controls="collapseExample">
    เพิ่ม Delivery Plan
</button>
<div class="collapse mt-2" id="collapseExample">
    <div class="card card-body">
        <form action="{{ route('addTestDeliveryPlan') }}" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-3">
                    <h5 class="card-title">เพิ่ม Delivery Plan V 2</h5>
                    {{ csrf_field() }}
                    <label>Customer </label>
                    <select name="customer_id" class="first-select2  form-select form-select-sm" style="width: 100%">
                        @foreach ($customers as $customer_id => $customer_name)
                            <option value="{{ $customer_id }}">{{ $customer_name }}</option>
                        @endforeach
                    </select>

                    <label>Order</label>
                    <input type="text" name="orders" class="form-control form-control-sm" required>

                    <label>Customer PO No.</label>
                    <input type="text" name="customer_po_no" class="form-control form-control-sm">

                    <label>Booking No.</label>
                    <input type="text" name="booking_no" class="form-control form-control-sm">

                    <hr>

                    <label>Product name</label>
                    <select name="product_id[0]" class="first-select2" style="width: 100%" id="productSelect">
                        @foreach ($products as $product)
                            <option value="{{ $product->id }}" data-weight="{{ $product->weight_with_bag }}">
                                {{ $product->name }}
                            </option>
                        @endforeach
                    </select>

                    <label>Weight</label>
                    <input type="number" name="weight[0]" id="weightInput" class="form-control form-control-sm"
                        step="0.01" required>

                    <!-- ช่องสำหรับแสดงจำนวนกล่องที่คำนวณได้ -->
                    <div style="background-color: #fffd83" class="mt-2">
                        <h4 id="calculatedBoxes" class="p-2 text-center">จำนวนกล่อง: 0</h4>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label>Loading Date</label>
                            <input type="date" name="loading_date[0]" class="form-control form-control-sm" required
                                value="{{ date('Y-m-d') }}">

                        </div>
                        {{-- @if (Auth::user()->group_id == 1)
                            <div class="col">
                            <label>วันที่จัดทำใบแจ้งบรรจุ</label>
                                <input type="date" name="production_date[0]" class="form-control form-control-sm"
                                    required value="{{ date('Y-m-d') }}">
                            </div>
                        @endif --}}
                    </div>
                    <label>Product Stock</label>

                    <div class="input-group mb-3">
                        <input type="checkbox" name="is_stock[0]" class="mx-2">
                        <input type="month" name="is_stock_date[0]" class="form-control form-control-sm"
                            value="{{ date('Y-m') }}">
                    </div>

                    <label>Remark</label>
                    <input type="text" name="remark[0]" class="form-control form-control-sm">

                </div>
                <div class="col-3 ">
                    <a id="add-field-btn" class="btn btn-primary btn-sm">+ Product</a>
                    <div id="fields-container">

                    </div>
                    <button type="submit" class="btn btn-success btn-sm my-2 w-100"
                        onclick="return confirm('สร้าง Delivery Plan')">เพิ่ม Delivery Plan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const selectElement = document.getElementById('productSelect');
        const weightInput = document.getElementById('weightInput');
        const calculatedBoxes = document.getElementById('calculatedBoxes');

        // ฟังก์ชันสำหรับการคำนวณและแสดงจำนวนกล่อง
        function calculateBoxes() {
            const selectedOption = selectElement.options[selectElement.selectedIndex];
            const weightWithBag = parseFloat(selectedOption.getAttribute('data-weight'));
            const totalWeight = parseFloat(weightInput.value);

            if (!isNaN(weightWithBag) && !isNaN(totalWeight) && weightWithBag > 0) {
                const numberOfBoxes = Math.floor(totalWeight / weightWithBag);
                calculatedBoxes.innerText = `จำนวนกล่อง: ${numberOfBoxes}`;
            } else {
                calculatedBoxes.innerText = 'จำนวนกล่อง: ข้อมูลไม่ถูกต้อง';
            }
        }

        // เพิ่ม event listeners เมื่อมีการเปลี่ยนแปลงการเลือกสินค้าและการกรอกน้ำหนัก
        selectElement.addEventListener('change', calculateBoxes);
        weightInput.addEventListener('input', calculateBoxes);

        // เรียกฟังก์ชันคำนวณครั้งแรกเมื่อโหลดหน้าเว็บ
        calculateBoxes();
    });
</script>
