<div class="btn-group" role="group" aria-label="Basic example">
    <!-- Button to Edit Delivery Plan modal -->
    <button type="button" class="btn " data-toggle="modal" data-target="#editDeliveryPlan-{{ $deliveryPlan->id }}"
        title="แก้ไข">
        <i class='bx bx-edit-alt  bx-sm'></i>
    </button>

    <!-- Modal -->
    <div class="modal fade" id="editDeliveryPlan-{{ $deliveryPlan->id }}" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">แก้ไข Delivery Plan
                        {{ $deliveryPlan->name }}
                    </h5>
                    <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <form action="{{ route('updateDeliveryPlan', $deliveryPlan->id) }}" method="post"
                    enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <label>Customer </label>
                        <select name="customer_id" class="editDeliveryPlan-select2 form-control-sm" style="width: 100%"
                            data-id="{{ $deliveryPlan->id }}">
                            @foreach ($customers as $customer_id => $customer_name)
                                <option value="{{ $customer_id }}" @if ($deliveryPlan->customer_id == $customer_id) selected @endif>
                                    {{ $customer_name }}
                                </option>
                            @endforeach
                        </select>

                        <label>Order</label>
                        <input type="text" name="orders" class="form-control form-control-sm" required
                            value="{{ $deliveryPlan->orders }}">

                        <label>Customer PO No.</label>
                        <input type="text" name="customer_po_no" class="form-control form-control-sm"
                            value="{{ $deliveryPlan->customer_po_no }}">

                        <label>Booking No.</label>
                        <input type="text" name="booking_no" class="form-control form-control-sm"
                            value="{{ $deliveryPlan->booking_no }}">

                        <!-- ================================= Products by Delivery Plan ================================= -->
                        @foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
                            <label>Product name</label>
                            <select name="product_id[{{ $delivery_plan_product->id }}]"
                                id="productSelectEdit-{{ $delivery_plan_product->id }}" class="form-control-sm "
                                style="width: 100%" data-id="{{ $delivery_plan_product->id }}"
                                data-id2="{{ $deliveryPlan->id }}">
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}"
                                        data-weight-edit="{{ $product->weight_with_bag }}"
                                        @if ($delivery_plan_product->product_id == $product->id) selected @endif>
                                        {{ $product->name }}
                                    </option>
                                @endforeach
                            </select>

                            <label>Weight</label>
                            <input type="number" name="weight[{{ $delivery_plan_product->id }}]"
                                id="weightInputEdit-{{ $delivery_plan_product->id }}"
                                class="form-control form-control-sm" step="0.01"
                                value="{{ $delivery_plan_product->weight }}">

                            <!-- ช่องสำหรับแสดงจำนวนกล่องที่คำนวณได้ -->
                            <p id="calculatedBoxesEdit-{{ $delivery_plan_product->id }}">จำนวนกล่อง: 0</p>

                            <div class="row">
                                <div class="col">
                                    <label>Loading Date</label>
                                    <input type="date" name="loading_date[{{ $delivery_plan_product->id }}]"
                                        class="form-control form-control-sm" required
                                        value="{{ date('Y-m-d', strtotime($delivery_plan_product->loading_date)) }}">

                                </div>
                                <div class="col">
                                    <label>วันที่จัดทำใบแจ้งบรรจ</label>
                                    <input type="date" name="production_date[{{ $delivery_plan_product->id }}]"
                                        class="form-control form-control-sm" required
                                        value="{{ date('Y-m-d', strtotime($delivery_plan_product->production_date)) }}">
                                </div>
                            </div>
                            <label>Product Stock</label>

                            @php
                                $is_stock_date = date('Y-m-d');
                                if ($delivery_plan_product->productStock) {
                                    $is_stock_date =
                                        $delivery_plan_product->productStock->year .
                                        '-' .
                                        $delivery_plan_product->productStock->month .
                                        '-01';
                                }

                            @endphp

                            <div class="input-group mb-3">
                                <input type="checkbox" name="is_stock[{{ $delivery_plan_product->id }}]" class="mx-2"
                                    @if ($delivery_plan_product->productStock) checked @endif>
                                <input type="month" name="is_stock_date[{{ $delivery_plan_product->id }}]"
                                    class="form-control form-control-sm"
                                    value="{{ date('Y-m', strtotime($is_stock_date)) }}">
                            </div>

                            <label>Remark</label>
                            <input type="text" name="remark[{{ $delivery_plan_product->id }}]"
                                class="form-control form-control-sm" value="{{ $delivery_plan_product->remark }}">
                        @endforeach
                        <!-- ================================= END ================================= -->



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-primary">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('deleteDeliveryPlan', $deliveryPlan->id) }}" style="display:inline">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn" title="Delete %%modelName%%"
            onclick="return confirm(&quot; ต้องการลบ Delivery plan ?&quot;)"><i class='bx bx-trash bx-sm'></i></button>
    </form>

</div>


<script>
    $(document).ready(function() {

        $('[id^=productSelectEdit]').each(function() {
            var deliveryPlanProductEditId = $(this).data('id');
            var deliveryPlanEditId = $(this).data('id2');

            $(this).select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#editDeliveryPlan-' + deliveryPlanEditId)
            });


            var selectElementEdit = document.getElementById('productSelectEdit-' +
                deliveryPlanProductEditId);
            var weightInputEdit = document.getElementById('weightInputEdit-' +
                deliveryPlanProductEditId);
            var calculatedBoxesEdit = document.getElementById('calculatedBoxesEdit-' +
                deliveryPlanProductEditId);

            // ฟังก์ชันสำหรับการคำนวณและแสดงจำนวนกล่อง
            function calculateBoxesEdit() {
                const selectedOptionEdit = selectElementEdit.options[selectElementEdit.selectedIndex];
                const weightWithBagEdit = parseFloat(selectedOptionEdit.getAttribute(
                    'data-weight-edit'));
                const totalWeightEdit = parseFloat(weightInputEdit.value);

                if (!isNaN(weightWithBagEdit) && !isNaN(totalWeightEdit) && weightWithBagEdit > 0) {
                    const numberOfBoxesEdit = Math.floor(totalWeightEdit / weightWithBagEdit);
                    calculatedBoxesEdit.innerText = `จำนวนกล่อง: ${numberOfBoxesEdit}`;
                } else {
                    calculatedBoxesEdit.innerText = 'จำนวนกล่อง: ข้อมูลไม่ถูกต้อง';
                }
            }

            // เพิ่ม event listeners เมื่อมีการเปลี่ยนแปลงการเลือกสินค้าและการกรอกน้ำหนัก
            selectElementEdit.addEventListener('change', calculateBoxesEdit);
            weightInputEdit.addEventListener('input', calculateBoxesEdit);

            // เรียกฟังก์ชันคำนวณครั้งแรกเมื่อโหลดหน้าเว็บ
            calculateBoxesEdit();
        });
    });
</script>
