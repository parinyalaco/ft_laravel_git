@foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
    @if (isset($delivery_plan_product->product))
        <p
            title="รายละเอียดสินค้า &#013;product Name: {{ $delivery_plan_product->product_name }}&#013;Weight: {{ $delivery_plan_product->weight }}&#013;Quantity: {{ $delivery_plan_product->quantity }}&#013;Loading Date: {{ $delivery_plan_product->loading_date }}&#013;วันแจ้งบรรจุ: {{ $delivery_plan_product->packing_date }}&#013;">
            {{ $delivery_plan_product->product->name }}
        </p>

        @if (isset($delivery_plan_product->pack_paper))
            @if ($delivery_plan_product->pack_paper->status == 'Active')
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" style="width: 200px"
                    data-target="#pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                    ใบแจ้งบรรจุ เลขที่ {{ $delivery_plan_product->pack_paper->id }}
                </button>
            @else
                <button class="btn btn-danger btn-sm"
                    onclick="return alert('{{ $delivery_plan_product->pack_paper->remark }}')">ใบแจ้งบรรจุเลขที่
                    {{ $delivery_plan_product->pack_paper->id }}
                    ถูกยกเลิก </button>
            @endif
            <div class="modal" id="pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{ route('test_pack_paper_doc', [$delivery_plan_product->pack_paper->id]) }}"
                            method="post" enctype="multipart/form-data" target="_blank">

                            <div class="modal-header">
                                <h5 class="modal-title">รายละเอียด วันที่ผลิต
                                    ของใบแจ้งบรรจุ
                                </h5>
                                <button type="button" class="close btn " data-dismiss="modal">X</button>
                            </div>
                            <div class="modal-body">

                                {{ csrf_field() }}
                                @foreach ($delivery_plan_product->pack_paper->pack_paper_lots as $pack_paper_lot)
                                    <label>วันที่ผลิตของLot.
                                        <strong>{{ $pack_paper_lot->lot }}</strong></label>
                                    <div class="input-group mb-3">

                                        <input type="date"
                                        name="mfg_month[{{ $pack_paper_lot->id }}]"
                                        class="form-control form-control-sm"
                                        value="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}"
                                        min="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}">

                                        <input type="text" name="remark[{{ $pack_paper_lot->id }}]"
                                            value="{{ $pack_paper_lot->remark }}" class="form-control form-control-sm"
                                            placeholder="หมายเหตุ">
                                    </div>
                                @endforeach
                                <input type="text" name="packPaperRemark" class="form-control form-control-sm"
                                    value="{{ $delivery_plan_product->pack_paper->remark }}">

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                    data-target="#cancelModal-{{ $delivery_plan_product->pack_paper->id }}">
                                    ยกเลิกใบแจ้งบรรจุ
                                </button>
                                <button type="submit" class="btn btn-success"
                                    onclick="return confirm('ไปหน้า ใบบรรจุ')">ใบแจ้งบรรจุ
                                    เลขที่
                                    {{ $delivery_plan_product->pack_paper->id }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="cancelModal-{{ $delivery_plan_product->pack_paper->id }}" tabindex="-1"
                aria-labelledby="nestedModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="nestedModalLabel">
                                ยกเลิกใบแจ้งบรรจุเลขที่
                                {{ $delivery_plan_product->pack_paper->id }}
                            </h5>
                            <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                        <form action="{{ route('test_cancel_pack_paper', [$delivery_plan_product->pack_paper->id]) }}"
                            method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <label>รายละเอียด</label>
                                <input type="text" name="remark" class="form-control form-control-sm">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger my-2">บันทึก</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    @endif
@endforeach
