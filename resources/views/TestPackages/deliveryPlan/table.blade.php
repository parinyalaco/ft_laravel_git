<div class="m-2" style="font-size: 16px">
    <h5>ตาราง Delivery Plan</h5>

    {{-- id="deliveryPlanTable" --}}
    <table class="table table table-striped">
        <thead>
            <tr class="table-primary">
                <th>Customer Name</th>
                <th>Booking No.</th>
                <th>Orders</th>
                <th>Customer PO No.</th>
                <th>Product</th>
                <th style="text-align: center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($deliveryPlans as $deliveryPlan)
                <tr>
                    <td>{{ $deliveryPlan->customer->name or '-' }}</td>
                    <td>{{ $deliveryPlan->booking_no or '-' }}</td>
                    <td>{{ $deliveryPlan->orders or '-' }}</td>
                    <td>{{ $deliveryPlan->customer_po_no or '-' }}</td>
                    <td>
                        @include('TestPackages.deliveryPlan.table.deliveryPlanProduct')
                    </td>

                    <td style="width: 250px" class="text-center">
                        {{-- @include('TestPackages.deliveryPlan.table.editDeliveryPlan') --}}

                        <a href="{{ route('test_productByDeliveryPlan', [$deliveryPlan->id]) }}" class="btn btn-primary"
                            onclick="return confirm('ไปหน้า ทำใบแจ้งบรรจุ')">ทำใบแจ้งบรรจุ</a>
                    </td>

                </tr>
            @endforeach

        </tbody>
    </table>

    {{ $deliveryPlans->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}

</div>
