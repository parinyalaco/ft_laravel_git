<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link href="path/to/select2-bootstrap-5-theme.min.css" rel="stylesheet" />



<script>
    $(document).ready(function() {
        // Initialize Select2 for existing elements
        $('.first-select2').select2({
            theme: 'bootstrap-5',
            placeholder: 'Select an option',
            dropdownParent: $('#collapseExample')
        });

        // Initialize DataTable
        new DataTable('#deliveryPlanTable', {
            order: [
                [3, 'desc']
            ]
        });

        // Initialize Select2 for editDeliveryPlan-select2 elements
        $('.editDeliveryPlan-select2').each(function() {
            var deliveryPlanId = $(this).data('id');
            $(this).select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#editDeliveryPlan-' + deliveryPlanId)
            });
        });



        var i = 0;

        function addNewField() {
            i++;
            var newField =
                '<div class="form-group mb-2">' +
                '<div class="d-grid gap-2 d-md-flex justify-content-md-end">' +
                '<button type="button" class="btn btn-danger btn-sm remove-field-btn">ลบ</button></div>' +

                '<label>Product</label>' +
                '<select name="product_id[' + i +
                ']" class="form-select-sm form-control select2 product-select" required>' +
                '@foreach ($products as $product)' +
                '<option value="{{ $product->id }}" data-weight="{{ $product->weight_with_bag }}">{{ $product->name }}</option>' +
                '@endforeach' +
                '</select>' +

                '<label>Weight</label>' +
                '<input type="number" name="weight[' + i +
                ']" class="form-control form-control-sm weight-input" required>' +
                '<div style="background-color: #fffd83" class="mt-2">' +

                '<h4 class="calculated-boxes p-2 text-center">จำนวนกล่อง: 0</h4>' +
                '</div>' +

                '<div class="row">' +
                '<div class="col">' +
                '<label>Loading Date</label>' +
                '<input type="date" name="loading_date[' + i +
                ']" class="form-control form-control-sm" required value="{{ date('Y-m-d') }}">' +
                '<label>Product Stock</label>' +
                '</div>' +


                '<div class="col">' +
                '<label>วันแจ้งบรรจุ</label>' +
                '<input type="date" name="production_date[' + i +
                ']" class="form-control form-control-sm" required value="{{ date('Y-m-d') }}">' +
                '</div>' +
                '</div>' +

                '<div class="input-group mb-3">' +
                '<input type="checkbox" name="is_stock[' + i +
                ']" class="mx-2">' +
                '<input type="month" name="is_stock_date[' + i +
                ']" class="form-control form-control-sm" value="{{ date('Y-m') }}">' +
                '</div>' +

                '<label>Remark</label>' +
                '<input type="text" name="remark[' + i +
                ']" class="form-control form-control-sm">' +

                '<hr>' +
                '</div>';

            $('#fields-container').append(newField);

            // Initialize Select2 for the newly added select element
            $('#fields-container .select2').last().select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#collapseExample')
            });

            // Add event listeners for the newly added select and input elements
            $('#fields-container .product-select').last().on('change', calculateBoxes);
            $('#fields-container .weight-input').last().on('input', calculateBoxes);
        }

        $('#add-field-btn').click(function() {
            addNewField();
        });

        $('body').on('click', '.remove-field-btn', function() {
            $(this).closest('.form-group').remove();
        });

        function calculateBoxes() {
            var $parent = $(this).closest('.form-group');
            var selectedOption = $parent.find('.product-select option:selected');
            var weightWithBag = parseFloat(selectedOption.data('weight'));
            var totalWeight = parseFloat($parent.find('.weight-input').val());

            if (!isNaN(weightWithBag) && !isNaN(totalWeight) && weightWithBag > 0) {
                var numberOfBoxes = Math.floor(totalWeight / weightWithBag);
                $parent.find('.calculated-boxes').text(`จำนวนกล่อง: ${numberOfBoxes}`);
            } else {
                $parent.find('.calculated-boxes').text('จำนวนกล่อง: ข้อมูลไม่ถูกต้อง');
            }
        }
    });
</script>
