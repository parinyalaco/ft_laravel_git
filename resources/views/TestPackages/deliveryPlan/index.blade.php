@extends('TestPackages.layouts.layout')

@section('content')
    <div class="m-2">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="mx-2">
            @include('TestPackages.deliveryPlan.deliveryPlanCreate')
        </div>
        @include('TestPackages.deliveryPlan.table')
    </div>
    @include('TestPackages.deliveryPlan.script')
@endsection
