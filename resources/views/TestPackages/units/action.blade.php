<div class="btn-group" role="group" aria-label="Basic example">
    <!-- Button to trigger modal -->
    <button type="button" class="btn " data-toggle="modal" data-target="#editUnit-{{ $unit->id }}">
        <i class='bx bx-edit-alt  bx-sm'></i>
    </button>

    <!-- Modal -->
    <div class="modal fade" id="editUnit-{{ $unit->id }}" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">แก้ไข Unit {{ $unit->name }}</h5>
                    <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <form action="{{ route('updatePackagesUnit', $unit->id) }}" method="post" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <label>ชื่อ</label>
                        <input type="text" name="name" class="form-control" required value="{{ $unit->name }}">

                        <label>รายละเอียด</label>
                        <input type="text" name="desc" class="form-control" required value="{{ $unit->desc }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-primary">บันทึก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <form method="POST" action="{{ route('deletePackageUnit', $unit->id) }}" style="display:inline">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <button type="submit" class="btn" title="Delete %%modelName%%"
            onclick="return confirm(&quot;Confirm delete?&quot;)"><i class='bx bx-trash bx-sm'></i></button>

    </form>

</div>
