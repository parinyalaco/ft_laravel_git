@extends('TestPackages.layouts.layout')

@section('content')
    <div class="m-2">
        <h4>Unit </h4>
    </div>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    @include('TestPackages.units.createUnit')


    <table class="table">
        <thead>
            <tr>
                <th>ชื่อ</th>
                <th>รายละเอียด</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>

            @foreach ($units as $unit)
                <tr>
                    <td>{{ $unit->name }}</td>
                    <td>{{ $unit->desc }}</td>
                    <td>
                        @include('TestPackages.units.action')
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $units->links() }}
@endsection
