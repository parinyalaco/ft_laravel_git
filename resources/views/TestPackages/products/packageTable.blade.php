<table class="table table-bordered">

    @foreach ($product->packagings->groupBy('product_id') as $productId => $packagings)
        @foreach ($packagings as $packaging)
            <tr>
                <td colspan="4" style="background-color: #ceebcf">
                    <div class="row">
                        <div class="col">
                            <strong>Version :</strong>
                            {{ $packaging->version }} |
                            <strong class="mx-2">น้ำหนักต่อถุง (g):</strong> {{ $packaging->inner_weight_g }} ,

                            <strong class="mx-2">น้ำหนักต่อถุง (g) (ประมาณค่า):</strong>
                            {{ $packaging->inner_weight_g_es }} |

                            <strong class="mx-2">น้ำหนักต่อ กล่อง :</strong>

                            {{ number_format($packaging->outer_weight_kg, 3) }}|

                            <strong class="mx-2">จำนวนถุง/กล่อง :</strong>
                            {{ number_format($packaging->number_per_pack, 2) }}|

                            @if ($packaging->status == 'Active')
                                <strong style="background-color: aliceblue;color: #008000" class="mx-2">
                                    {{ $packaging->status }} <i class='bx bx-check'></i></strong>
                            @else
                                <strong style="background-color: aliceblue;color: #ff0000" class="mx-2">
                                    {{ $packaging->status }} <i class='bx bx-x'></i></strong>
                            @endif

                        </div>
                        <div class="col col-auto">
                            <div class="btn-group">
                                <button class="btn" data-bs-toggle="modal"
                                    data-bs-target="#editPackaging-{{ $packaging->id }}">
                                    <i class='bx bx-edit-alt bx-tada-hover bx-sm'> </i>
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="editPackaging-{{ $packaging->id }}" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5>แก้ไข Packaging Version: {{ $packaging->version }}
                                                </h5>

                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>

                                            <div class="modal-body">
                                                <form action="{{ route('updatePackaging', $packaging->id) }}"
                                                    method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    {{ method_field('PUT') }}


                                                    <div class="row">
                                                        <div class="col">
                                                            <label>น้ำหนักต่อถุง (g)</label>
                                                            <input type="number" name="inner_weight_g"
                                                                class="form-control form-control-sm"
                                                                value="{{ $packaging->inner_weight_g }}">
                                                        </div>
                                                        <div class="col">
                                                            <label>น้ำหนักต่อถุง (g) (ประมาณค่า) </label>
                                                            <input type="text" name="inner_weight_g_es"
                                                                class="form-control form-control-sm"
                                                                value="{{ $packaging->inner_weight_g_es }}">
                                                        </div>
                                                    </div>

                                                    <label>น้ำหนักต่อ กล่อง (กิโลกรัม)</label>
                                                    <input type="number" name="outer_weight_kg" step="0.001"
                                                        class="form-control form-control-sm"
                                                        value="{{ number_format($packaging->outer_weight_kg, 3) }}">

                                                    <label>จำนวนถุง/กล่อง</label>
                                                    <input type="number" name="number_per_pack"
                                                        class="form-control form-control-sm"
                                                        value="{{ $packaging->number_per_pack }}">


                                                    <label>สถานะ</label>
                                                    <select name="status" class="form-select">
                                                        <option value="Active"
                                                            @if ($packaging->status == 'Active') selected @endif>
                                                            Active</option>
                                                        <option value="Inactive"
                                                            @if ($packaging->status == 'Inactive') selected @endif>
                                                            Inactive</option>

                                                    </select>
                                                    <button class="btn btn-warning mt-2 w-100" type="submit"
                                                        onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">แก้ไข</button>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <form action="{{ route('deletePackaging', $packaging->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn"
                                        onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                                            class='bx bx-trash bx-tada-hover bx-sm'></i>

                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td style="background-color: #ceebcf">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#detailModal-{{ $productId }}-{{ $packaging->version }}">
                        เพิ่ม Package ของ version {{ $packaging->version }}
                    </button>
                </td>
            </tr>
            <tr class="table-primary">
                <th>Pakage</th>
                <th>description </th>
                <th>Size</th>
                <th>สถานะ</th>
                <th>Action</th>
            </tr>

            <div class="modal fade" id="detailModal-{{ $productId }}-{{ $packaging->version }}" tabindex="-1"
                aria-labelledby="detailModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">

                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="detailModalLabel">เพิ่ม Package ของ
                                {{ $packaging->version }}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="tabA" data-bs-toggle="tab"
                                        data-bs-target="#contentA-{{ $productId }}-{{ $packaging->version }}"
                                        type="button" role="tab" aria-controls="contentA"
                                        aria-selected="true">เพิ่ม Package ใหม่</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="tabB" data-bs-toggle="tab"
                                        data-bs-target="#contentB-{{ $productId }}-{{ $packaging->version }}"
                                        type="button" role="tab" aria-controls="contentB"
                                        aria-selected="false">เลือก package</button>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content mt-2">
                                <div class="tab-pane fade show active"
                                    id="contentA-{{ $productId }}-{{ $packaging->version }}">
                                    <h5>เพิ่ม Package ใหม่</h5>

                                    <form
                                        action="{{ route('addNewPackage', [$productId, $packaging->id, $packaging->version]) }}"
                                        method="POST">
                                        {{ csrf_field() }}

                                        <label>Package Type </label>
                                        <select name="package_type_id" class="form-control">
                                            @foreach ($package_types as $package_type)
                                                <option value="{{ $package_type->id }}">
                                                    {{ $package_type->name }}</option>
                                            @endforeach
                                        </select>
                                        <label>Package Name</label>
                                        <input type="text" name="name" class="form-control">

                                        <label>Package Description</label>
                                        <input type="text" name="desc" class="form-control">

                                        <label>Package Size</label>
                                        <input type="text" name="size" class="form-control">

                                        <label>วันที่ผลิต</label>
                                        <div class="input-group mb-3">
                                            <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2"
                                                value="Active">

                                            <input name="mfg_front" type="text"
                                                class="form-control form-control-sm on_bag_MFG_stamp mx-1">


                                            <div class="on_bag_MFG_stamp">
                                                <input type="radio" name="mfg_era" value="AD"
                                                    class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                                ค.ศ.
                                                <input type="radio" name="mfg-era" value="BE"
                                                    class="form-check-input on_bag_MFG_stamp mx-1">
                                                พ.ศ.
                                            </div>

                                            <select name="mfg_stampDateFormat"
                                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm">
                                                @foreach ($stamp_date_formats as $id => $name)
                                                    <option value="{{ $id }}">
                                                        {{ $name }}</option>
                                                @endforeach
                                            </select>

                                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox"
                                                value="on" name="mfg_lot">
                                            <label class="form-check-label">
                                                Lot
                                            </label>
                                        </div>
                                        <label>วันที่หมดอายุ</label>
                                        <div class="input-group mb-3">
                                            <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2"
                                                value="Active">

                                            <input name="exp_front" type="text"
                                                class="form-control form-control-sm on_bag_MFG_stamp mx-1">

                                            <div class="on_bag_MFG_stamp">
                                                <input type="radio" name="exp_era" value="AD"
                                                    class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                                ค.ศ.
                                                <input type="radio" name="exp_era" value="BE"
                                                    class="form-check-input on_bag_MFG_stamp mx-1">
                                                พ.ศ.
                                            </div>

                                            <select name="exp_stampDateFormat"
                                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm">
                                                @foreach ($stamp_date_formats as $id => $name)
                                                    <option value="{{ $id }}">
                                                        {{ $name }}</option>
                                                @endforeach
                                            </select>

                                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox"
                                                value="on" name="exp_lot">
                                            <label class="form-check-label">
                                                Lot
                                            </label>
                                        </div>

                                        <label>Sap Note</label>
                                        <input type="text" name="sapnote" class="form-control">

                                        <hr>

                                        <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                                    </form>
                                </div>

                                <div class="tab-pane fade"
                                    id="contentB-{{ $productId }}-{{ $packaging->version }}">
                                    <h5>เลือกจาก Package เดิม</h5>

                                    <form
                                        action="{{ route('addPackagebySelect', [$productId, $packaging->id, $packaging->version]) }}"
                                        method="POST">
                                        {{ csrf_field() }}
                                        <select name="package_select" class="addPackagebySelect-select2"
                                            style="width: 100%" data-product-id="{{ $productId }}"
                                            data-packaging-id="{{ $packaging->version }}">
                                            @foreach ($packages as $packagesList)
                                                <option value="{{ $packagesList->id }}">
                                                    {{ $packagesList->name }} ({{ $packagesList->packagetype->name }})
                                                </option>
                                            @endforeach
                                        </select>

                                        <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach ($packaging->packaging_packages as $packaging_package)
                @if ($packaging_package->package)
                    <tr>
                        <td> {{ $packaging_package->package->name }}</td>
                        <td>{{ $packaging_package->package->desc }}</td>
                        <td>{{ $packaging_package->package->size }}</td>
                        @if ($packaging_package->package->status == 'Active')
                            <td style="color: green"> <i
                                    class='bx bxs-circle'></i><strong>{{ $packaging_package->package->status }}</strong>
                            </td>
                        @else
                            <td style="color: red"><i
                                    class='bx bxs-circle'></i><strong>{{ $packaging_package->package->status }}</strong>
                            </td>
                        @endif
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('viewPackage', $packaging_package->package->id) }}" class="btn"
                                    title="ดูรายละเอียด" target="_blank"> <i
                                        class='bx bxs-detail bx-tada-hover bx-sm'></i></a>

                                <button class="btn" data-bs-toggle="modal"
                                    data-bs-target="#changePackage-{{ $packaging_package->package->id }}">
                                    <i class='bx bx-edit-alt bx-tada-hover bx-sm'> </i>
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="changePackage-{{ $packaging_package->package->id }}"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5>เปลี่ยน Package {{ $packaging_package->package->id }}</h5>

                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>

                                            <div class="modal-body">
                                                <form action="{{ route('changePackage', $packaging_package->id) }}"
                                                    method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    {{ method_field('PUT') }}

                                                    <label>Package</label>
                                                    <select name="package"
                                                        class="changePackage-select2 form-select-sm"
                                                        style="width: 100%"
                                                        data-package-id="{{ $packaging_package->package->id }}">
                                                        @foreach ($packages as $packagesList)
                                                            <option value="{{ $packagesList->id }}"
                                                                @if ($packaging_package->package->id == $packagesList->id) selected @endif>
                                                                {{ $packagesList->name }}
                                                                ({{ $packagesList->packagetype->name }})
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <button class="btn btn-warning mt-2 w-100" type="submit"
                                                        onclick="return confirm(&quot;ต้องการเปลี่ยน Package ?&quot;)">เปลี่ยน</button>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <form action="{{ route('deletePackageInPackaging', $packaging_package->id) }}"
                                    method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn"
                                        onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                                            class='bx bx-trash bx-tada-hover bx-sm'></i>

                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
        @endforeach
    @endforeach
</table>
