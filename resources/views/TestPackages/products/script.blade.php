@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>


    <script>
        $(document).ready(function() {
            $('.morePackaging').click(function() {
                var productId = $(this).data('product-id');
                $('#packaging-list-' + productId).toggle();

                $(this).toggleClass('active');
            });
        });
    </script>
@endpush
