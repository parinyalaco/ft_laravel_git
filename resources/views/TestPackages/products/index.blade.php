@extends('TestPackages.layouts.layout')

@section('content')
    <div class="m-2">
        <h4>Prodeuct | สินค้า </h4>
    </div>

    @include('TestPackages.products.top')

    <form action="{{ route('testsProduct') }}" method="get">
        {{ csrf_field() }}

        <div class="input-group my-3">
            <input type="text" class="form-control" name="searchProduct" placeholder="ค้นหา Product. . . " required
                value="{{ $searchProduct }}">
            <a href="{{ route('testsProduct') }}" class="btn btn-warning">คืนค่า</a>
            <button type="submit" class="btn btn-primary">ค้นหา</button>
        </div>
    </form>
    <table class="table ">
        <thead>
            <tr class="table-success">
                <th>Product</th>
                <th>Procuct Description</th>
                <th>น้ำหนักรวม</th>
                <th>Shelf life</th>
                <th>Unit</th>
                <th>Detail</th>
                <th>Packaging Package</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td> {{ $product->name }}</td>
                    <td> {{ $product->product_fac or '-' }}</td>
                    <td> {{ $product->weight_with_bag or '-' }}</td>
                    <td> {{ $product->shelf_life or '-' }}</td>
                    <td> {{ $product->unit->name or '' }}</td>
                    <td> {{ $product->desc or '-' }}</td>

                    <td>
                        @include('TestPackages.products.packagingPackageAction')
                    </td>
                    <td>
                        @include('TestPackages.products.action')
                    </td>
                </tr>
                <tr class="packaging-list" id="packaging-list-{{ $product->id }}" style="display: none;">
                    <td colspan="7" style="background-color: #eeeeee">
                        @include('TestPackages.products.packageTable')
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $products->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}

    @include('TestPackages.products.script')
@endsection
