<div class="btn-group my-2">
    <button type="button" class="btn btn-outline-success" data-bs-toggle="modal"
        data-bs-target="#packageModal-{{ $product->id }}-new">
        เพิ่ม version ใหม่
    </button>

    @if (count($product->packagings) > 0)
        <a class="morePackaging btn btn-outline-primary" data-product-id="{{ $product->id }}">
            <i class='bx bxs-package'></i> Packages
        </a>
        {{-- <a href="{{ route('testsPackageByProduct', $product->id) }}" class="btn btn-outline-secondary">
            ดู package ตาม product</a> --}}
    @endif
</div>

<div class="modal fade" id="packageModal-{{ $product->id }}-new" tabindex="-1" aria-labelledby="detailModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">เพิ่ม Package ใหม่
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="tabA" data-bs-toggle="tab"
                            data-bs-target="#contentpackageA-{{ $product->id }}-new" type="button" role="tab"
                            aria-controls="contentA" aria-selected="true">เพิ่ม
                            Package ใหม่</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="tabB" data-bs-toggle="tab"
                            data-bs-target="#contentpackageB-{{ $product->id }}-new" type="button" role="tab"
                            aria-controls="contentB" aria-selected="false">เลือก package </button>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content mt-2">
                    <div class="tab-pane fade show active" id="contentpackageA-{{ $product->id }}-new">
                        <h5>เพิ่ม Package ใหม่</h5>

                        <form action="{{ route('addNewPackage', [$product->id, 'new', 'new']) }}" method="POST">
                            {{ csrf_field() }}

                            <label>Package Type </label>
                            <select name="package_type_id" class="form-select form-control-sm">
                                @foreach ($package_types as $package_type)
                                    <option value="{{ $package_type->id }}">
                                        {{ $package_type->name }}</option>
                                @endforeach
                            </select>

                            <label>Package Name</label>
                            <input type="text" name="name" class="form-control form-control-sm">

                            <label>Package Description</label>
                            <input type="text" name="desc" class="form-control form-control-sm">

                            <label>Package Size</label>
                            <input type="text" name="package-size" class="form-control form-control-sm">

                            <label>วันที่ผลิต</label>
                            <div class="input-group mb-3">
                                <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active">

                                <input name="mfg_front" type="text"
                                    class="form-control form-control-sm on_bag_MFG_stamp mx-1">


                                <div class="on_bag_MFG_stamp">
                                    <input type="radio" name="mfg_era" value="AD"
                                        class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                    ค.ศ.
                                    <input type="radio" name="mfg-era" value="BE"
                                        class="form-check-input on_bag_MFG_stamp mx-1">
                                    พ.ศ.
                                </div>

                                <select name="mfg_stampDateFormat"
                                    class="form-select on_bag_MFG_stamp mx-1 form-select-sm addPackage-select2"
                                    data-ProductId="{{ $product->id }}" style="width: 200px">
                                    @foreach ($stamp_date_formats as $id => $name)
                                        <option value="{{ $id }}">
                                            {{ $name }}</option>
                                    @endforeach
                                </select>

                                <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" value="on"
                                    name="mfg_lot">
                                <label class="form-check-label">
                                    Lot
                                </label>
                            </div>
                            <label>วันที่หมดอายุ</label>
                            <div class="input-group mb-3">
                                <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active">

                                <input name="exp_front" type="text"
                                    class="form-control form-control-sm on_bag_MFG_stamp mx-1">


                                <div class="on_bag_MFG_stamp">
                                    <input type="radio" name="exp_era" value="AD"
                                        class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                    ค.ศ.
                                    <input type="radio" name="exp_era" value="BE"
                                        class="form-check-input on_bag_MFG_stamp mx-1">
                                    พ.ศ.
                                </div>

                                <select name="exp_stampDateFormat"
                                    class="form-select on_bag_MFG_stamp mx-1 form-select-sm addPackage-select2"
                                    data-ProductId="{{ $product->id }}" style="width: 200px">
                                    @foreach ($stamp_date_formats as $id => $name)
                                        <option value="{{ $id }}">
                                            {{ $name }}</option>
                                    @endforeach
                                </select>

                                <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" value="on"
                                    name="exp_lot">
                                <label class="form-check-label">
                                    Lot
                                </label>
                            </div>


                            <label>Sap Note</label>
                            <input type="text" name="sapnote" class="form-control form-control-sm">
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <label>น้ำหนักต่อถุง (g) (กรัม)</label>
                                    <input type="number" name="inner_weight_g" class="form-control form-control-sm"
                                        step="0.001" placeholder="ตัวอย่าง: 410">
                                </div>
                                <div class="col">
                                    <label>น้ำหนักต่อถุง (g) (กรัม) (ประมาณค่า) </label>
                                    <input type="text" name="inner_weight_g_es"
                                        class="form-control form-control-sm" placeholder="ตัวอย่าง: 410 - 450 กรัม">
                                </div>
                            </div>

                            <label>น้ำหนักต่อ กล่อง (กิโลกรัม)</label>
                            <input type="number" name="outer_weight_kg" step="0.001"
                                class="form-control form-control-sm" placeholder="ตัวอย่าง: 25.05">

                            <label>จำนวนถุง/กล่อง</label>
                            <input type="number" name="number_per_pack" class="form-control form-control-sm"
                                placeholder="ตัวอย่าง: 36">

                            <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="contentpackageB-{{ $product->id }}-new">
                        <h5>เลือกจาก Package เดิม</h5>

                        <form action="{{ route('addPackagebySelect', [$product->id, 'new', 'new']) }}"
                            method="POST">
                            {{ csrf_field() }}
                            <select name="package_select" class="addPackage-select2 form-select-sm"
                                style="width: 100%" data-ProductId="{{ $product->id }}">
                                @foreach ($packages as $packagesList)
                                    <option value="{{ $packagesList->id }}">
                                        {{ $packagesList->name }} ({{ $packagesList->packagetype->name }})</option>
                                @endforeach
                            </select>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    <label>น้ำหนักต่อถุง (g) (กรัม)</label>
                                    <input type="number" name="inner_weight_g" class="form-control form-control-sm"
                                        step="0.001" placeholder="ตัวอย่าง: 410">
                                </div>
                                <div class="col">
                                    <label>น้ำหนักต่อถุง (g) (กรัม) (ประมาณค่า) </label>
                                    <input type="text" name="inner_weight_g_es"
                                        class="form-control form-control-sm" placeholder="ตัวอย่าง: 410 - 450 กรัม">
                                </div>
                            </div>

                            <label>น้ำหนักต่อ กล่อง (กิโลกรัม)</label>
                            <input type="number" name="outer_weight_kg" step="0.001"
                                class="form-control form-control-sm" placeholder="ตัวอย่าง: 25.015">

                            <label>จำนวนถุง/กล่อง</label>
                            <input type="number" name="number_per_pack" class="form-control form-control-sm"
                                placeholder="ตัวอย่าง: 36">

                            <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
