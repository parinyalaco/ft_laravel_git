<div class="btn-group">
    <button class="btn  " data-bs-toggle="modal" data-bs-target="#editProduct-{{ $product->id }}">
        <i class='bx bx-edit-alt bx-tada-hover bx-sm'></i>
    </button>
    <!-- Modal -->
    <div class="modal fade" id="editProduct-{{ $product->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">แก้ไข Product: {{ $product->name }}
                    </h5>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div class="modal-body">
                    <form action="{{ route('updateProduct', $product->id) }}" method="POST"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <label>ชื่อ Customer</label>
                        <select name="customer_id" class="addEditProduct-select2 form-select-sm" style="width: 100%"
                            data-id="{{ $product->id }}">
                            @foreach ($customers as $id => $name)
                                <option value="{{ $id }}" @if ($product->customer_id == $id) selected @endif>
                                    {{ $name }}
                                </option>
                            @endforeach
                        </select>
                        <label>Product Group</label>
                        <select name="product_group_id" class="addEditProduct-select2 form-select-sm"
                            style="width: 100%" data-id="{{ $product->id }}">
                            @foreach ($product_groups as $id => $name)
                                <option value="{{ $id }}" @if ($product->product_group_id == $id) selected @endif>
                                    {{ $name }}
                                </option>
                            @endforeach
                        </select>

                        <label>ชื่อ</label>
                        <input type="text" name="name" class="form-control form-control-sm"
                            value="{{ $product->name }}">

                        <label>Procuct Description</label>
                        <input type="text" name="product_fac" class="form-control form-control-sm"
                            value="{{ $product->product_fac }}">

                        <label>รายละเอียด</label>
                        <textarea name="desc" cols="30" rows="5" class="form-control">{{ $product->desc }}</textarea>

                        <label>SAP</label>
                        <input type="text" name="SAP"
                            class="form-control form-control-sm"value="{{ $product->SAP }}">

                        <label>shelf life</label>
                        <input type="number" name="shelf_life"
                            class="form-control form-control-sm"value="{{ $product->shelf_life }}">

                        <label>น้ำหนักรวม</label>
                        <input type="number" name="weight_with_bag"
                            class="form-control form-control-sm"value="{{ $product->weight_with_bag }}" step='0.001'>

                        <label>หน่วย</label>
                        <select name="unit_id" class="addEditProduct-select2 form-select-sm" style="width: 100%"
                            data-id="{{ $product->id }}">
                            @foreach ($units as $id => $name)
                                <option value="{{ $id }}" @if ($product->product_group_id == $id) selected @endif>
                                    {{ $name }}
                                </option>
                            @endforeach
                        </select>

                        <button class="btn btn-warning mt-2 w-100" type="submit"
                            onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">แก้ไข</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <form action="{{ route('deleteProduct', $product->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                class='bx bx-trash bx-tada-hover bx-sm'></i>

        </button>
    </form>
</div>


@push('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@1.6.2/dist/select2-bootstrap4.min.css"
        rel="stylesheet" />


    <script>
        $(document).each(function() {
            $('.changePackage-select2').each(function() {
                var packageId = $(this).data('package-id');
                $(this).select2({
                    theme: 'bootstrap-5',

                    placeholder: 'Select an option',
                    dropdownParent: $('#changePackage-' + packageId)
                });
            });


        });

        $(document).each(function() {
            $('.addEditProduct-select2').each(function() {
                var productId = $(this).data('id');
                $(this).select2({
                    theme: 'bootstrap-5',
                    placeholder: 'Select an option',
                    dropdownParent: $('#editProduct-' + productId)
                });
            });
        });


        $('.addPackage-select2').each(function() {
            var addPackageProductId = $(this).data('productid');
            $(this).select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#packageModal-' + addPackageProductId + '-new')
            });
        });

        $(document).each(function() {
            $('.addPackagebySelect-select2').each(function() {
                var dataProductId = $(this).data('product-id');
                var dataPackagetId = $(this).data('packaging-id');
                $(this).select2({
                    theme: 'bootstrap-5',
                    placeholder: 'Select an option',
                    dropdownParent: $('#detailModal-' + dataProductId + '-' + dataPackagetId)
                });
            });
        });
    </script>
@endpush
