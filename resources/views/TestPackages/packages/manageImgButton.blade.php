<button class="btn" data-bs-toggle="modal" data-bs-target="#imgManage-{{ $package->id }}" title="ดูรูปภาพ"
    @if (count($package->packagesImgs) > 0) style="color: forestgreen" @else style="color: brown" @endif>
    <i class='bx bx-image-alt bx-tada-hover bx-sm'></i>
</button>

<!-- Modal -->
<div class="modal fade" id="imgManage-{{ $package->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">จัดการรูป Package :
                    {{ $package->name }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

                @if (count($package->packagesImgs) > 0)
                    @foreach ($package->packagesImgs as $packagesImg)
                        <div class="row">
                            <div class="col">
                                <strong> ตำแหน่ง:</strong> {{ $packagesImg->name }}
                            </div>
                            <div class="col col-auto">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <button class="btn" data-toggle="modal"
                                        data-target="#package_imgModal-{{ $packagesImg->id }}">
                                        <i class='bx bx-edit-alt bx-sm'></i>
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="package_imgModal-{{ $packagesImg->id }}" tabindex="-1"
                                        role="dialog" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">แก้ไข
                                                        {{ $packagesImg->name }}</h5>
                                                    <button type="button" class="btn close btn-close"
                                                        data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                                <form action="{{ route('updatePackageImg', $packagesImg->id) }}"
                                                    method="post" enctype="multipart/form-data">
                                                    <div class="modal-body">
                                                        {{ method_field('PUT') }}
                                                        {{ csrf_field() }}
                                                        <label>ตำแหน่ง</label>
                                                        <input type="text" name="name" class="form-control"
                                                            value="{{ $packagesImg->name }}">

                                                        <label>รูปหลัก</label>
                                                        <br>

                                                        @if ($packagesImg->main_img_path)
                                                            <img id="blah"
                                                                src="{{ asset('storage/' . $packagesImg->main_img_path) }}"
                                                                class="img-fluid my-2 mx-auto d-block"
                                                                data-id="{{ $packagesImg->main_img_path }}"
                                                                width="150px">
                                                        @else
                                                            <div
                                                                class="alert alert-danger w-50 mx-auto text-center MT-2">
                                                                ไม่มีรูปภาพ
                                                            </div>
                                                        @endif

                                                        <input type="file" class="form-control"
                                                            data-id="{{ $packagesImg->main_img_path }}"
                                                            onchange="readURL(this);" accept="image/*"
                                                            name="main_img_path">

                                                        <label>รูป stamp</label>

                                                        @if ($packagesImg->stamp_img_paht)
                                                            <img id="blah" class="mx-auto d-block my-2"
                                                                src="{{ asset('storage/' . $packagesImg->stamp_img_paht) }}"
                                                                data-id="{{ $packagesImg->stamp_img_paht }}"
                                                                alt="your image" width="150px" />
                                                        @else
                                                            <div
                                                                class="alert alert-danger w-50 mx-auto text-center MT-2">
                                                                ไม่มีรูปภาพ
                                                            </div>
                                                        @endif


                                                        <input type='file' onchange="readURL(this);"
                                                            name="stamp_img_paht" class="img-fluid form-control"
                                                            data-id="{{ $packagesImg->stamp_img_paht }}"
                                                            accept="image/*" />

                                                        <label>Stamp Format</label>
                                                        <input type="text" name="stamp_format" class="form-control"
                                                            value="{{ $packagesImg->stamp_format }}">

                                                        <label>รายละเอียด</label>
                                                        <textarea name="detail" cols="5" rows="10" class="form-control" style="height: 100px">{{ $packagesImg->detail }}</textarea>


                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">ปิด</button>
                                                        <button type="submit" class="btn btn-primary">บันทึก</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <form action="{{ route('deletePackageImg', $packagesImg->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn"
                                            onclick="return confirm(&quot;ต้องการลบ ?&quot;)">
                                            <i class='bx bx-trash bx-tada-hover bx-sm'></i>

                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <strong> Stamp Format:</strong> {{ $packagesImg->stamp_format }}
                        <strong>รายละเอียด:</strong> {{ $packagesImg->detail }}
                        <div class="modal-img-container">

                            @if ($packagesImg->main_img_path)
                                <a type="button" data-toggle="modal"
                                    data-target="#main_img_path{{ $packagesImg->id }}">
                                    <img src="{{ asset('storage/' . $packagesImg->main_img_path) }}" width="150px">
                                </a>
                                <!-- main_img_path Modal -->
                                <div class="modal fade" id="main_img_path{{ $packagesImg->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    รูปหลัก</h5>
                                                <button type="button" class="close btn-close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="{{ asset('storage/' . $packagesImg->main_img_path) }}"
                                                    width="750px">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert alert-danger w-50 mx-auto text-center MT-2">
                                    ไม่มีรูปภาพ
                                </div>
                            @endif

                            @if ($packagesImg->stamp_img_paht)
                                <a type="button" data-toggle="modal"
                                    data-target="#stamp_img_paht{{ $packagesImg->id }}">
                                    <img src="{{ asset('storage/' . $packagesImg->stamp_img_paht) }}" width="150px">
                                </a>
                                <!-- stamp_img_paht Modal -->
                                <div class="modal fade" id="stamp_img_paht{{ $packagesImg->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    รูป Stamp </h5>
                                                <button type="button" class="close btn-close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="{{ asset('storage/' . $packagesImg->stamp_img_paht) }}"
                                                    width="750px">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="alert alert-danger w-50 mx-auto text-center MT-2">
                                    ไม่มีรูปภาพ
                                </div>
                            @endif
                            <hr>
                        </div>
                    @endforeach
                @endif

                <button class="btn btn-primary my-2" type="button" data-toggle="collapse"
                    data-target="#collapseAddPackageImg-{{ $package->id }}" aria-expanded="false"
                    aria-controls="collapseExample">
                    เพิ่มรูป Package
                </button>
                <div class="collapse" id="collapseAddPackageImg-{{ $package->id }}">
                    <div class="card card-body">
                        <form action="{{ route('storePackageImg', $package->id) }}" method="post"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <label>ตำแหน่ง</label>
                            <input type="text" name="name" class="form-control">

                            <label>รูปหลัก</label>


                            <img id="blah" class="img-fluid my-2 mx-auto d-block"
                                data-id="{{ $package->id }}package_main_imgs" width="150px">

                            <input type="file" class="form-control"
                                data-id="{{ $package->id }}package_main_imgs" onchange="readURL(this);"
                                accept="image/*" name="package_main_imgs">

                            <label>รูป stamp</label>

                            <img id="blah" class="img-fluid my-2 mx-auto d-block"
                                data-id="{{ $package->id }}package_stamp_imgs" width="150px">

                            <input type="file" class="form-control"
                                data-id="{{ $package->id }}package_stamp_imgs" onchange="readURL(this);"
                                accept="image/*" name="package_stamp_imgs">

                            <label>Stamp Format</label>
                            <input type="text" name="stamp_format" class="form-control">

                            <label>รายละเอียด</label>

                            <textarea name="detail" cols="5" rows="10" class="form-control" style="height: 100px"></textarea>

                            <button type="submit" class="btn btn-success my-2 w-100">
                                เพิ่มรูป
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
