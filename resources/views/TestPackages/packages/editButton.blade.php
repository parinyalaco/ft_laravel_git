<button class="btn" data-bs-toggle="modal" data-bs-target="#editPackage-{{ $package->id }}" title="แก้ไข">
    <i class='bx bx-edit-alt bx-tada-hover bx-sm'></i>
</button>
<!-- Modal -->
<div class="modal fade" id="editPackage-{{ $package->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">แก้ไข Package :
                    {{ $package->name }}</h5>

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form action="{{ route('updatePackages', $package->id) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <label>Package Type</label>

                    <select name="package_type_id" class="addEditPackage-select2" style="width: 100%"
                        data-id="{{ $package->id }}">
                        @foreach ($package_types as $package_type)
                            <option value="{{ $package_type->id }}" @if ($package->package_type_id == $package_type->id) selected @endif>
                                {{ $package_type->name }}
                            </option>
                        @endforeach
                    </select>
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $package->name }}">

                    <label>Description</label>
                    <textarea name="desc" cols="30" rows="5" class="form-control">{{ $package->desc }}</textarea>

                    <label>Size</label>
                    <input type="text" name="size" class="form-control" value="{{ $package->size }}">

                    {{-- stamp Format --}}
                    <label>วันที่ผลิต บน @if (!empty(config('myconfig.head_column.' . $package->packagetype->name)))
                            {{ config('myconfig.head_column.' . $package->packagetype->name) }}
                        @else
                            {{ $package->packagetype->name }}
                        @endif
                    </label>

                    @if (isset($package->stampFormat->mfg))
                        <div class="input-group mb-3">
                            <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active"
                                @if ($package->stampFormat->mfg->status == 'Active') checked @endif>

                            <input name="mfg_front" type="text"
                                class="form-control form-control-sm on_bag_MFG_stamp mx-1"
                                value="{{ $package->stampFormat->mfg->front_text }}">


                            <div class="on_bag_MFG_stamp">
                                <input type="radio" name="mfg_era" value="AD"
                                    class="form-check-input on_bag_MFG_stamp mx-1"
                                    @if ($package->stampFormat->mfg->date_era_format == 'AD') checked @endif>
                                ค.ศ.
                                <input type="radio" name="mfg_era" value="BE"
                                    class="form-check-input on_bag_MFG_stamp mx-1"
                                    @if ($package->stampFormat->mfg->date_era_format == 'BE') checked @endif>
                                พ.ศ.
                            </div>

                            <select name="mfg_stampDateFormat"
                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm addEditPackage-select2"
                                data-id="{{ $package->id }}" style="width: 200px">
                                @foreach ($stamp_date_formats as $id => $name)
                                    <option value="{{ $id }}"
                                        @if ($package->stampFormat->mfg->stamp_date_format_id == $id) selected @endif>
                                        {{ $name }}</option>
                                @endforeach
                            </select>

                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" name="mfg_lot"
                                @if ($package->stampFormat->mfg->lot == 'on') checked @endif>
                            <label class="form-check-label">
                                Lot
                            </label>
                        </div>
                    @else
                        <div class="input-group mb-3">
                            <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active">

                            <input name="mfg_front" type="text"
                                class="form-control form-control-sm on_bag_MFG_stamp mx-1">

                            <div class="on_bag_MFG_stamp">
                                <input type="radio" name="mfg_era" value="AD"
                                    class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                ค.ศ.
                                <input type="radio" name="mfg_era" value="BE"
                                    class="form-check-input on_bag_MFG_stamp mx-1">
                                พ.ศ.
                            </div>

                            <select name="mfg_stampDateFormat"
                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm addEditPackage-select2"
                                data-id="{{ $package->id }}" style="width: 200px">
                                @foreach ($stamp_date_formats as $id => $name)
                                    <option value="{{ $id }}">
                                        {{ $name }}</option>
                                @endforeach
                            </select>

                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" name="mfg_lot">
                            <label class="form-check-label">
                                Lot
                            </label>
                        </div>
                    @endif
                    <label>วันที่หมดอายุ บน
                        @if (!empty(config('myconfig.head_column.' . $package->packagetype->name)))
                            {{ config('myconfig.head_column.' . $package->packagetype->name) }}
                        @else
                            {{ $package->packagetype->name }}
                        @endif

                    </label>
                    @if (isset($package->stampFormat->exp))

                        <div class="input-group mb-3">
                            <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active"
                                @if ($package->stampFormat->exp->status == 'Active') checked @endif>

                            <input name="exp_front" type="text"
                                class="form-control form-control-sm on_bag_MFG_stamp mx-1"
                                value="{{ $package->stampFormat->exp->front_text }}">


                            <div class="on_bag_MFG_stamp">
                                <input type="radio" name="exp_era" value="AD"
                                    class="form-check-input on_bag_MFG_stamp mx-1"
                                    @if ($package->stampFormat->exp->date_era_format == 'AD') checked @endif>
                                ค.ศ.
                                <input type="radio" name="exp_era" value="BE"
                                    class="form-check-input on_bag_MFG_stamp mx-1"
                                    @if ($package->stampFormat->exp->date_era_format == 'BE') checked @endif>
                                พ.ศ.
                            </div>

                            <select name="exp_stampDateFormat"
                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm addEditPackage-select2"
                                data-id="{{ $package->id }}" style="width: 200px">
                                @foreach ($stamp_date_formats as $id => $name)
                                    <option value="{{ $id }}"
                                        @if ($package->stampFormat->exp->stamp_date_format_id == $id) selected @endif>
                                        {{ $name }}</option>
                                @endforeach
                            </select>

                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" name="exp_lot"
                                @if ($package->stampFormat->exp->lot == 'on') checked @endif>
                            <label class="form-check-label">
                                Lot
                            </label>
                        </div>
                    @else
                        <div class="input-group mb-3">
                            <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active">

                            <input name="exp_front" type="text"
                                class="form-control form-control-sm on_bag_MFG_stamp mx-1">


                            <div class="on_bag_MFG_stamp">
                                <input type="radio" name="exp_era" value="AD"
                                    class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                ค.ศ.
                                <input type="radio" name="exp_era" value="BE"
                                    class="form-check-input on_bag_MFG_stamp mx-1">
                                พ.ศ.
                            </div>

                            <select name="exp_stampDateFormat"
                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm addEditPackage-select2"
                                data-id="{{ $package->id }}" style="width: 200px">
                                @foreach ($stamp_date_formats as $id => $name)
                                    <option value="{{ $id }}">
                                        {{ $name }}</option>
                                @endforeach
                            </select>

                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" name="exp_lot">
                            <label class="form-check-label">
                                Lot
                            </label>
                        </div>
                    @endif

                    <label>วันที่หมดอายุ บน
                        @if (!empty(config('myconfig.head_column.' . $package->packagetype->name)))
                            {{ config('myconfig.head_column.' . $package->packagetype->name) }}
                        @else
                            {{ $package->packagetype->name }}
                        @endif

                    </label>


                    <label>Sapnote</label>
                    <input type="text" name="sapnote" class="form-control" value="{{ $package->sapnote }}">

                    <button class="btn btn-warning mt-2 w-100"
                        onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">แก้ไข</button>
                </form>
            </div>

        </div>
    </div>
</div>


@push('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@1.6.2/dist/select2-bootstrap4.min.css"
        rel="stylesheet">
    <script>
        $('.addEditPackage-select2').each(function() {
            var packageId = $(this).data('id');
            $(this).select2({
                placeholder: 'Select an option',
                theme: 'bootstrap-5',
                dropdownParent: $('#editPackage-' + packageId)
            });
        });
    </script>
@endpush
