@extends('TestPackages.layouts.layout')
@push('styles')
    <style>
        .modal-img-container {
            text-align: center;
        }

        .modal-img-container img {
            max-width: 100%;
            height: auto;
            display: inline-block;
            vertical-align: middle;
        }
    </style>
@endpush

@section('content')
    <div class="m-2">
        <h4>Package | แพ็คเก็จ </h4>
    </div>

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addNewPackageModal">
        เพิ่ม package
    </button>
    <!-- Modal -->
    <div class="modal fade" id="addNewPackageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">เพิ่ม Package</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('storePacgakes') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <label>Package Type</label>
                        <select name="package_type_id" class="addNewPackageSelect2" style="width: 100%">
                            @foreach ($package_types as $package_type)
                                <option value="{{ $package_type->id }}">{{ $package_type->name }}</option>
                            @endforeach
                        </select>

                        <label>Name</label>
                        <input type="text" name="name" class="form-control">

                        <label>Description</label>
                        <input type="text" name="desc" class="form-control">

                        <label>Size</label>
                        <input type="text" name="size" class="form-control">

                        <label>วันที่ผลิต</label>
                        <div class="input-group mb-3">
                            <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active">

                            <input name="mfg_front" type="text"
                                class="form-control form-control-sm on_bag_MFG_stamp mx-1">


                            <div class="on_bag_MFG_stamp">
                                <input type="radio" name="mfg_era" value="AD"
                                    class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                ค.ศ.
                                <input type="radio" name="mfg-era" value="BE"
                                    class="form-check-input on_bag_MFG_stamp mx-1">
                                พ.ศ.
                            </div>

                            <select name="mfg_stampDateFormat"
                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm addNewPackageSelect2" style="width: 200px">
                                @foreach ($stamp_date_formats as $id => $name)
                                    <option value="{{ $id }}">
                                        {{ $name }}</option>
                                @endforeach
                            </select>

                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" value="on"
                                name="mfg_lot">
                            <label class="form-check-label">
                                Lot
                            </label>
                        </div>
                        <label>วันที่หมดอายุ</label>
                        <div class="input-group mb-3">
                            <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active">

                            <input name="exp_front" type="text"
                                class="form-control form-control-sm on_bag_MFG_stamp mx-1">


                            <div class="on_bag_MFG_stamp">
                                <input type="radio" name="exp_era" value="AD"
                                    class="form-check-input on_bag_MFG_stamp mx-1" checked>
                                ค.ศ.
                                <input type="radio" name="exp_era" value="BE"
                                    class="form-check-input on_bag_MFG_stamp mx-1">
                                พ.ศ.
                            </div>

                            <select name="exp_stampDateFormat"
                                class="form-select on_bag_MFG_stamp mx-1 form-select-sm addNewPackageSelect2" style="width: 200px">
                                @foreach ($stamp_date_formats as $id => $name)
                                    <option value="{{ $id }}">
                                        {{ $name }}</option>
                                @endforeach
                            </select>

                            <input class="form-check-input mx-1 on_bag_MFG_stamp" type="checkbox" value="on"
                                name="exp_lot">
                            <label class="form-check-label">
                                Lot
                            </label>
                        </div>

                        <label>Sapnote</label>
                        <input type="text" name="sapnote" class="form-control">

                        <button class="btn btn-success mt-2 w-100">เพิ่ม Package</button>
                    </form>

                </div>

            </div>
        </div>
    </div>
    <form action="{{ route('testsPackages') }}" method="get">
        {{ csrf_field() }}

        <div class="input-group my-3">
            <input type="text" class="form-control" name="searchPackage" placeholder="ค้นหา Package. . . " required
                value="{{ $searchPackage }}">
            <a href="{{ route('testsPackages') }}" class="btn btn-warning">คืนค่า</a>
            <button type="submit" class="btn btn-primary">ค้นหา</button>
        </div>
    </form>

    <table class="table ">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Size</th>
                <th>Stamp Format</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($packages as $package)
                <tr>
                    <td>{{ $package->name }}</td>
                    <td>{{ $package->desc }}</td>
                    <td>{{ $package->size }}</td>
                    <td>

                    </td>
                    <td>{{ $package->status }}</td>

                    <td>
                        <div class="btn-group">
                            {{-- Mange Image Button --}}
                            @include('TestPackages.packages.manageImgButton')

                            {{-- view Button --}}
                            <a href="{{ route('viewPackage', $package->id) }}" class="btn" title="ดูรายละเอียด"> <i
                                    class='bx bxs-detail bx-tada-hover bx-sm'></i></a>


                            {{-- Edit Button --}}
                            @include('TestPackages.packages.editButton')

                            {{-- Delete Button --}}
                            <form action="{{ route('deletePackages', $package->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)"
                                    title="ลบ">
                                    <i class='bx bx-trash bx-tada-hover bx-sm'></i>

                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
    {{ $packages->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}
    <hr>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@1.6.2/dist/select2-bootstrap4.min.css"
        rel="stylesheet" />

    <script>
        $('.addNewPackageSelect2').select2({
            placeholder: 'Select an option',
            theme: 'bootstrap-5',

            dropdownParent: '#addNewPackageModal'
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var dataId = $(input).data('id');
                    $('img[data-id="' + dataId + '"]').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
