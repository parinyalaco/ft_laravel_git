@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

<div class="row">
    <div class="col">

        <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#addNewCustomerModal">
            เพิ่ม Customer
        </button>
        <!-- Modal -->
        <div class="modal fade" id="addNewCustomerModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addNewCustomerModal">เพิ่ม Customer</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <!-- Modal content goes here -->
                        <form action="{{ route('addNewCustomer') }}" method="POST">
                            {{ csrf_field() }}
                            <label>ชื่อ Customer</label>
                            <input type="text" name="name" class="form-control">

                            <label>รายละเอียด</label>
                            <input type="text" name="desc" class="form-control">

                            <button class="btn btn-success my-2 w-100">บันทึก</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


@push('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $('.addNewProduct-select2').select2({
            placeholder: 'Select an option',
            dropdownParent: '#addNewProductModal'
        });
    </script>
@endpush
