@extends('layouts.appweightchart')

@section('content')
    <div class="container">
        <div class="row">

            @foreach ($data2 as $key => $item)
                <div class="col-md-12">
                    <div style="border: 1px solid #9e9e9e ; padding: 10px; border-radius: 20px;">

                        <h1 style="text-align: center"> {{ $mlist[$key] }} of {{ date('d M Y') }}</h1>
                        <canvas id="my_Chart{{ $key }}" height=22vh" width="90vw"></canvas>
                    </div>
                    <br>
                </div>
            @endforeach

        </div>
    </div>

    <script>
        setTimeout(function() {
            location.reload();
        }, 300000);
    </script>


    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>

    <script>
        @foreach ($data2 as $key => $item)
            var myData{{ $key }} = {
                labels: [
                    @foreach ($item as $key3 => $item3)
                        '{{ substr($key3, 0, 16) }}',
                    @endforeach
                ],
                datasets: [
                    @php
                        $chekdata = 1;
                    @endphp
                    @foreach ($data[$key] as $key11 => $item11)
                        {
                            label: '{{ $key11 }}',
                            data: [
                                @foreach ($item as $key3 => $item3)
                                    @if (isset($item3[$key11]))
                                        {{ $item3[$key11] }},
                                    @else
                                        0,
                                    @endif
                                @endforeach
                            ],
                            fill: false,
                            borderColor: 'rgb({{ $chekdata * $key * 25 }}, {{ 255 - $chekdata * $key * 25 }},{{ $chekdata * 50 - $key * 25 }})',
                            tension: 0.1,
                            datalabels: {
                                anchor: 'end',
                                align: 'top',
                                formatter: (value) => value,
                                font: {
                                    weight: 'bold'
                                },
                                color: '#000'
                            }
                        },
                        @php
                            $chekdata++;
                        @endphp
                    @endforeach
                ]
            };

            var maxYValue = Math.max(
                @foreach ($item as $key3 => $item3)
                    @foreach ($data[$key] as $key11 => $item11)
                        @if (isset($item3[$key11]))
                            {{ $item3[$key11] }},
                        @else
                            0,
                        @endif
                    @endforeach
                @endforeach
            );

            var myoption = {
                responsive: true,
                plugins: {
                    datalabels: {
                        display: true,
                        anchor: 'end',
                        align: 'top',
                        formatter: (value) => value,
                        font: {
                            weight: 'bold'
                        },
                        color: '#000'
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        min: 0, // กำหนดค่าต่ำสุดของแกน Y
                        suggestedMax: maxYValue + 10, // กำหนดค่าสูงสุดของแกน Y เพิ่มขึ้น 10 หน่วยจากค่า maxYValue
                        ticks: {
                            stepSize: 10 // ระยะห่างระหว่างค่าของแต่ละ tick
                        }
                    }
                }
            };

            var ctx = document.getElementById('my_Chart{{ $key }}').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: myData{{ $key }},
                options: myoption,
                plugins: [ChartDataLabels] // ใส่ plugin ที่นี่
            });
        @endforeach
    </script>
@endsection
