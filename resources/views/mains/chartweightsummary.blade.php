@extends('layouts.appweightchart')

@section('content')
    <div class="container">
 
        <div class="row">
            @foreach ($data as $key => $item)
                <div style="border: 1px solid #9e9e9e ; padding: 10px; border-radius: 20px;">
                    <div class="col-md-12">
                        <h1 style="text-align: center"> {{ $mlist[$key] }} of {{ date('d M Y') }}</h1>
                        <canvas id="my_Chart{{ $key }}" height=22vh" width="90vw"></canvas>
                        <hr>
                    </div>

                    @foreach ($item as $key1 => $item1)
                        <h3 style="margin: 15px 10px;"> แพ็ค: {{ $key1 }} ไปแล้วจำนวน

                            @if (isset($packages[explode('#', $key1)[0]]) && (isset($item1['OK']) || isset($item1['NG'])))
                                @if (isset($item1['OK']) && isset($item1['NG']))
                                    {{ number_format($packages[explode('#', $key1)[0]]->numperbox * ($item1['OK'] + $item1['NG']), '0', '', ',') }}
                                    ถุง
                                @else
                                    @if (isset($item1['OK']))
                                        {{ number_format($packages[explode('#', $key1)[0]]->numperbox * $item1['OK'], '0', '', ',') }}
                                        ถุง
                                    @else
                                        {{ number_format($packages[explode('#', $key1)[0]]->numperbox * $item1['NG'], '0', '', ',') }}
                                        ถุง
                                    @endif
                                @endif
                            @else
                                @if (isset($item1['OK']))
                                    {{ $item1['OK'] }} กล่อง
                                @else
                                    -
                                @endif
                            @endif

                        </h3>
                    @endforeach

                </div>
                <br>
            @endforeach

        </div>

    </div>

    <script>
        setTimeout(function() {
            location.reload();
        }, 300000);
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>

    <script>
        @foreach ($data as $key => $item)

            // คำนวณค่าสูงสุดของแกน Y
            var maxYValue = Math.max(
                @foreach ($item as $key1 => $item1)
                    {{ $item1['OK'] or 0 }},
                    {{ $item1['NG'] or 0 }},
                    {{ $item1['NC'] or 0 }},
                @endforeach
            );

            var myData{{ $key }} = {
                labels: [
                    @foreach ($item as $key1 => $item1)
                        '{{ $key1 }}',
                    @endforeach
                ],
                datasets: [{
                        label: 'OK',
                        data: [
                            @foreach ($item as $key1 => $item1)
                                {{ $item1['OK'] or 0 }},
                            @endforeach
                        ],
                        fill: false,
                        backgroundColor: 'rgba(0, 255, 0, 0.9)',
                        borderColor: 'rgb(0, 255, 0)',
                        tension: 0.1
                    },
                    {
                        label: 'NG',
                        data: [
                            @foreach ($item as $key1 => $item1)
                                {{ $item1['NG'] or 0 }},
                            @endforeach
                        ],
                        fill: false,
                        backgroundColor: 'rgba(0, 0, 255, 0.9)',
                        borderColor: 'rgb(0, 0, 255)',
                        tension: 0.1
                    },
                    {
                        label: 'NC',
                        data: [
                            @foreach ($item as $key1 => $item1)
                                {{ $item1['NC'] or 0 }},
                            @endforeach
                        ],
                        fill: false,
                        backgroundColor: 'rgba(255, 0, 0, 0.9)',
                        borderColor: 'rgb(255, 0, 0)',
                        tension: 0.1
                    }
                ]
            };

            var myoption = {
                responsive: true,
                plugins: {
                    legend: {
                        display: true,
                        position: 'bottom' // กำหนดตำแหน่งของ Legend ให้อยู่ด้านล่าง
                    },
                    datalabels: {
                        anchor: 'end',
                        align: 'top',
                        formatter: (value, context) => value,
                        font: {
                            weight: 'bold'
                        }
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        suggestedMax: maxYValue + 20 // กำหนดให้แกน y แสดงค่าสูงสุดที่มากกว่าค่าที่แสดงอยู่สองค่า
                    }
                }
            };

            var ctx = document.getElementById('my_Chart{{ $key }}').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: myData{{ $key }},
                options: myoption,
                plugins: [ChartDataLabels] // ใส่ plugin ที่นี่
            });
        @endforeach
    </script>
@endsection
