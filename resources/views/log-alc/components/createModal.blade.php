<div>
    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal">
        <i class="fa fa-plus" aria-hidden="true"></i> สร้างรายการใหม่
    </button>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">


        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">บันทึกข้อมูลแอลกอฮอล์ คลอรีน แตก</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('log-alc.store') }}" method="POST" accept-charset="UTF-8"
                    class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <label>วันผลิต</label>
                        <input type="date" class="form-control" name="production_date" value="{{ date('Y-m-d') }}"
                            required>

                        <label>กะ</label>
                        <select name="onshift" class="form-control">
                            <option value="B">B</option>
                            <option value="C">C</option>
                        </select>

                        <label>แผนกงาน</label>
                        <select name="department" class="form-control">
                            <option value="PK">PK</option>
                            <option value="PF">PF</option>
                            <option value="RTE">RTE</option>
                            <option value="FS">FS</option>
                        </select>

                        <label>Note</label>
                        <textarea name="note" cols="30" rows="5" class="form-control"></textarea>
                        <h4><input type="checkbox" id="toggleAlc" name="Alc_type"><label> กระปุกแอลกอฮอล์</label></h4>

                        <div class="row" id="alcFormFields" style="display: none;">
                            <div class="col-md-4">
                                <label for="quantity">จำนวนที่ใช้</label>
                                <input type="number" class="form-control" name="Alc_amount_used" value="0">
                            </div>
                            <div class="col-md-4">
                                <label for="amount">จำนวนที่แตก</label>
                                <input type="number" class="form-control" name="Alc_amount_broked" value="0">
                            </div>
                            <div class="col-md-4">
                                <label for="cause">สาเหตุที่ชำรุด</label>
                                <input type="text" class="form-control" name="Alc_details">
                            </div>
                        </div>
                        <hr>
                        <h4><input type="checkbox" id="toggleChlorine" name="Cl_type"><label>&nbsp; กระปุกคลอรีน</label>
                        </h4>

                        <div class="row" id="chlorineFormFields" style="display: none;">
                            <div class="col-md-4">
                                <label for="quantity">จำนวนที่ใช้</label>
                                <input type="number" class="form-control" name="Cl_amount_used" value="0">
                            </div>
                            <div class="col-md-4">
                                <label for="amount">จำนวนที่แตก</label>
                                <input type="number" class="form-control" name="Cl_amount_broked" value="0">
                            </div>
                            <div class="col-md-4">
                                <label for="cause">สาเหตุที่ชำรุด</label>
                                <input type="text" class="form-control" name="Cl_details">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                        <button type="submit" class="btn btn-primary">บันทึก</button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        document.getElementById("toggleChlorine").onchange = function() {
            var formFields = document.getElementById("chlorineFormFields");
            if (this.checked) {
                formFields.style.display = "block";

                var inputs = formFields.querySelectorAll('input');
                inputs.forEach(function(input) {
                    input.disabled = false;
                });
            } else {
                formFields.style.display = "none";

                var inputs = formFields.querySelectorAll('input');
                inputs.forEach(function(input) {
                    input.disabled = true;
                });
            }
        };

        document.getElementById("toggleAlc").onchange = function() {
            var formFields = document.getElementById("alcFormFields");
            if (this.checked) {
                formFields.style.display = "block";
                var inputs = formFields.querySelectorAll('input');
                inputs.forEach(function(input) {
                    input.disabled = false;
                });
            } else {
                formFields.style.display = "none";
                var inputs = formFields.querySelectorAll('input');
                inputs.forEach(function(input) {
                    input.disabled = true;
                });
            }
        };
    </script>
@endpush
