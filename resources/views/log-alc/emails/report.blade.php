<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Welcome Email</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Prompt&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: "Prompt", sans-serif;

        }

        #customers {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td,
        #customers th {
            border: 1px solid #ddd;
            padding: 5px;
        }

        #customers tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #customers tr:hover {
            background-color: #ddd;
        }

        #customers th {
            padding-top: 10px;
            padding-bottom: 10px;
            text-align: left;
            background-color: #04AA6D;
            color: white;
        }
    </style>
</head>

<body>
    <h1>รายงาน ข้อมูลแอลกอฮอล์ และ คลอรีนที่ใช้ </h1>
    <p>TO..ALL</p>
    <p> รายงานผลข้อมูลแอลกอฮอล์ และ คลอรีนที่ใช้ วันที่ {{ date('d/m/Y', strtotime($yesterday)) }} รายละเอียดดังนี้</p>


    <table id="customers">
        <thead>
            <tr>
                <th>วันที่</th>
                <th>กะ</th>
                <th>แผนก</th>
                <th>Note</th>
                <th>ชนิด</th>
                <th>จำนวนที่ใช้</th>
                <th>จำนวนที่แตก</th>
                <th>รายละเอียด</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($log_alcs as $log_alc)
                <tr>
                    {{-- <td>{{ $log_alc->id ?? '-' }}</td> --}}
                    <td>{{ date('d/m/Y', strtotime($log_alc->production_date)) }}</td>
                    <td>{{ $log_alc->onshift ?? '-' }}</td>
                    <td>{{ $log_alc->department ?? '-' }}</td>
                    <td>{{ $log_alc->note ?? '-' }}</td>
                    <td>{{ $log_alc->type ?? '-' }}</td>
                    <td>{{ $log_alc->amount_used ?? '-' }}</td>
                    <td>{{ $log_alc->amount_broked ?? '-' }}</td>
                    <td>{{ $log_alc->details ?? '-' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p>ขอบคุณครับ</p>
    <p>FT Alert</p>

</body>

</html>
