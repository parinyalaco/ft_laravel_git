@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>แก้ไขข้อมูลแอลกอฮอล์ คลอรีน แตก </h3>
                    </div>

                    <a href="{{ route('log-alc.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        <form action="{{ route('log-alc.update', $logAlc->id) }}" method="POST">

                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <div class="modal-body">
                                <label>วันผลิต</label>

                                <input type="date" class="form-control" name="production_date"
                                    value="{{ date('Y-m-d', strtotime($logAlc->production_date)) }}">

                                <label>กะ</label>
                                <select name="onshift" class="form-control">
                                    <option value="B" @if (trim($logAlc->onshift) == 'B') selected @endif>B</option>
                                    <option value="C" @if (trim($logAlc->onshift) == 'C') selected @endif>C</option>
                                </select>

                                <label>แผนกงาน</label>
                                <select name="department" class="form-control">
                                    <option value="PK" @if (trim($logAlc->department) == 'PK') selected @endif>PK</option>
                                    <option value="PF" @if (trim($logAlc->department) == 'PF') selected @endif>PF</option>
                                    <option value="RTE" @if (trim($logAlc->department) == 'RTE') selected @endif>RTE</option>
                                    <option value="FS" @if (trim($logAlc->department) == 'FS') selected @endif>FS</option>
                                </select>

                                <label>Note</label>
                                <textarea name="note" cols="30" rows="5" class="form-control">{{ $logAlc->note }}</textarea>

                                <br>
                                <select name="type" class="form-control mt-2">
                                    <option value="Alc" @if (trim($logAlc->type) == 'Alc') selected @endif>กระปุกแอลกอฮอล์
                                    </option>
                                    <option value="Cl" @if (trim($logAlc->type) == 'Cl') selected @endif>กระปุกคลอรีน
                                    </option>
                                </select>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="quantity">จำนวนที่ใช้</label>
                                        <input type="number" class="form-control" name="amount_used"
                                            value="{{ $logAlc->amount_used }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="amount">จำนวนที่แตก</label>
                                        <input type="number" class="form-control" name="amount_broked"
                                            value="{{ $logAlc->amount_broked }}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="cause">สาเหตุที่ชำรุด</label>
                                        <input type="text" class="form-control" name="details"
                                            value="{{ $logAlc->details }}">
                                    </div>
                                </div>

                                <br>

                                <!-- ส่วนอื่นๆ ของฟอร์ม -->
                                <button type="submit" class="btn btn-primary">Update</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
