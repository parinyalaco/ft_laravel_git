@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3>บันทึกข้อมูลแอลกอฮอล์ คลอรีน แตก </h3>
                    </div>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @include('log-alc.components.createModal')


                        <hr>

                        <form method="GET" action="{{ url('/log-select-ms') }}" accept-charset="UTF-8"
                            class="form-inline my-2 my-lg-0 float-right" role="search">

                            <div class="input-group  mt-2">
                                <input type="text" class="form-control" name="search" placeholder="Search..."
                                    {{-- value="{{ request('search') }}" --}}>
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <br />

                        <div class="table-responsive">
                            <table class="table">

                                <thead>
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        <th>วันที่</th>
                                        <th>กะ</th>
                                        <th>แผนก</th>
                                        <th>Note</th>
                                        <th>ชนิด</th>
                                        <th>จำนวนที่ใช้</th>
                                        <th>จำนวนที่แตก</th>
                                        <th>รายละเอียด</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($log_alcs as $log_alc)
                                        <tr>
                                            {{-- <td>{{ $log_alc->id ?? '-' }}</td> --}}
                                            <td>{{ date('d/m/Y', strtotime($log_alc->production_date)) }}</td>
                                            <td>{{ $log_alc->onshift ?? '-' }}</td>
                                            <td>{{ $log_alc->department ?? '-' }}</td>
                                            <td>{{ $log_alc->note ?? '-' }}</td>
                                            <td>{{ $log_alc->type ?? '-' }}</td>
                                            <td>{{ $log_alc->amount_used ?? '-' }}</td>
                                            <td>{{ $log_alc->amount_broked ?? '-' }}</td>
                                            <td>{{ $log_alc->details ?? '-' }}</td>
                                            <td>
                                                <form action="{{ route('log-alc.destroy', $log_alc->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <a href="{{ route('log-alc.edit', $log_alc->id) }}" type="button"
                                                            class="btn btn-primary">แก้ไข</a>
                                                        <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure?')">ลบ</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                            {{-- <div class="pagination-wrapper"> {!! $log_alcs->appends(['search' => Request::get('search'), 'status' => $status])->render() !!} </div> --}}
                            <div class="pagination-wrapper"> {!! $log_alcs->Links() !!} </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
