<div class="row">
    <div class="form-group col-md-3 {{ $errors->has('process_datetime') ? 'has-error' : ''}}">
        <label for="process_datetime" class="control-label">{{ 'วัน-เวลา' }}</label>
        <input class="form-control" name="process_datetime" type="datetime-local" id="process_datetime" 
        value = "@php
            if(isset($logpackd->process_datetime)){
                echo date('Y-m-d\TH:i',strtotime($logpackd->process_datetime));
            }else{
                echo \Carbon\Carbon::now()->format('Y-m-d\TH:i');
            }
        @endphp" >
        {!! $errors->first('process_datetime', '<p class="help-block">:message</p>') !!}
    </div>
    
   
    <div class="form-group col-md-3 {{ $errors->has('workhours') ? 'has-error' : ''}}">
        <label for="workhours" class="control-label">{{ 'ชม.' }}</label>
        <input class="form-control" name="workhours" type="text" id="workhours" value="{{ $logpackd->workhours or '1' }}" >
        {!! $errors->first('workhours', '<p class="help-block">:message</p>') !!}
    </div>
<div class="form-group col-md-3 {{ $errors->has('std_rate') ? 'has-error' : ''}}">
    <label for="std_rate" class="control-label">{{ 'STD Productivity' }}</label>
    <input class="form-control" name="std_rate" type="text" readonly id="std_rate" value="{{ $logpackm->stdpack->std_rate or ''}}" >
    {!! $errors->first('std_rate', '<p class="help-block">:message</p>') !!}
</div>
    
    <div class="form-group col-md-3 {{ $errors->has('input_kg') ? 'has-error' : ''}}">
        <label for="input_kg" class="control-label">{{ 'Input (kg)' }}</label>
        <input class="form-control sumkg" name="input_kg" type="text" id="input_kg" value="{{ $logpackd->input_kg or '0' }}" >
        {!! $errors->first('input_kg', '<p class="help-block">:message</p>') !!}
    </div>
     <div class="form-group col-md-3 {{ $errors->has('output_kg') ? 'has-error' : ''}}">
        <label for="output_kg" class="control-label">{{ 'Output (kg)' }}</label>
        <input class="form-control sumkg calpack" name="output_kg" type="text" id="output_kg" value="{{ $logpackd->output_kg or '0' }}" >
        {!! $errors->first('output_kg', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-3 {{ $errors->has('input_kg_sum') ? 'has-error' : ''}}">
        <label for="input_kg_sum" class="control-label">{{ 'รวม Input (kg)' }}</label>
        <input class="form-control" name="input_kg_sum" type="text" id="input_kg_sum" value="{{ $logpackd->input_kg_sum or '0' }}" readonly>
        {!! $errors->first('input_kg_sum', '<p class="help-block">:message</p>') !!}
    </div>
     <div class="form-group col-md-3 {{ $errors->has('output_kg_sum') ? 'has-error' : ''}}">
        <label for="output_kg_sum" class="control-label">{{ 'รวม Output (kg)' }}</label>
        <input class="form-control" name="output_kg_sum" type="text" id="output_kg_sum" value="{{ $logpackd->output_kg_sum or '0' }}" readonly>
        {!! $errors->first('output_kg_sum', '<p class="help-block">:message</p>') !!}
    </div>
 <div class="form-group col-md-3 {{ $errors->has('output_pack') ? 'has-error' : ''}}">
        <label for="output_pack" class="control-label">{{ 'Output (กล่อง หรือ EA)' }}</label>
        <input class="form-control" name="output_pack" type="text" id="output_pack" value="{{ $logpackd->output_pack or '0' }}" >
        {!! $errors->first('output_pack', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-md-3 {{ $errors->has('output_pack_sum') ? 'has-error' : ''}}">
        <label for="output_pack_sum" class="control-label">{{ 'รวม Output (กล่อง หรือ EA)' }}</label>
        <input class="form-control" name="output_pack_sum" type="text" id="output_pack_sum" value="{{ $logpackd->output_pack_sum or '0' }}" readonly>
        {!! $errors->first('output_pack_sum', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group col-md-3 {{ $errors->has('num_pack') ? 'has-error' : ''}}">
        <label for="num_pack" class="control-label">{{ 'จำนวนคน' }}</label>
        <input class="form-control sumkg" name="num_pack" type="text" id="num_pack" value="{{ $logpackd->num_pack or '0' }}" >
        {!! $errors->first('num_pack', '<p class="help-block">:message</p>') !!}
    </div>

     <div class="form-group col-md-3 {{ $errors->has('productivity') ? 'has-error' : ''}}">
    <label for="productivity" class="control-label">{{ 'Productivity' }}</label>
    <input class="form-control" name="productivity" type="text" readonly id="productivity" value="{{ $logpackd->productivity or ''}}" >
    {!! $errors->first('productivity', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group col-md-3 {{ $errors->has('yeild_percent') ? 'has-error' : ''}}">
    <label for="yeild_percent" class="control-label">{{ 'Yeild %' }}</label>
    <input class="form-control" name="yeild_percent" type="text" readonly id="yeild_percent" value="{{ $logpackd->yeild_percent or ''}}" >
    {!! $errors->first('yeild_percent', '<p class="help-block">:message</p>') !!}
</div>
    <div class="form-group col-md-4 {{ $errors->has('note') ? 'has-error' : ''}}">
        <label for="note" class="control-label">{{ 'Note' }}</label>
        <input class="form-control" name="note" type="text" id="note" value="{{ $logpackd->note or '' }}" >
        <input type='hidden' name='kgsperpack' id='kgsperpack' value='{{ $logpackm->package->kgsperpack }}' >
        <input type='hidden' name='log_pack_m_id' id='log_pack_m_id' value='{{ $logpackm->id }}' >
        {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
    </div>
    
    <div class="form-group col-md-4 {{ $errors->has('problem') ? 'has-error' : ''}}">
        <label for="problem" class="control-label">{{ 'ปัญหาที่พบ' }}</label>
        <input class="form-control" name="problem"  id="problem" value="{{ $logpackd->problem or '' }}" >
        {!! $errors->first('problem', '<p class="help-block">:message</p>') !!}
    </div>
    
     <div class="form-group col-md-4 {{ $errors->has('breakdown') ? 'has-error' : ''}}">
        <label for="breakdown_min" class="control-label">{{ 'Breakdown (นาที)' }}</label>
        <input class="form-control" type="number" name="breakdown_min"  id="breakdown_min" value="{{ $logpackd->breakdown_min or '0' }}" >
        {!! $errors->first('breakdown_min', '<p class="help-block">:message</p>') !!}
    </div>
    
    <div class="form-group col-md-6 {{ $errors->has('problem_img') ? 'has-error' : ''}}">
        <label for="problem_img" class="control-label">{{ 'ภาพปัญหาที่พบ 1' }}</label>
        {!! Form::file('problem_img', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path))
            <a href="{{ url($logpackd->img_path) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img1') ? 'has-error' : ''}}">
        <label for="problem_img1" class="control-label">{{ 'ภาพปัญหาที่พบ 2' }}</label>
        {!! Form::file('problem_img1', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path1))
            <a href="{{ url($logpackd->img_path1) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path1) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img2') ? 'has-error' : ''}}">
        <label for="problem_img2" class="control-label">{{ 'ภาพปัญหาที่พบ 3' }}</label>
        {!! Form::file('problem_img2', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path2))
            <a href="{{ url($logpackd->img_path2) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path2) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img3') ? 'has-error' : ''}}">
        <label for="problem_img3" class="control-label">{{ 'ภาพปัญหาที่พบ 4' }}</label>
        {!! Form::file('problem_img3', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path3))
            <a href="{{ url($logpackd->img_path3) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path3) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img4') ? 'has-error' : ''}}">
        <label for="problem_img4" class="control-label">{{ 'ภาพปัญหาที่พบ 5' }}</label>
        {!! Form::file('problem_img4', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path4))
            <a href="{{ url($logpackd->img_path4) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path4) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img5') ? 'has-error' : ''}}">
        <label for="problem_img5" class="control-label">{{ 'ภาพปัญหาที่พบ 6' }}</label>
        {!! Form::file('problem_img5', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path5))
            <a href="{{ url($logpackd->img_path5) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path5) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img6') ? 'has-error' : ''}}">
        <label for="problem_img6" class="control-label">{{ 'ภาพปัญหาที่พบ 7' }}</label>
        {!! Form::file('problem_img6', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path6))
            <a href="{{ url($logpackd->img_path6) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path6) }}" ></a>            
        @endif          
    </div>
    <div class="form-group col-md-6 {{ $errors->has('problem_img7') ? 'has-error' : ''}}">
        <label for="problem_img7" class="control-label">{{ 'ภาพปัญหาที่พบ 8' }}</label>
        {!! Form::file('problem_img7', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']); !!}   
        @if (isset($logpackd->img_path7))
            <a href="{{ url($logpackd->img_path7) }}" target="_blank"><img height="50px" src="{{ url($logpackd->img_path7) }}" ></a>            
        @endif          
    </div>
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
