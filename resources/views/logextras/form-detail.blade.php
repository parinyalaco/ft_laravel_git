<div class="form-group col-md-4 {{ $errors->has('extra_job_id') ? 'has-error' : ''}}">
    <label for="extra_job_id" class="control-label">{{ 'งาน' }}</label>
    <select name="extra_job_id" class="form-control" id="product_id" >
    @foreach ($extrajobs as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($logextrad->extra_job_id) && $logextrad->extra_job_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('extra_job_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-2 {{ $errors->has('plan_num') ? 'has-error' : '' }}">
    <label for="plan_num" class="control-label">{{ 'แผน(คน)' }}</label>
    <input class="form-control" name="plan_num" type="number" 
        value="{{ $logextrad->plan_num or '0' }}">
    {!! $errors->first('plan_num', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-2 {{ $errors->has('act_num') ? 'has-error' : '' }}">
    <label for="act_num" class="control-label">{{ 'เข้าจริง(คน)' }}</label>
    <input class="form-control" name="act_num" type="number" 
        value="{{ $logextrad->act_num or '0' }}">
    {!! $errors->first('act_num', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-5 {{ $errors->has('note') ? 'has-error' : '' }}">
    <label for="note" class="control-label">{{ 'Note' }}</label>
    <input class="form-control" name="note" type="text" 
        value="{{ $logextrad->note or '0' }}">
    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
