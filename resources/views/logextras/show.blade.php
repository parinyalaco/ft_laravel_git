@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>งานเพิ่มเติม #{{ $logextram->id }}</h3></div>
                    <div class="card-body">

                        <a href="{{ url('/logextras') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/logextras/' . $logextram->id . '/edit') }}" title="Edit LogSelectM"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        @if ($logextram->status == 'Active' )
                          <a href="{{ url('/logextras/createDetail/' . $logextram->id ) }}" title="Add Details"><button class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add</button></a>
                        @endif
                        <br/>
                        <div class="row">
                            <div class="col-md-3">
                                <label for="id">ID</label>
                                {{ $logextram->id }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">วันที่</label>
                                {{ $logextram->process_date }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">กะ</label>
                                {{ $logextram->shift->name }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">แผนกงาน</label>
                                {{ $logextram->dep }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">note</label>
                                {{ $logextram->note }}
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>งาน</th>
                                        <th>Plan</th>
                                        <th>Actual</th>
                                        <th>หมายเหตุ ปัญหา</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($logextram->logextrads()->get() as $item)
                                       <tr>
                                            <td>{{ $item->extrajob->name }}</td>
                                            <td>{{ $item->plan_num }}</td>
                                            <td>{{ $item->act_num }}</td>
                                            <td>{{ $item->note }}
                                            @if (!empty($item->ref_note))
                                                <br/>{{ $item->ref_note }}
                                            @endif
                                            </td>
                                            <td><a href="{{ url('/logextras/editDetail/' . $item->id) }}" title="Edit LogExtraD"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            <a href="{{ url('/logextras/deleteDetail/' . $item->id . '/'. $item->log_select_m_id) }}" title="Delete LogExtraD"><button class="btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>
                                       </td>
                                        </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
