@foreach ($extrajobs as $item)
    @if ($formMode != 'edit')
        <div class="row">
            <div class="form-group col-md-3 {{ $errors->has('job') ? 'has-error' : '' }}">
                <label for="job" class="control-label">{{ 'งาน' }}</label>
                <input class="form-control" name="job[{{ $item->id }}]" type="text" 
                    value="{{ $item->name }}" readonly>
                <input class="form-control" name="extra_job_id[{{ $item->id }}]" type="hidden" 
                    value="{{ $item->id }}" readonly>
                {!! $errors->first('job', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group col-md-2 {{ $errors->has('plan_num') ? 'has-error' : '' }}">
                <label for="plan_num" class="control-label">{{ 'แผน(คน)' }}</label>
                <input class="form-control" name="plan_num[{{ $item->id }}]" type="number" 
                    value="{{ $item->default_plan or '0' }}">
                {!! $errors->first('plan_num', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group col-md-2 {{ $errors->has('act_num') ? 'has-error' : '' }}">
                <label for="act_num" class="control-label">{{ 'เข้าจริง(คน)' }}</label>
                <input class="form-control" name="act_num[{{ $item->id }}]" type="number" 
                    value="0">
                {!! $errors->first('act_num', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="form-group col-md-5 {{ $errors->has('note') ? 'has-error' : '' }}">
                <label for="note" class="control-label">{{ 'Note' }}</label>
                <input class="form-control" name="note[{{ $item->id }}]" type="text" 
                    value="">
                {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @else
        <div class="row">
            
        </div>
    @endif
@endforeach

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
