@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>งานเพิ่มเติม</h3></div>
                    <div class="card-body">
                        <a href="{{ url('/logextras/create') }}" class="btn btn-success btn-sm" title="Add New LogExtra">
                            <i class="fa fa-plus" aria-hidden="true"></i> สร้างรายการใหม่
                        </a>

                        <form method="GET" action="{{ url('/logextras') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
 <br/>
                        @if ($status == 'Active')
                            <a href="{{ url('/logextras/?status=Active') }}" class="btn btn-primary btn-sm" title="Status Active">
                            <i class="fa fa-unlock" aria-hidden="true"></i> Active
                        </a>
                        <a href="{{ url('/logextras/?status=Closed') }}" class="btn btn-success btn-sm" title="Add New FreezeM">
                            <i class="fa fa-lock" aria-hidden="true"></i> Closed
                        </a>
                        @else
                            <a href="{{ url('/logextras/?status=Active') }}" class="btn btn-success btn-sm" title="Status Active">
                            <i class="fa fa-unlock" aria-hidden="true"></i> Active
                        </a>
                        <a href="{{ url('/logextras/?status=Closed') }}" class="btn btn-primary btn-sm" title="Add New FreezeM">
                            <i class="fa fa-lock" aria-hidden="true"></i> Closed
                        </a>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>วันที่ / กะ</th>
                                        <th>แผนกงาน</th>
                                        <th>จำนวนงาน </th>
                                        <th>Status </th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($logextrams as $item)
                                    <tr>
                                        <td>{{ $item->process_date }} / {{ $item->shift->name }}</td>
                                        <td>{{ $item->dep }}</td>
                                        <td>{{ $item->logextrads->count() }}</td>
                                        <td>{{ $item->status}}</td>
                                        <td>
                                            @if ($item->status == 'Active' && ( $item->logextrads->count() == 0))
                                                <a href="{{ url('/logextras/initialDetails/'.$item->id) }}" class="btn btn-success btn-sm" title="Add New LogPrepareM">
                                                <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มข้อมูล
                                                </a>    
                                            @endif
                                                <a href="{{ url('/logextras/' . $item->id) }}" title="View LogPackM"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> จัดการ</button></a>
                                                <a href="{{ url('/logextras/changestatus/' . $item->id) }}" title="Add FreezeM"><button class="btn btn-success btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> {{ $item->status }}</button></a>
                                                <a href="{{ url('/logextras/' . $item->id . '/edit') }}" title="Edit LogPackM"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                                <form method="POST" action="{{ url('/logextras' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete LogExtra" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $logextrams->appends(['search' => Request::get('search'),'status'=>$status])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
