@extends('layouts.appselect')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>สร้างรายการในงานเพิ่มเติม</h3></div>
                    <div class="card-body">
                        <a href="{{ url('/logextras/'.$logextram->id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                         <div class="row">
                            <div class="col-md-3">
                                <label for="id">ID</label>
                                {{ $logextram->id }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">วันที่</label>
                                {{ $logextram->process_date }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">กะ</label>
                                {{ $logextram->shift->name }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">แผนกงาน</label>
                                {{ $logextram->dep }}
                            </div>
                            <div class="col-md-3">
                                <label for="id">note</label>
                                {{ $logextram->note }}
                            </div>
                        </div>

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/logextras/storeDetail/'.$logextram->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('logextras.form-detail', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
