@extends('packPapers.layouts.app')

@section('content')
    <div class="m-2">
        <a href="{{ route('packPaper2.0') }}" class="btn btn-warning ">กลับ</a>

        <h4 class="mt-2">Delivery Plan </h4>
        <table class="table table-bordered table-striped w-50 my-3">
            <tr>
                <th>Customer Name</th>
                <td>{{ $deliveryPlan->customer->name or '-' }}</td>
            </tr>
            <tr>

                <th>Booking No.</th>

                <td>{{ $deliveryPlan->booking_no or '-' }}</td>
            </tr>
            <tr>
                <th>Orders</th>
                <td>{{ $deliveryPlan->orders or '-' }}</td>
            </tr>
            <tr>
                <th>Customer PO No.</th>
                <td>{{ $deliveryPlan->customer_po_no or '-' }}</td>
            </tr>

            @foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
                @if (isset($delivery_plan_product->product))
                    <tr>
                        <th>Product</th>
                        <td>{{ $delivery_plan_product->product->name }}</td>
                    </tr>
                    <tr>
                        <th>น้ำหนัก</th>
                        <td>{{ number_format($delivery_plan_product->weight, 2) }}</td>
                    </tr>
                    <tr>
                        <th>จำนวน</th>
                        <td>{{ number_format($delivery_plan_product->quantity) }}</td>
                    </tr>
                @endif
            @endforeach

        </table>

        <table class="table">
            <thead>
                <tr class="table-primary">
                    <th>Product name</th>
                    <th>Product Fac</th>
                    <th class="text-center">Version</th>
                    <th class="text-center">Package Name</th>
                    <th class="text-center">Inner weight</th>
                    <th class="text-center">Number per pack</th>
                    <th class="text-center">Outer weight</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
                    <tr>
                        <td>{{ $delivery_plan_product->product->name }}</td>
                        <td>{{ $delivery_plan_product->product->product_fac or 'ไม่ได้ตั้ง' }}</td>

                        @if (count($delivery_plan_product->product->packagings) > 0)
                            @foreach ($delivery_plan_product->product->packagings as $packaging)
                                @if (count($packaging->package) > 0 && $packaging->status == 'Active')
                                    <th class="text-center">{{ $packaging->version }} </th>
                                    <td>
                                        @foreach ($packaging->packaging_packages as $packaging_package)
                                            @if ($packaging_package->package)
                                                |
                                                <a href="{{ route('packPaper2.0.package.view', $packaging_package->package->id) }}"
                                                    title="ดูรายละเอียด" target="_blank">
                                                    {{ $packaging_package->package->name or '' }}</a>
                                                |
                                            @endif
                                        @endforeach

                                    </td>

                                    <td style="text-align: center">
                                        @if (is_numeric($packaging->inner_weight_g))
                                            {{ number_format($packaging->inner_weight_g, 2) }}
                                        @else
                                            {{ $packaging->inner_weight_g }}
                                        @endif

                                    </td>
                                    <td style="text-align: center">
                                        @if (is_numeric($packaging->number_per_pack))
                                            {{ number_format($packaging->number_per_pack, 2) }}
                                        @else
                                            {{ $packaging->number_per_pack }}
                                        @endif

                                    </td>
                                    <td style="text-align: center">
                                        @if (is_numeric($packaging->number_per_pack))
                                            {{ number_format($packaging->number_per_pack, 2) }} Kg
                                        @else
                                            {{ $packaging->number_per_pack }} Kg
                                        @endif
                                    </td>

                                    <td>
                                        @if (isset($delivery_plan_product->pack_paper))
                                            @if ($delivery_plan_product->pack_paper->status == 'Active')
                                                <button type="button" class="btn btn-success btn-sm" data-toggle="modal"
                                                    data-target="#pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                                                    ใบแจ้งบรรจุ เลขที่ {{ $delivery_plan_product->pack_paper->id }}
                                                </button>
                                            @else
                                                <button class="btn btn-danger btn-sm"
                                                    onclick="return alert('{{ $delivery_plan_product->pack_paper->remark }}')">ใบแจ้งบรรจุเลขที่
                                                    {{ $delivery_plan_product->pack_paper->id }}
                                                    ถูกยกเลิก </button>
                                            @endif


                                            <div class="modal"
                                                id="pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <form
                                                            action="{{ route('packPaper2.0.deliveryPlan.createPackPaperDoc', [$delivery_plan_product->pack_paper->id]) }}"
                                                            method="post" enctype="multipart/form-data">

                                                            <div class="modal-header">
                                                                <h5 class="modal-title">รายละเอียด วันที่ผลิต ของใบแจ้งบรรจุ
                                                                </h5>
                                                                <button type="button" class="close btn-close "
                                                                    data-dismiss="modal"></button>
                                                            </div>

                                                            <div class="modal-body">

                                                                {{ csrf_field() }}
                                                                @foreach ($delivery_plan_product->pack_paper->pack_paper_lots as $pack_paper_lot)
                                                                    <label>วันที่ผลิตของLot.
                                                                        <strong>{{ $pack_paper_lot->lot }}</strong></label>

                                                                    <div class="input-group mb-3">

                                                                        <input type="date"
                                                                            name="mfg_month[{{ $pack_paper_lot->id }}]"
                                                                            class="form-control form-control-sm"
                                                                            value="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}"
                                                                            min="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}">


                                                                        <input type="text"
                                                                            name="remark[{{ $pack_paper_lot->id }}]"
                                                                            value="{{ $pack_paper_lot->remark }}"
                                                                            class="form-control form-control-sm"
                                                                            placeholder="หมายเหตุ">
                                                                    </div>
                                                                @endforeach
                                                                <input type="text" name="packPaperRemark"
                                                                    class="form-control form-control-sm"
                                                                    value="{{ $delivery_plan_product->pack_paper->remark }}">

                                                            </div>

                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-danger"
                                                                    data-toggle="modal"
                                                                    data-target="#cancelModal-{{ $delivery_plan_product->pack_paper->id }}">
                                                                    ยกเลิกใบแจ้งบรรจุ
                                                                </button>
                                                                <button type="submit" class="btn btn-success"
                                                                    onclick="return confirm('ไปหน้า ใบบรรจุ')">ใบแจ้งบรรจุ
                                                                    เลขที่
                                                                    {{ $delivery_plan_product->pack_paper->id }}</button>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade"
                                                id="cancelModal-{{ $delivery_plan_product->pack_paper->id }}"
                                                tabindex="-1" aria-labelledby="nestedModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="nestedModalLabel">
                                                                ยกเลิกใบแจ้งบรรจุเลขที่
                                                                {{ $delivery_plan_product->pack_paper->id }}</h5>
                                                            <button type="button" class="close btn" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">X</span>
                                                            </button>
                                                        </div>
                                                        <form
                                                            action="{{ route('test_cancel_pack_paper', [$delivery_plan_product->pack_paper->id]) }}"
                                                            method="POST" enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="modal-body">
                                                                <label>รายละเอียด</label>
                                                                <input type="text" name="remark"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit"
                                                                    class="btn btn-danger my-2">บันทึก</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#makePack_paper_standard-{{ $delivery_plan_product->id }}-{{ $packaging->id }}">
                                                สร้างใบแจ้งบรรจุ
                                            </button>

                                            <div class="modal"
                                                id="makePack_paper_standard-{{ $delivery_plan_product->id }}-{{ $packaging->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <h4 class="modal-title">ตั้งค่า ใบแจ้งบรรจุ </h4>
                                                            <button type="button" class="close btn-close"
                                                                data-dismiss="modal"></button>
                                                        </div>

                                                        <div class="modal-body">

                                                            <nav>
                                                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                                    <button class="nav-link active" data-bs-toggle="tab"
                                                                        data-bs-target="#nav-auto" type="button"
                                                                        role="tab" aria-selected="true">Auto
                                                                        Lots</button>

                                                                    <button class="nav-link" data-bs-toggle="tab"
                                                                        data-bs-target="#nav-manual" type="button"
                                                                        role="tab" aria-selected="false">Manual
                                                                        Lots</button>
                                                                </div>
                                                            </nav>

                                                            <div class="tab-content" id="nav-tabContent">
                                                                <div class="tab-pane fade show active" id="nav-auto"
                                                                    role="tabpanel">
                                                                    <form class="mt-2"
                                                                        action="{{ route('packPaper2.0.deliveryPlan.createPackPaper', [$delivery_plan_product->id, $packaging->id, 'auto']) }}"
                                                                        method="post" enctype="multipart/form-data">

                                                                        {{ csrf_field() }}

                                                                        <label>จำนวนกล่อง แต่ละ Lot</label>
                                                                        <input type="number" name="products_per_lot"
                                                                            class="form-control form-control-sm" required>

                                                                        <label>สายรัด</label>
                                                                        <input type="text" name="cable"
                                                                            class="form-control form-control-sm">

                                                                        <label>ฐานเรียง</label>
                                                                        <input type="number" name="pallet_low"
                                                                            class="form-control form-control-sm" required>

                                                                        <label>จำนวนชั้น</label>
                                                                        <input type="number" name="pallet_height"
                                                                            class="form-control form-control-sm" required>

                                                                        <hr>
                                                                        <div class="text-end">

                                                                            <button type="button"
                                                                                class="btn btn-secondary"
                                                                                data-dismiss="modal">ปิด</button>
                                                                            <button type="submit" class="btn btn-primary"
                                                                                onclick="return confirm('ไปหน้า ใบบรรจุ')">สร้างใบแจ้งบรรจุ</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                                <div class="tab-pane fade" id="nav-manual"
                                                                    role="tabpanel">

                                                                    <form
                                                                        action="{{ route('packPaper2.0.deliveryPlan.createPackPaper', [$delivery_plan_product->id, $packaging->id, 'manual']) }}"
                                                                        method="post" class="mt-2">
                                                                        {{ csrf_field() }}

                                                                        <input type="number" name="products_per_lot"
                                                                            value="0" hidden>
                                                                        <label>สายรัด</label>
                                                                        <input type="text" name="cable"
                                                                            class="form-control form-control-sm">

                                                                        <label>ฐานเรียง</label>
                                                                        <input type="number" name="pallet_low"
                                                                            class="form-control form-control-sm" required>

                                                                        <label>จำนวนชั้น</label>
                                                                        <input type="number" name="pallet_height"
                                                                            class="form-control form-control-sm" required>

                                                                        <div id="lotsContainer" class="row m-3">
                                                                            <!-- ฟิลด์ lot และ quantity จะถูกเพิ่มที่นี่ -->
                                                                        </div>

                                                                        <div class="d-flex m-3">
                                                                            <button type="button" id="addLotButton"
                                                                                class="btn btn-success">
                                                                                <i class="fa-solid fa-plus"></i> เพิ่ม
                                                                                lot
                                                                            </button>

                                                                        </div>

                                                                        <hr>
                                                                        <div class="text-end">

                                                                            <button type="button"
                                                                                class="btn btn-secondary"
                                                                                data-dismiss="modal">ปิด</button>
                                                                            <button type="submit" class="btn btn-primary"
                                                                                onclick="return confirm('ไปหน้า ใบบรรจุ')">สร้างใบแจ้งบรรจุ</button>
                                                                        </div>
                                                                    </form>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                @endif
                            @endforeach
                        @else
                            <td colspan="6">
                                <a href="{{ route('packPaper2.0.product.packagingPackaging.addNewVersion', [$delivery_plan_product->product->id]) }}"
                                    class="btn btn-success" style="width: 200px" target="_blank">
                                    <i class="fa-solid fa-plus"></i> เพิ่ม Version ใหม่
                                </a>
                            </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    {{-- @push('scripts')
        <script>
            let remainingQuantity = {{ $quantity }};
            let index = {{ $index }};
            const alphabet = '{{ $alphabet }}';

            document.getElementById('addLotButton').addEventListener('click', function() {
                if (remainingQuantity > 0 && index < alphabet.length) {
                    // สร้างฟิลด์ใหม่สำหรับ lot และ quantity
                    const lotLabel = document.createElement('label');
                    lotLabel.textContent = `Lot ${alphabet[index]}`;
                    lotLabel.className = 'col-form-label col-sm-2';

                    const quantityInput = document.createElement('input');
                    quantityInput.type = 'number';
                    quantityInput.name = `quantity[${alphabet[index]}]`;
                    quantityInput.max = remainingQuantity;
                    quantityInput.placeholder = `จำนวนไม่เกิน ${remainingQuantity}`;
                    quantityInput.className = 'form-control col-sm-10';
                    quantityInput.required = true;

                    const formGroup = document.createElement('div');
                    formGroup.className = 'row align-items-center';

                    formGroup.appendChild(lotLabel);
                    formGroup.appendChild(quantityInput);

                    // เพิ่มฟิลด์ใหม่ในฟอร์ม
                    const container = document.getElementById('lotsContainer');
                    container.appendChild(formGroup);

                    // ลดจำนวน remainingQuantity ตามค่า input ที่ใส่
                    quantityInput.addEventListener('input', function() {
                        let sum = 0;
                        const inputs = document.querySelectorAll('input[type="number"]');
                        inputs.forEach(input => {
                            sum += parseInt(input.value) || 0;
                        });
                        remainingQuantity = {{ $quantity }} - sum;
                        // ปรับปรุง max ของ input field ตามค่า remainingQuantity ที่เปลี่ยนแปลง
                        inputs.forEach(input => {
                            input.max = remainingQuantity + parseInt(input.value) || 0;
                        });
                    });

                    // เพิ่ม index เพื่อให้เพิ่ม lot ถัดไป
                    index++;
                } else if (remainingQuantity <= 0) {
                    alert('จำนวนทั้งหมดถูกใช้หมดแล้ว');
                }
            });

            document.getElementById('lotForm').addEventListener('submit', function(event) {
                if (remainingQuantity < 0) {
                    alert('จำนวนรวมเกินกว่าที่กำหนด');
                    event.preventDefault(); // หยุดการ submit
                }
            });
        </script>
    @endpush --}}



    @push('scripts')
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                let remainingQuantity = {{ $quantity }};
                let index = {{ $index }};
                const alphabet = '{{ $alphabet }}';

                document.getElementById('addLotButton').addEventListener('click', function() {
                    if (remainingQuantity > 0 && index < alphabet.length) {
                        // สร้างฟิลด์ใหม่สำหรับ lot และ quantity
                        const lotLabel = document.createElement('label');
                        lotLabel.textContent = `Lot ${alphabet[index]}`;
                        lotLabel.className = 'col-form-label col-sm-2';

                        const quantityInput = document.createElement('input');
                        quantityInput.type = 'number';
                        quantityInput.name = `quantity[${alphabet[index]}]`;
                        quantityInput.max = remainingQuantity;
                        quantityInput.placeholder = `จำนวนไม่เกิน ${remainingQuantity}`;
                        quantityInput.className = 'form-control col-sm-10';
                        quantityInput.required = true;

                        const formGroup = document.createElement('div');
                        formGroup.className = 'row align-items-center mb-2';

                        formGroup.appendChild(lotLabel);
                        formGroup.appendChild(quantityInput);

                        // เพิ่มฟิลด์ใหม่ในฟอร์ม
                        const container = document.getElementById('lotsContainer');
                        container.appendChild(formGroup);

                        // ลดจำนวน remainingQuantity ตามค่า input ที่ใส่
                        quantityInput.addEventListener('input', function() {
                            let sum = 0;
                            const inputs = document.querySelectorAll(
                                '#lotsContainer input[type="number"]');
                            inputs.forEach(input => {
                                sum += parseInt(input.value) || 0;
                            });
                            remainingQuantity = {{ $quantity }} - sum;

                            // ปรับปรุง max ของ input field ตามค่า remainingQuantity ที่เปลี่ยนแปลง
                            inputs.forEach(input => {
                                input.max = remainingQuantity + parseInt(input.value) || 0;
                            });
                        });

                        // เพิ่ม index เพื่อให้เพิ่ม lot ถัดไป
                        index++;
                    } else if (remainingQuantity <= 0) {
                        alert('จำนวนทั้งหมดถูกใช้หมดแล้ว');
                    }
                });

                document.getElementById('lotForm').addEventListener('submit', function(event) {
                    if (remainingQuantity < 0) {
                        alert('จำนวนรวมเกินกว่าที่กำหนด');
                        event.preventDefault(); // หยุดการ submit
                    }
                });
            });
        </script>
    @endpush



@endsection
