 <p class="text-center">ตำแหน่งการ Stamp</p>

 @php
     $packagings = $pack_paper->delivery_plan_product->product->packagings->where('status', 'Active');
 @endphp

 <table style="border: 1px solid black ; width:100%" class="text-center">

     <tr style="vertical-align: top">
         @foreach ($packagings as $packaging)
             @foreach ($packaging->packaging_packages as $packaging_package)
                 @if ($packaging_package->package->packagetype->name == 'Bag')
                     @if ($packaging_package->package)
                         @if (count($packaging_package->package->packagesImgs) > 0)
                             @foreach ($packaging_package->package->packagesImgs as $packagesImgs)
                                 <th style=" border: 1px solid ">
                                     <strong>{{ $packaging_package->package->name }}</strong>
                                     <br>
                                     {{ $packagesImgs->name or '-' }}
                                     <br>


                                     <img src="{{ asset('storage/' . $packagesImgs->main_img_path) }}"
                                         title="{{ $packagesImgs->name }}&#10;{{ $packagesImgs->detail }}"
                                         style="height:300px; max-width: 250px">

                                     @if (!empty($packagesImgs->stamp_img_paht))
                                         <img src="{{ asset('storage/' . $packagesImgs->stamp_img_paht) }}"
                                             title="{{ $packagesImgs->name }}&#10;{{ $packagesImgs->detail }}"
                                             style="height:300px; max-width: 250px ">
                                     @endif
                                     <br>
                                     @if (!empty($packagesImgs->stamp_format))
                                         <strong>{!! nl2br($packagesImgs->stamp_format) !!}</strong>
                                     @endif
                                     <br>
                                     @if (!empty($packagesImgs->detail))
                                         <strong>{{ $packagesImgs->detail }}</strong>
                                     @endif
                                 </th>
                             @endforeach
                         @endif
                     @endif
                 @endif

                 @if ($packaging_package->package->packagetype->name == 'Innerbox')
                     @if ($packaging_package->package)
                         @if (count($packaging_package->package->packagesImgs) > 0)
                             @foreach ($packaging_package->package->packagesImgs as $packagesImgs)
                                 <th style=" border: 1px solid  ">
                                     <strong>{{ $packaging_package->package->name }}</strong>
                                     <br>
                                     {{ $packagesImgs->name or '-' }} <br>

                                     <img src="{{ asset('storage/' . $packagesImgs->main_img_path) }}"
                                         title="{{ $packagesImgs->name }}&#10;{{ $packagesImgs->detail }}"
                                         style="height:300px;max-width: 250px ">

                                     @if (!empty($packagesImgs->stamp_img_paht))
                                         <img src="{{ asset('storage/' . $packagesImgs->stamp_img_paht) }}"
                                             title="{{ $packagesImgs->name }}&#10;{{ $packagesImgs->detail }}"
                                             style="height:300px; max-width: 250px ">
                                     @endif
                                     <br>
                                     @if (!empty($packagesImgs->stamp_format))
                                         <strong>{!! nl2br($packagesImgs->stamp_format) !!}</strong>
                                     @endif
                                     <br>
                                     @if (!empty($packagesImgs->detail))
                                         <strong>{{ $packagesImgs->detail }}</strong>
                                     @endif
                                 </th>
                             @endforeach
                         @endif
                     @endif
                 @endif
             @endforeach
         @endforeach
     </tr>

     @foreach ($packagings as $packaging)
         @foreach ($packaging->packaging_packages as $packaging_package)
             @if ($packaging_package->package->packagetype->name == 'Carton')
                 <th colspan="4" style=" border: 1px solid; ">
                     <strong>{{ $packaging_package->package->name }}</strong>
                     <br>
                     {{ $packagesImgs->name or '-' }} <br>

                     @foreach ($packaging_package->package->packagesImgs as $packagesImgs)
                         {{-- {{ $packagesImgs->name or '-' }} --}}

                         <img src="{{ asset('storage/' . $packagesImgs->main_img_path) }}"
                             title="{{ $packagesImgs->name }}&#10;{{ $packagesImgs->detail }}"
                             style="max-height:500px; max-width: 1100px">


                         @if (!empty($packagesImgs->stamp_img_paht))
                             <img src="{{ asset('storage/' . $packagesImgs->stamp_img_paht) }}"
                                 title="{{ $packagesImgs->name }}&#10;{{ $packagesImgs->detail }}"
                                 style="height:500px;">
                         @endif

                         <br>
                         @if (!empty($packagesImgs->stamp_format))
                             <strong>{!! nl2br($packagesImgs->stamp_format) !!}</strong>
                         @endif
                         <br>
                         @if (!empty($packagesImgs->detail))
                             <strong>{{ $packagesImgs->detail }}</strong>
                         @endif
                         <br>
                     @endforeach
                 </th>
             @endif
         @endforeach
     @endforeach

     </tr>

 </table>
