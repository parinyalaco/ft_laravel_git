<table class="center">
    <tr>
        <td style="border: none"></td>
        <th colspan="2">ปริมาณ</th>
        <th style="border-bottom: none;">ระยะเวลาที่หมด</th>
        <th rowspan="2">วันที่หมดอายุ</th>

        @foreach ($pack_paper->delivery_plan_product->product->packaging->packaging_packages as $packaging_package)
            <th style="border-bottom: none;">
                วันที่ผลิต
            </th>
            <th style="border-bottom: none;">
                วันที่หมดอายุ
            </th>
            <th style="border-bottom: none;">
                Lot
            </th>
        @endforeach
    </tr>

    <tr>
        <td style="border: none"></td>
        <th>กก.</th>
        <th>กล่อง</th>
        <th style="border-top: none;">อายุ (เดือน)</th>

        @foreach ($pack_paper->delivery_plan_product->product->packaging->packaging_packages as $packaging_package)
            <th style="border-top: none;">
                @if ($packaging_package->package)
                    @if (!empty(config('myconfig.head_column.' . $packaging_package->package->packagetype->name)))
                        บน{{ config('myconfig.head_column.' . $packaging_package->package->packagetype->name) }}สำเร็จรูป
                    @else
                        บน{{ $packaging_package->package->packagetype->name }}สำเร็จรูป
                    @endif
                @endif
            </th>

            <th style="border-top: none;">
                @if ($packaging_package->package)
                    @if (!empty(config('myconfig.head_column.' . $packaging_package->package->packagetype->name)))
                        บน{{ config('myconfig.head_column.' . $packaging_package->package->packagetype->name) }}สำเร็จรูป
                    @else
                        บน{{ $packaging_package->package->packagetype->name }}สำเร็จรูป
                    @endif
                @endif

            </th>

            <th style="border-top: none;">
                @if ($packaging_package->package)
                    @if (!empty(config('myconfig.head_column.' . $packaging_package->package->packagetype->name)))
                        บน{{ config('myconfig.head_column.' . $packaging_package->package->packagetype->name) }}สำเร็จรูป
                    @else
                        บน{{ $packaging_package->package->packagetype->name }}สำเร็จรูป
                    @endif
                @endif
            </th>
        @endforeach

    </tr>

    <tr>
        <td style="border: none">std.</td>
        <td>-</td>
        <td>-</td>
        <th>{{ $pack_paper->pack_paper_standard->exp_month }}</th>
        <th>ว/ด/ป</th>

        @foreach ($pack_paper->delivery_plan_product->product->packaging->packaging_packages as $packaging_package)
            <th>
                @if ($packaging_package->package)
                    @if (is_numeric($packaging_package->package->stamp_format))
                        @if (!empty($packaging_package->package->stampFormat->mfg->stamp_date_format))
                            {{ $packaging_package->package->stampFormat->mfg->front_text }}

                            {{ strtoupper($packaging_package->package->stampFormat->mfg->stamp_date_format->name) }}

                            @if (!empty($packaging_package->package->stampFormat->mfg->lot))
                                {{$packaging_package->package->stampFormat->mfg->lot}}
                            @endif
                        @else
                            ไม่มีระบุ
                        @endif
                    @else
                        ไม่มีระบุ
                    @endif
                @endif

            </th>
            <th>
                @if ($packaging_package->package)

                    @if (is_numeric($packaging_package->package->stamp_format))
                        @if (!empty($packaging_package->package->stampFormat->exp->stamp_date_format))
                            {{ $packaging_package->package->stampFormat->exp->front_text }}

                            {{ strtoupper($packaging_package->package->stampFormat->exp->stamp_date_format->name) }}
                            @if (!empty($packaging_package->package->stampFormat->exp->lot))
                                {{ $packaging_package->package->stampFormat->exp->lot }}
                            @endif
                        @else
                            ไม่มีระบุ
                        @endif
                    @else
                        ไม่มีระบุ
                    @endif
                @endif

            </th>
            <th>
                @if ($packaging_package->package)
                    @if (is_numeric($packaging_package->package->stamp_format))
                        @if (
                            !empty($packaging_package->package->stampFormat->mfg->lot) ||
                                !empty($packaging_package->package->stampFormat->exp->lot))
                            A-Z
                        @else
                            ไม่มีระบุ
                        @endif
                    @else
                        ไม่มีระบุ
                    @endif
                @endif

            </th>
        @endforeach

    </tr>

    @foreach ($pack_paper->pack_paper_lots->groupBy('mfg_date') as $mfg_date => $pack_paper_lots)
        @php
            $mfg_weight = $pack_paper_lots->sum(function ($pack_paper_lot) use ($pack_paper) {
                return $pack_paper_lot->quantity_boxes *
                    $pack_paper->delivery_plan_product->product->packaging->outer_weight_kg;
            });
            $formatted_mfg_weight = number_format($mfg_weight, 0);

            $mdg_quantity_boxes = number_format($pack_paper_lots->sum('quantity_boxes'));

        @endphp

        <tr>
            <td style="border: none"></td>
            <td>{{ $formatted_mfg_weight }}</td>
            <td>{{ $mdg_quantity_boxes }}</td>
            <td>{{ $pack_paper->pack_paper_standard->exp_month }}</td>
            <td>

                @php
                    $exp_date_standard = $pack_paper->pack_paper_standard->exp_month;

                    $mfg_month = date('Y-m-d', strtotime($mfg_date));
                    $manufacture_date = $mfg_month;

                    $manufacture_date_cal = date('Y-m', strtotime($manufacture_date));

                    $expiry_date = date('Y-m-d', strtotime($manufacture_date . '+' . $exp_date_standard . 'months'));
                    $expiry_date_cal = date('Y-m',strtotime($manufacture_date_cal . '+' . $exp_date_standard . 'months'));

                    $last_day_of_manufacture_month = date('t', strtotime($manufacture_date_cal));

                    $last_day_of_expiry_month = date('t', strtotime($expiry_date_cal));

                    if ($last_day_of_manufacture_month > $last_day_of_expiry_month) {
                        if ($last_day_of_manufacture_month == date('d', strtotime($manufacture_date))) {
                            $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months -1 day'));
                        } else {
                            $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . 'months'));
                        }
                    }

                    if ($last_day_of_expiry_month < 30) {
                        if ($last_day_of_expiry_month == 28) {
                            if (date('d', strtotime($manufacture_date)) == 31) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . '+' . $exp_date_standard . ' months' . ' -3 day'));
                            }
                            if (date('d', strtotime($manufacture_date)) == 30) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . '+' . $exp_date_standard . ' months' . ' -2 day'));
                            }

                            if (date('d', strtotime($manufacture_date)) == 29) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . ' +' . $exp_date_standard . ' months' . ' -1 day'));
                            }
                        }

                        if ($last_day_of_expiry_month == 29) {
                            if (date('d', strtotime($manufacture_date)) == 31) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . '+' . $exp_date_standard . ' months' . ' -2 day'));
                            }
                            if (date('d', strtotime($manufacture_date)) == 30) {
                                $expiry_date = date('Y-m-d',strtotime($manufacture_date . '+' . $exp_date_standard . ' months' . ' -1 day'));
                            }
                        }
                    }

                @endphp

                {{ date('d/m/Y', strtotime($expiry_date)) }}

            </td>

            @foreach ($pack_paper->delivery_plan_product->product->packaging->packaging_packages as $packaging_package)
                <td>
                    @if ($packaging_package->package)
                        @if (is_numeric($packaging_package->package->stamp_format))
                            @if (!empty($packaging_package->package->stampFormat->mfg->stamp_date_format))
                                @php
                                    $date_era_format = $packaging_package->package->stampFormat->mfg->date_era_format;
                                    $BE = null;
                                    $date = \Carbon\Carbon::createFromFormat('Y-m-d', $mfg_month);
                                    if ($date_era_format == 'BE') {
                                        $date->addYears(543);
                                    }
                                    $formatted_date = $date->format(
                                        $packaging_package->package->stampFormat->mfg->stamp_date_format
                                            ->stamp_date_format,
                                    );

                                    ($front_text = $packaging_package->package->stampFormat->mfg->front_text) or '';
                                    if (strtoupper($packaging_package->package->stampFormat->mfg->front_text) == 'X') {
                                        $data = $pack_paper->delivery_plan_product->delivery_plan->orders; // ตัวอย่างข้อมูล
                                        $pattern = '/#(\d+):/'; // รูปแบบ regex เพื่อหาตัวเลขที่ต้องการ

                                        preg_match($pattern, $data, $matches);
                                        $number = isset($matches[1]) ? $matches[1] : 'ไม่พบข้อมูลที่ต้องการ';

                                        // ใช้ preg_replace เพื่อลบศูนย์นำหน้าตัวเลขที่มีหลักเดียว
                                        $front_text = preg_replace('/^0+(\d+)$/', '$1', $number);
                                    }
                                @endphp


                                {{ $front_text }}{{ $formatted_date }}

                                @if (
                                    !empty($packaging_package->package->stampFormat->mfg->lot) &&
                                        $packaging_package->package->stampFormat->mfg->lot == 'on')
                                    {{ $pack_paper_lot->lot }}
                                @endif
                            @else
                                ไม่มีระบุ
                            @endif
                        @else
                            ไม่มีระบุ
                        @endif
                    @endif


                </td>
                <td>

                    @if ($packaging_package->package)
                        @if (is_numeric($packaging_package->package->stamp_format))
                            @if (!empty($packaging_package->package->stampFormat->exp->stamp_date_format))
                                {{ $packaging_package->package->stampFormat->exp->front_text or '' }}

                                @php
                                    $date_era_format = $packaging_package->package->stampFormat->exp->date_era_format;
                                    $BE = null;
                                    $date = \Carbon\Carbon::createFromFormat('Y-m-d', $expiry_date);
                                    if ($date_era_format == 'BE') {
                                        $date->addYears(543);
                                    }
                                    $formatted_date = $date->format(
                                        $packaging_package->package->stampFormat->exp->stamp_date_format
                                            ->stamp_date_format,
                                    );

                                @endphp

                                {{ $formatted_date }}
                            @else
                                ไม่มีระบุ
                            @endif
                        @else
                            ไม่มีระบุ
                        @endif
                    @endif
                </td>
                <td>


                    @if ($packaging_package->package)
                        @if (is_numeric($packaging_package->package->stamp_format))
                            @if (
                                !empty($packaging_package->package->stampFormat->exp->lot) ||
                                    !empty($packaging_package->package->stampFormat->mfg->lot))
                                @foreach ($pack_paper_lots as $index => $pack_paper_lot)
                                    {{ $pack_paper_lot->lot }}


                                    @if ($index + 1 < count($pack_paper_lots))
                                        ,
                                    @endif
                                @endforeach
                            @else
                                ไม่มีระบุ
                            @endif
                        @else
                            ไม่มีระบุ
                        @endif
                    @endif
                </td>
            @endforeach

        </tr>
    @endforeach
</table>
