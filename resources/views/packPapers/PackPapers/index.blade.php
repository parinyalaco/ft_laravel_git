<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ใบแจ้งบรรจุ เลขที่ {{ $pack_paper_id }}</title>

    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Sarabun:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
        rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.1/jspdf.umd.min.js"></script>


    <style>
        @media print {
            @page {
                size: 60% A4 landscape;

            }


            .d-print-none {
                display: none;
            }

            .page-break {
                page-break-before: always;
            }

            body {
                -webkit-print-color-adjust: exact;
                print-color-adjust: exact;

            }

            .card {
                display: inline-flex;
                flex-direction: column;
                max-width: 100%;
                border: 1px solid #ffffff;
                border-radius: 5px;
            }
        }

        body {
            font-family: "Sarabun", sans-serif;

        }

        .center {
            border-collapse: collapse;
            max-width: 100%;
            margin-bottom: 1rem;
            width: 100%;
            font-size: 18px;
        }

        .center td,
        .center th {
            text-align: center;
            border: 1px solid #000;
            padding: 0px;
        }

        .card {
            display: inline-flex;
            flex-direction: column;
            /* width: fit-content; */
            max-width: 100%;
            /* margin: 10px; */
            /* padding: 10px; */
            border: 1px solid #ffffff;
            border-radius: 5px;
        }


        .package {
            margin: 1px 0;
            border: 1px solid #000000;
            padding: 1px;
        }

        .package-name {
            text-align: center;
        }

        .package-imgs {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
        }

        .package-img {
            flex: 0 1 20px;
            margin: 1px;
            text-align: center.
        }

        .package-img-main img,
        .package-img-stamp img {
            max-height: 500px;
            height: auto;
            border: 1px solid #000000;

        }

        .package-img-name,
        .package-img-format,
        .package-img-detail {
            margin-top: 1px;
            border: 1px solid #000000;

        }
    </style>

</head>

<body>
    <div class="container-fluid" style="font-size: 18px">
        <div class="row">
            <div class="col">
                <img src="{{ asset('LacoHeader.png') }}" width="500px">
            </div>
            <div class="col">

                <p class="text-center" style="font-size: 18px">ใบแจ้งการบรรจุผลิตภัณฑ์แช่แข็งสำเร็จรูป</p>
            </div>
            <div class="col d-flex justify-content-end">
                <button onclick="window.print()" class="btn btn-sm btn-primary mt-3 d-print-none"> <i
                        class='bi bi-printer-fill align-middle' style="font-size: 2rem"> </i>
                    พิมพ์เอกสารใบแจ้งบรรจุ</button>

                <button onclick="history.back()" class="btn btn-sm btn-warning mt-3 mx-2 d-print-none">
                    Back</button>
            </div>
        </div>

        <p class="text-left mb-1">Doc. Code: F-PL-PK-001/{{ $pack_paper->id }}</p>
        <p class="text-left mb-1">วันที่: {{ date('d/m/Y') }}</p>

        <p class="text-left mb-1">วันที่จัดทำใบแจ้งบรรจุ:
            {{ date('d/m/Y', strtotime($pack_paper->delivery_plan_product->packing_date)) }}</p>

        {{-- Table 1  --}}
        @include('packPapers.PackPapers.table1')

        {{-- table 2  --}}
        @include('packPapers.PackPapers.table2')

        {{-- table 3 --}}
        @include('packPapers.PackPapers.table3')
        <p> Remark : {{ $pack_paper->remark }}</p>

        {{-- หมายเหตุ --}}
        <table class="center w-25">
            <tr>
                <th>จำนวนกล่องต่อพาเลท</th>
                <th>ฐานเรียง</th>
                <th>จำนวนชั้น</th>
            </tr>
            <tr>
                <td>{{ $pack_paper->pack_paper_standard->pallet_base }} </td>
                <td>{{ $pack_paper->pack_paper_standard->pallet_low }}</td>
                <td>{{ $pack_paper->pack_paper_standard->pallet_height }}</td>
            </tr>
        </table>
        {{-- table 4 --}}
        @include('TestPackages.PackPapers.table4')

        <div class="row" style="font-size: 18px">
            <div class="col">
                <p class="text-left mb-1">Remark : SL Document :</p>
                <p class="text-left mb-1">
                    จัดทำโดย
                    ............................................................................
                    พนักงานแผนก/หัวหน้าแผนก
                    (แผนกคัดบรรจุ/แผนกผลิตแช่แข็ง)</p>

                <p class="text-left mb-1">
                    ตรวจทานโดย
                    ......................................................................
                    หัวหน้าแผนกวางแผนการผลิตขึ้นไป</p>

                <p class="text-left mb-1">
                    รับทราบโดย
                    ........................................................................
                    หัวหน้าแผนกควบคุมคุณภาพขึ้นไป
                </p>
            </div>
            <div class="col">
                <p class="text-left mb-1"> &nbsp;</p>
                <p class="text-left mb-1">
                    ตรวจสอบโดย
                    ..............................................................................
                    ผู้จัดการแผนกคัดบรรจุ/ผู้จัดการแผนกผลิตแช่แข็ง
                </p>

                <p class="text-left mb-1">
                    ตรวจทานโดย
                    ..............................................................................
                    ผู้ช่วยผู้จัดการแผนกประกันคุณภาพ/แผนกควบคุมคุณภาพขึ้นไป</p>

                <p class="text-left mb-1">
                    อนุมัติโดย
                    ....................................................................................
                    ผู้ช่วยผู้จัดการฝ่ายโรงงานขึ้นไป
                </p>
            </div>
        </div>
        <hr>
        <div class="page-break">

        </div>


        {{-- Image of packing --}}
        @include('packPapers.PackPapers.image')
    </div>

</body>

</html>
