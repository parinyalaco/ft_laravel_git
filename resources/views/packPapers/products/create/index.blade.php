@extends('packPapers.layouts.app')

@section('content')
    <div class="container">
        <a href="{{route('packPaper2.0.product')}}" class="btn btn-warning my-2">กลับ</a>
        <h4 class="text-center">เพิ่ม Product</h4>
        <hr>

        <form action="{{ route('packPaper2.0.product.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label>ชื่อ Customer</label>
            <select name="customer_id" class="addNewProduct-select2 form-select-sm" style="width: 100%">
                @foreach ($customers as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
            </select>

            <label>Product Group</label>
            <select name="product_group_id" class="addNewProduct-select2 form-select-sm" style="width: 100%">
                @foreach ($product_groups as $id => $name)
                    <option value="{{ $id }}">{{ $name }}</option>
                @endforeach
            </select>

            <label>ชื่อ</label>
            <input type="text" name="name" class="form-control form-control-sm">

            <label>Product Fac.</label>
            <input type="text" name="product_fac" class="form-control form-control-sm">

            <label>รายละเอียด</label>
            <textarea name="desc" cols="30" rows="5" class="form-control"></textarea>

            <label>SAP</label>
            <input type="text" name="SAP" class="form-control form-control-sm">

            <label>shelf life</label>
            <input type="number" name="shelf_life" class="form-control form-control-sm">

            <label>น้ำหนักรวม</label>
            <input type="number" name="weight_with_bag" class="form-control form-control-sm" step='0.001'>

            <label>หน่วย</label>
            <select name="unit_id" class="addNewProduct-select2 form-select-sm" style="width: 100%">
                @foreach ($units as $id => $name)
                    <option value="{{ $id }}">
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
        </form>
    </div>

    @push('scripts')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
            $('.addNewProduct-select2').select2({
                theme: 'bootstrap-5',

                placeholder: 'Select an option',
 
            });
        </script>
    @endpush
@endsection
