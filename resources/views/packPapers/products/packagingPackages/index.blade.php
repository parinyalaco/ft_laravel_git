@extends('packPapers.layouts.app')

@section('content')
    <div class="m-2">
        <a href="{{ route('packPaper2.0.product') }}" class="btn btn-warning">กลับ</a>

        <a href="{{ route('packPaper2.0.product.packagingPackaging.addNewVersion', [$product->id]) }}"
            class="btn btn-success">
            <i class="fa-solid fa-plus"></i> เพิ่ม Version ใหม่
        </a>
        <h4 class="mt-2">Packaging Package ของ {{ $product->name }} </h4>
    </div>
    <hr>



    @foreach ($product->packagings as $packaging)
        <div class="card p-2 mt-3" style="border: 2px solid #696969 ">
            <table class="table">
                <tr>
                    <td>
                        <strong>Packaging Version :</strong>
                        {{ $packaging->version }}
                    </td>
                    <td> <strong class="mx-2">น้ำหนักต่อถุง (g):</strong> {{ $packaging->inner_weight_g }} </td>
                    <td><strong class="mx-2">น้ำหนักต่อถุง (g) (ประมาณค่า):</strong>
                        {{ $packaging->inner_weight_g_es }}</td>
                    <td><strong class="mx-2">น้ำหนักต่อ กล่อง :</strong>

                        {{ number_format($packaging->outer_weight_kg, 3) }}</td>
                    <td> <strong class="mx-2">จำนวนถุง/กล่อง :</strong>
                        {{ number_format($packaging->number_per_pack, 2) }}</td>
                    <td>

                        <form
                            action="{{ route('packPaper2.0.product.packagingPackaging.switchVersion', [$product->id, $packaging->id]) }}"
                            method="POST" id="statusForm-{{ $packaging->id }}">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}

                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" role="switch"
                                    id="statusSwitch-{{ $packaging->id }}" @if ($packaging->status == 'Active') checked @endif
                                    onchange="document.getElementById('statusForm-{{ $packaging->id }}').submit();">

                                <label class="form-check-label" for="flexSwitchCheckDefault">
                                    สถานะการใช้งาน
                                </label>
                            </div>
                        </form>



                    </td>
                    <td>
                        <a href="{{ route('packPaper2.0.product.packagingPackaging.addNewPackage', [$product->id, $packaging->id]) }}"
                            class="btn btn-primary">
                            เพิ่ม Package ของ version {{ $packaging->version }}
                        </a>
                        <div class="btn-group">
                            <button class="btn" data-bs-toggle="modal"
                                data-bs-target="#editPackaging-{{ $packaging->id }}">
                                <i class='bx bx-edit-alt bx-tada-hover bx-sm'> </i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="editPackaging-{{ $packaging->id }}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5>แก้ไข Packaging Version: {{ $packaging->version }}
                                            </h5>

                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>

                                        <div class="modal-body">
                                            <form
                                                action="{{ route('packPaper2.0.product.packagingPackaging.updateVersion', [$product->id, $packaging->id]) }}"
                                                method="POST" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}


                                                <div class="row">
                                                    <div class="col">
                                                        <label>น้ำหนักต่อถุง (g)</label>
                                                        <input type="number" name="inner_weight_g"
                                                            class="form-control form-control-sm"
                                                            value="{{ $packaging->inner_weight_g }}">
                                                    </div>
                                                    <div class="col">
                                                        <label>น้ำหนักต่อถุง (g) (ประมาณค่า) </label>
                                                        <input type="text" name="inner_weight_g_es"
                                                            class="form-control form-control-sm"
                                                            value="{{ $packaging->inner_weight_g_es }}">
                                                    </div>
                                                </div>

                                                <label>น้ำหนักต่อ กล่อง (กิโลกรัม)</label>
                                                <input type="number" name="outer_weight_kg" step="0.001"
                                                    class="form-control form-control-sm"
                                                    value="{{ number_format($packaging->outer_weight_kg, 3) }}">

                                                <label>จำนวนถุง/กล่อง</label>
                                                <input type="number" name="number_per_pack"
                                                    class="form-control form-control-sm"
                                                    value="{{ $packaging->number_per_pack }}">


                                                <label>สถานะ</label>
                                                <select name="status" class="form-select">
                                                    <option value="Active"
                                                        @if ($packaging->status == 'Active') selected @endif>
                                                        Active</option>
                                                    <option value="Inactive"
                                                        @if ($packaging->status == 'Inactive') selected @endif>
                                                        Inactive</option>

                                                </select>
                                                <button class="btn btn-warning mt-2 w-100" type="submit"
                                                    onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">แก้ไข</button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>


                            <form
                                action="{{ route('packPaper2.0.product.packagingPackaging.deleteVersion', [$product->id, $packaging->id]) }}"
                                method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                                        class='bx bx-trash bx-tada-hover bx-sm'></i>

                                </button>
                            </form>
                        </div>
                    </td>

                </tr>
            </table>

            <table class="table table-bordered mt-2">


                <tr class="table-primary">
                    <th>Pakage</th>
                    <th>description </th>
                    <th>Size</th>
                    <th>สถานะ</th>
                    <th>Stamp Date วันผลิต </th>
                    <th>Stamp Date วันหมดอายุ</th>


                    <th class="text-center">Action</th>
                </tr>


                @foreach ($packaging->packaging_packages as $packaging_package)
                    @if ($packaging_package->package)
                        <tr>
                            <td> {{ $packaging_package->package->name }}</td>
                            <td>{{ $packaging_package->package->desc }}</td>
                            <td>{{ $packaging_package->package->size }}</td>
                            @if ($packaging_package->package->status == 'Active')
                                <td style="color: green"> <i
                                        class='bx bxs-circle'></i><strong>{{ $packaging_package->package->status }}</strong>
                                </td>
                            @else
                                <td style="color: red"><i
                                        class='bx bxs-circle'></i><strong>{{ $packaging_package->package->status }}</strong>
                                </td>
                            @endif
                            <td class="text-center">
                                @isset($packaging_package->package->stampFormat->mfg)
                                    <table class="table">
                                        <tr>
                                            <th>คำหน้า</th>
                                            <th>Format</th>
                                            <th>คำท้าย</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! nl2br($packaging_package->package->stampFormat->mfg->front_text ?? '') !!}

                                            </td>
                                            <td>
                                                {{ $packaging_package->package->stampFormat->mfg->stamp_date_format->name ?? '' }}
                                                @if ($packaging_package->package->stampFormat->mfg->date_era_format === 'BE')
                                                    (พ.ศ.)
                                                @elseif($packaging_package->package->stampFormat->mfg->date_era_format === 'AD')
                                                    (ค.ศ.)
                                                @endif

                                            </td>
                                            <td>

                                                {!! nl2br($packaging_package->package->stampFormat->mfg->lot ?? '') !!}
                                            </td>

                                        </tr>

                                    </table>
                                @endisset

                            </td>
                            <td class="text-center">
                                @isset($packaging_package->package->stampFormat->exp)
                                    <table class="table">
                                        <tr>
                                            <th>คำหน้า</th>
                                            <th>Format</th>
                                            <th>คำท้าย</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                {!! nl2br($packaging_package->package->stampFormat->exp->front_text ?? '') !!}

                                            </td>
                                            <td>
                                                {{ $packaging_package->package->stampFormat->exp->stamp_date_format->name ?? '' }}
                                                @if ($packaging_package->package->stampFormat->exp->date_era_format === 'BE')
                                                    (พ.ศ.)
                                                @elseif($packaging_package->package->stampFormat->exp->date_era_format === 'AD')
                                                    (ค.ศ.)
                                                @endif

                                            </td>
                                            <td>

                                                {!! nl2br($packaging_package->package->stampFormat->exp->lot ?? '') !!}
                                            </td>
                                        </tr>

                                    </table>
                                @endisset


                            </td>
                            <td>
                                <a href="{{ route('packPaper2.0.package.view', $packaging_package->package->id) }}"
                                    class="btn btn-primary" title="จัดการ" target="_blank">จัดการ </a>

                                <div class="btn-group">
                                    <button class="btn" data-bs-toggle="modal"
                                        data-bs-target="#changePackage-{{ $packaging_package->package->id }}">
                                        <i class='bx bx-edit-alt bx-tada-hover bx-sm'> </i>
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="changePackage-{{ $packaging_package->package->id }}"
                                        aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5>เปลี่ยน Package {{ $packaging_package->package->name }}</h5>

                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>

                                                <div class="modal-body">
                                                    <form
                                                        action="{{ route('packPaper2.0.product.packagingPackaging.changePackage', [$product->id, $packaging_package->id]) }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}

                                                        <label>Package</label>
                                                        <select name="package"
                                                            class="changePackage-select2 form-select-sm"
                                                            style="width: 100%"
                                                            data-package-id="{{ $packaging_package->package->id }}">
                                                            @foreach ($packages as $package_id => $package_name)
                                                                <option value="{{ $package_id }}"
                                                                    @if ($packaging_package->package->id == $package_id) selected @endif>
                                                                    {{ $package_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <button class="btn btn-warning mt-2 w-100" type="submit"
                                                            onclick="return confirm(&quot;ต้องการเปลี่ยน Package ?&quot;)">เปลี่ยน</button>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <form
                                        action="{{ route('packPaper2.0.product.packagingPackaging.deletePackage', [$product->id, $packaging_package->id]) }}"
                                        method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn"
                                            onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                                                class='bx bx-trash bx-tada-hover bx-sm'></i>

                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach

            </table>
        </div>
    @endforeach

    @push('scripts')
        <script>
            $(document).ready(function() {
                $('.changePackage-select2').each(function() {
                    var packageId = $(this).data('package-id');
                    $(this).select2({
                        theme: 'bootstrap-5',
                        placeholder: 'Select an option',
                        dropdownParent: $('#changePackage-' + packageId)
                    });
                });
            });
        </script>
    @endpush
@endsection
