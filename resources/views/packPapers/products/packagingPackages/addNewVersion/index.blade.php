@extends('packPapers.layouts.app')

@section('content')
    <div class="container">

        <div class="m-2">
            <a href="{{ route('packPaper2.0.product.packagingPackaging', [$product->id]) }}"
                class="btn btn-warning mb-2">กลับ</a>

            <h4>เพิ่ม Version ใหม่ ของ {{ $product->name }} </h4>
        </div>
        <hr>

        <nav>
            <div class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
                <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home"
                    type="button" role="tab" aria-controls="nav-home" aria-selected="true">เพิ่ม Package ใหม่</button>

                <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile"
                    type="button" role="tab" aria-controls="nav-profile" aria-selected="false">เลือกจาก Package
                    เดิม</button>

            </div>
        </nav>
        <div class="tab-content p-3 border">
            <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <h5>เพิ่ม Package ใหม่</h5>
                <form action="{{ route('packPaper2.0.product.packagingPackaging.storeNewVersion', [$product->id, 'new']) }}"
                    method="POST">
                    {{ csrf_field() }}

                    <label>Package Type </label>
                    <div class="input-group mt-2">

                        <select name="package_type_id" class="form-select-sm first-select2">
                            @foreach ($package_types as $package_types_id => $package_type_name)
                                <option value="{{ $package_types_id }}">
                                    {{ $package_type_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label>Package Name</label>
                    <input type="text" name="name" class="form-control form-control-sm" required>

                    <label>Package Description</label>
                    <input type="text" name="desc" class="form-control form-control-sm">

                    <label>Package Size</label>
                    <input type="text" name="size" class="form-control form-control-sm">

                    <label>Sap Note</label>
                    <input type="text" name="sapnote" class="form-control form-control-sm">

                    <hr>

                    <h4 class="text-center">Stamp Date</h4>

                    <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active">
                    <label>วันที่ผลิต</label>
                    <input type="text" name="mfg_front" type="text" class="form-control form-control-sm">
                    <div class="input-group mt-2">
                        <div class="on_bag_MFG_stamp mx-2">
                            <input type="radio" name="mfg_era" value="AD"
                                class="form-check-input on_bag_MFG_stamp mx-1" checked>
                            ค.ศ.
                            <input type="radio" name="mfg_era" value="BE"
                                class="form-check-input on_bag_MFG_stamp mx-1">
                            พ.ศ.
                        </div>

                        <select name="mfg_stampDateFormat" class="form-select-sm first-select2" style="width: 0px">
                            @foreach ($stamp_date_formats as $id => $name)
                                <option value="{{ $id }}">
                                    {{ $name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <input type="text" name="mfg_back" type="text" class="form-control form-control-sm mt-2">

                    <hr>

                    <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active">
                    <label>วันที่หมดอายุ</label>

                    <input type="text" name="exp_front" type="text" class="form-control form-control-sm">
                    <div class="input-group mt-2">
                        <div class="on_bag_MFG_stamp mx-2">
                            <input type="radio" name="exp_era" value="AD"
                                class="form-check-input on_bag_MFG_stamp mx-1" checked>
                            ค.ศ.
                            <input type="radio" name="exp_era" value="BE"
                                class="form-check-input on_bag_MFG_stamp mx-1">
                            พ.ศ.
                        </div>

                        <select name="exp_stampDateFormat" class="form-select-sm first-select2" style="width: 0px">
                            @foreach ($stamp_date_formats as $id => $name)
                                <option value="{{ $id }}">
                                    {{ $name }}</option>
                            @endforeach
                        </select>

                    </div>
                    <input type="text" name="exp_back" type="text" class="form-control form-control-sm mt-2">
                    <hr>

                    <div class="row">
                        <div class="col">
                            <label>น้ำหนักต่อถุง (g) (กรัม)</label>
                            <input type="number" name="inner_weight_g" class="form-control form-control-sm"
                                step="0.001" placeholder="ตัวอย่าง: 410" required>
                        </div>
                        <div class="col">
                            <label>น้ำหนักต่อถุง (g) (กรัม) (ประมาณค่า) </label>
                            <input type="text" name="inner_weight_g_es" class="form-control form-control-sm"
                                placeholder="ตัวอย่าง: 410 - 450 กรัม">
                        </div>
                    </div>

                    <label>น้ำหนักต่อ กล่อง (กิโลกรัม)</label>
                    <input type="number" name="outer_weight_kg" step="0.001" class="form-control form-control-sm"
                        placeholder="ตัวอย่าง: 25.05" required>

                    <label>จำนวนถุง/กล่อง</label>
                    <input type="number" name="number_per_pack" class="form-control form-control-sm"
                        placeholder="ตัวอย่าง: 36">

                    <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                </form>
            </div>


            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <h5>เลือกจาก Package เดิม</h5>

                <form
                    action="{{ route('packPaper2.0.product.packagingPackaging.storeNewVersion', [$product->id, 'select']) }}"
                    method="POST">
                    {{ csrf_field() }}
                    <select name="package_select" class="form-select-sm second-select2" style="width: 100%">

                        @foreach ($packages as $package_id => $package_name)
                            <option value="{{ $package_id }}">
                                {{ $package_name }}
                            </option>
                        @endforeach
                    </select>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <label>น้ำหนักต่อถุง (g) (กรัม)</label>
                            <input type="number" name="inner_weight_g" class="form-control form-control-sm"
                                step="0.001" placeholder="ตัวอย่าง: 410">
                        </div>
                        <div class="col">
                            <label>น้ำหนักต่อถุง (g) (กรัม) (ประมาณค่า) </label>
                            <input type="text" name="inner_weight_g_es" class="form-control form-control-sm"
                                placeholder="ตัวอย่าง: 410 - 450 กรัม">
                        </div>
                    </div>

                    <label>น้ำหนักต่อ กล่อง (กิโลกรัม)</label>
                    <input type="number" name="outer_weight_kg" step="0.001" class="form-control form-control-sm"
                        placeholder="ตัวอย่าง: 25.05">

                    <label>จำนวนถุง/กล่อง</label>
                    <input type="number" name="number_per_pack" class="form-control form-control-sm"
                        placeholder="ตัวอย่าง: 36">

                    <button type="submit" class="btn btn-success my-2 w-100">บันทึก</button>
                </form>

            </div>

        </div>
    </div>

    @push('scripts')
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
        <link href="path/to/select2-bootstrap-5-theme.min.css" rel="stylesheet" />


        <script>
            $('.first-select2').select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#nav-home')
            });

            $('.second-select2').select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#nav-profile')
            });
        </script>
    @endpush
@endsection
