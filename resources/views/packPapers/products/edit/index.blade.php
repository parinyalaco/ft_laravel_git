@extends('packPapers.layouts.app')

@section('content')
    <div class="container">
        <div class="m-2">
            <a href="{{ route('packPaper2.0.product') }}" class="btn btn-warning">กลับ</a>
            <h4 class="mt-2">แก้ไข {{ $product->name }} </h4>
        </div>
        <hr>
        <form action="{{ route('packPaper2.0.product.update', $product->id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <label>ชื่อ Customer</label>
            <select name="customer_id" class="select2 form-select-sm" style="width: 100%"
                data-id="{{ $product->id }}">
                @foreach ($customers as $id => $name)
                    <option value="{{ $id }}" @if ($product->customer_id == $id) selected @endif>
                        {{ $name }}
                    </option>
                @endforeach
            </select>
            <label>Product Group</label>
            <select name="product_group_id" class="select2 form-select-sm" style="width: 100%"
                data-id="{{ $product->id }}">
                @foreach ($product_groups as $id => $name)
                    <option value="{{ $id }}" @if ($product->product_group_id == $id) selected @endif>
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            <label>ชื่อ</label>
            <input type="text" name="name" class="form-control form-control-sm" value="{{ $product->name }}">

            <label>Procuct Description</label>
            <input type="text" name="product_fac" class="form-control form-control-sm"
                value="{{ $product->product_fac }}">

            <label>รายละเอียด</label>
            <textarea name="desc" cols="30" rows="5" class="form-control">{{ $product->desc }}</textarea>

            <label>SAP</label>
            <input type="text" name="SAP" class="form-control form-control-sm"value="{{ $product->SAP }}">

            <label>shelf life</label>
            <input type="number" name="shelf_life" class="form-control form-control-sm"value="{{ $product->shelf_life }}">

            <label>น้ำหนักรวม</label>
            <input type="number" name="weight_with_bag"
                class="form-control form-control-sm"value="{{ $product->weight_with_bag }}" step='0.001'>

            <label>หน่วย</label>
            <select name="unit_id" class="select2 form-select-sm" style="width: 100%"
                data-id="{{ $product->id }}">
                @foreach ($units as $id => $name)
                    <option value="{{ $id }}" @if ($product->product_group_id == $id) selected @endif>
                        {{ $name }}
                    </option>
                @endforeach
            </select>

            <button class="btn btn-warning mt-2 w-100" type="submit"
                onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">แก้ไข</button>
        </form>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function() {
                $('.select2').select2({
                    theme: 'bootstrap-5',
                    placeholder: 'Select an option',
                });
            });
        </script>
    @endpush
@endsection
