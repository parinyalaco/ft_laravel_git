@extends('packPapers.layouts.app')

@section('content')
    <div class="m-2">
        <a href="{{ route('packPaper2.0') }}" class="btn btn-warning ">กลับ</a>
        <a href="{{ route('packPaper2.0.product.create') }}" class="btn btn-success">
            <i class="fa-solid fa-plus"></i> เพิ่ม Products
        </a>
        <hr>
        <h4 class="mt-2">Prodeuct | สินค้า </h4>
    </div>



    <form action="{{ route('packPaper2.0.product') }}" method="get">
        {{ csrf_field() }}

        <div class="input-group my-3">
            <input type="text" class="form-control" name="searchProduct" placeholder="ค้นหา Product. . . " required
                value="{{ $searchProduct }}">
            <a href="{{ route('packPaper2.0.product') }}" class="btn btn-warning">คืนค่า</a>
            <button type="submit" class="btn btn-primary">ค้นหา</button>
        </div>
    </form>
    <table class="table">
        <thead>
            <tr class="table-primary">
                <th>Product</th>
                <th>Procuct Description</th>
                <th>น้ำหนักรวม</th>
                <th>Shelf life</th>
                <th>Unit</th>
                <th>Detail</th>
                <th>Packaging Package</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td> {{ $product->name }}</td>
                    <td> {{ $product->product_fac or '-' }}</td>
                    <td> {{ $product->weight_with_bag or '-' }}</td>
                    <td> {{ $product->shelf_life or '-' }}</td>
                    <td> {{ $product->unit->name or '' }}</td>
                    <td> {!! nl2br($product->desc) !!}</td>
                    <td>

                        @if (count($product->packagings) > 0)
                            <a href="{{ route('packPaper2.0.product.packagingPackaging', [$product->id]) }}"
                                class="btn btn-primary" style="width: 200px">จัดการ Package ({{count($product->packagings)}})</a>
                        @else
                            <a href="{{ route('packPaper2.0.product.packagingPackaging.addNewVersion', [$product->id]) }}"
                                class="btn btn-success" style="width: 200px">
                                <i class="fa-solid fa-plus"></i> เพิ่ม Version ใหม่
                            </a>
                        @endif
                    </td>
                    <td>
                        <div class="btn-group">

                            <a href="{{ route('packPaper2.0.product.edit', [$product->id]) }}" class="btn">
                                <i class='bx bx-edit-alt bx-tada-hover bx-sm'></i>
                            </a>

                            <form action="{{ route('packPaper2.0.product.delete', $product->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)"> <i
                                        class='bx bx-trash bx-tada-hover bx-sm'></i>

                                </button>
                            </form>
                        </div>

                    </td>

                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $products->appends(['searchProduct' => $searchProduct])->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}
@endsection
