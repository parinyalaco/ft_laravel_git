@extends('packPapers.layouts.app')

@section('content')
    <div class="m-2">
        <a href="{{ route('packPaper2.0') }}" class="btn btn-warning ">กลับ</a>
        <a href="{{ route('packPaper2.0.package.create') }}" class="btn btn-success">
            <i class="fa-solid fa-plus"></i> เพิ่ม Package
        </a>

        <hr>
        <h4 class="mt-2">Packages</h4>
        <form action="{{ route('packPaper2.0.product') }}" method="get">
            {{ csrf_field() }}

            <div class="input-group my-3">
                <input type="text" class="form-control" name="searchTxt" placeholder="ค้นหา . . . " required
                    value="{{ $searchTxt }}">
                <a href="{{ route('packPaper2.0.product') }}" class="btn btn-warning">คืนค่า</a>
                <button type="submit" class="btn btn-primary">ค้นหา</button>
            </div>
        </form>


        <table class="table">
            <thead>
                <tr class="table-primary">
                    <th>Name</th>
                    <th>Description</th>
                    <th>Size</th>
                    <th>Stamp Format</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($packages as $package)
                    <tr>
                        <td>{{ $package->name }}</td>
                        <td>{{ $package->desc }}</td>
                        <td>{{ $package->size }}</td>
                        <td>

                        </td>
                        <td>{{ $package->status }}</td>

                        <td>
                            <a href="{{ route('packPaper2.0.package.view', [$package->id]) }}"
                                class="btn btn-primary">จัดการ</a>

                            <div class="btn-group">
                                <a href="{{ route('packPaper2.0.package.edit', [$package->id]) }}" class="btn"
                                    title="แก้ไข Package">
                                    <i class='bx bx-edit-alt bx-tada-hover bx-sm'></i>
                                </a>
                                <form action="{{ route('packPaper2.0.package.delete', $package->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)"
                                        title="ลบ Package">
                                        <i class='bx bx-trash bx-tada-hover bx-sm'></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>


        </table>



    </div>
@endsection
