@extends('packPapers.layouts.app')

@section('content')
    <div class="container">

        <div class="m-2">
            <a href="{{ route('packPaper2.0.package') }}" class="btn btn-warning ">กลับ</a>


            <hr>
            <h4 class="mt-2">แก้ไข Package {{ $package->name }}</h4>

            <form action="{{ route('packPaper2.0.package.update', [$package->id]) }}" method="POST"
                enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <label>Package Type</label>

                <select name="package_type_id" class="form-select-sm select2" style="width: 100%"
                    data-id="{{ $package->id }}">
                    @foreach ($package_types as $package_type_id => $package_type_name)
                        <option value="{{ $package_type_id }}" @if ($package->package_type_id == $package_type_id) selected @endif>
                            {{ $package_type_name }}
                        </option>
                    @endforeach
                </select>
                <label>Name</label>
                <input type="text" name="name" class="form-control" value="{{ $package->name }}">

                <label>Description</label>
                <input type="text" name="desc" cols="30" rows="5" class="form-control"
                    value="{{ $package->desc }}">

                <label>Size</label>
                <input type="text" name="size" class="form-control" value="{{ $package->size }}">

                <label>Sapnote</label>
                <input type="text" name="sapnote" class="form-control" value="{{ $package->sapnote }}">
                <hr>

                <h5 class="text-center">Stamp Date</h5>
                @if (isset($package->stampFormat->mfg))
                    <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active"
                        @if ($package->stampFormat->mfg->status == 'Active') checked @endif>
                    <label>วันที่ผลิต</label>
                    <input type="text" name="mfg_front" type="text" class="form-control form-control-sm"
                        value="{{ $package->stampFormat->mfg->front_text }}">
                    <div class="input-group mt-2">
                        <div class="on_bag_MFG_stamp mx-2">
                            <input type="radio" name="mfg_era" value="AD"
                                class="form-check-input on_bag_MFG_stamp mx-1"
                                @if ($package->stampFormat->mfg->date_era_format == 'AD') checked @endif>
                            ค.ศ.
                            <input type="radio" name="mfg_era" value="BE"
                                class="form-check-input on_bag_MFG_stamp mx-1"
                                @if ($package->stampFormat->mfg->date_era_format == 'BE') checked @endif>
                            พ.ศ.
                        </div>

                        <select name="mfg_stampDateFormat" class="form-select-sm select2" style="width: 0px">
                            @foreach ($stamp_date_formats as $id => $name)
                                <option value="{{ $id }}" @if ($package->stampFormat->mfg->stamp_date_format_id == $id) selected @endif>
                                    {{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="text" name="mfg_back" type="text" class="form-control form-control-sm mt-2"
                        value="{{ $package->stampFormat->mfg->lot }}">
                @else
                    <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active">
                    <label>วันที่ผลิต</label>
                    <input type="text" name="mfg_front" type="text" class="form-control form-control-sm">
                    <div class="input-group mt-2">
                        <div class="on_bag_MFG_stamp mx-2">
                            <input type="radio" name="mfg_era" value="AD"
                                class="form-check-input on_bag_MFG_stamp mx-1" checked>
                            ค.ศ.
                            <input type="radio" name="mfg_era" value="BE"
                                class="form-check-input on_bag_MFG_stamp mx-1">
                            พ.ศ.
                        </div>

                        <select name="mfg_stampDateFormat" class="form-select-sm select2" style="width: 0px">
                            @foreach ($stamp_date_formats as $id => $name)
                                <option value="{{ $id }}">
                                    {{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="text" name="mfg_back" type="text" class="form-control form-control-sm mt-2">
                @endif

                <hr>

                @if (isset($package->stampFormat->exp))
                    <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active"
                        @if ($package->stampFormat->exp->status == 'Active') checked @endif>
                    <label>วันที่หมดอายุ</label>
                    <input type="text" name="exp_front" type="text" class="form-control form-control-sm"
                        value="{{ $package->stampFormat->exp->front_text }}">
                    <div class="input-group mt-2">
                        <div class="on_bag_MFG_stamp mx-2">
                            <input type="radio" name="exp_era" value="AD"
                                class="form-check-input on_bag_MFG_stamp mx-1"
                                @if ($package->stampFormat->exp->date_era_format == 'AD') checked @endif>
                            ค.ศ.
                            <input type="radio" name="exp_era" value="BE"
                                class="form-check-input on_bag_MFG_stamp mx-1"
                                @if ($package->stampFormat->exp->date_era_format == 'BE') checked @endif>
                            พ.ศ.
                        </div>

                        <select name="exp_stampDateFormat" class="form-select-sm select2">
                            @foreach ($stamp_date_formats as $id => $name)
                                <option value="{{ $id }}" @if ($package->stampFormat->exp->stamp_date_format_id == $id) selected @endif>
                                    {{ $name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <input type="text" name="exp_back" type="text" class="form-control form-control-sm mt-2"
                        value="{{ $package->stampFormat->exp->lot }}">
                @else
                    <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active">
                    <label>วันที่หมดอายุ</label>
                    <input type="text" name="exp_front" type="text" class="form-control form-control-sm">
                    <div class="input-group mt-2">
                        <div class="on_bag_MFG_stamp mx-2">
                            <input type="radio" name="exp_era" value="AD" class="form-check-input mx-1" checked>
                            ค.ศ.
                            <input type="radio" name="exp_era" value="BE" class="form-check-input mx-1">
                            พ.ศ.
                        </div>


                        <select name="exp_stampDateFormat" class="form-select-sm select2"
                            @if ($package->stampFormat->mfg->stamp_date_format_id == $id) selected @endif>
                            @foreach ($stamp_date_formats as $id => $name)
                                <option value="{{ $id }}">
                                    {{ $name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <input type="text" name="exp_back" type="text" class="form-control form-control-sm mt-2">
                @endif
                <button class="btn btn-warning mt-2 w-100"
                    onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">แก้ไข</button>
            </form>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready(function() {
                // Initialize Select2 for existing elements
                $('.select2').select2({
                    theme: 'bootstrap-5',
                    placeholder: 'Select an option',
                    // dropdownParent: $('#createDeliveryPlanModal')
                });
            })
        </script>
    @endpush
@endsection
