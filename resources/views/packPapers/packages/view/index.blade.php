@extends('packPapers.layouts.app')

@push('styles')
    <style>
        .image-area {
            position: relative;
            width: 50%;
            margin: auto;
        }

        .image-area img {
            max-width: 100%;
            height: auto;
            display: block;
        }

        .remove-image {
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 50%;
            padding: 10px;
            background: rgba(255, 0, 0, 0.7);
            color: #fff;
            border: 2px solid #fff;
            cursor: pointer;
            transition: background 0.3s ease;
            font-size: 18px;
            width: 40px;
            height: 40px;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        */
    </style>
@endpush

@section('content')
    <div class="container">
        <a href="{{ route('packPaper2.0.package') }}" class="btn btn-warning ">กลับ</a>
        <hr>
        <h4 class="mt-2">จัดการ Package {{ $package->name }}</h4>
        <a href="{{ route('packPaper2.0.package.edit', [$package->id]) }}" class="btn btn-warning" title="แก้ไข Package"
            target="_blank">
            <i class="fa-solid fa-pen"></i> แก้ไข
        </a>
        <div class="btn-group">
            <form action="{{ route('packPaper2.0.package.delete', $package->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" class="btn btn-danger" onclick="return confirm(&quot;ต้องการลบ ?&quot;)"
                    title="ลบ Package">
                    <i class="fa-solid fa-trash"></i> ลบ
                </button>
            </form>
        </div>
        <button type="button" class="btn btn-primary my-2" data-toggle="modal"
            data-target="#addPackageImgModal-{{ $package->id }}">
            เพิ่มรูป Package
        </button>

        <!-- Modal -->
        <div class="modal fade" id="addPackageImgModal-{{ $package->id }}" tabindex="-1" role="dialog"
            aria-labelledby="addPackageImgModalLabel-{{ $package->id }}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addPackageImgModalLabel-{{ $package->id }}">เพิ่มรูป Package
                        </h5>
                        <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('packPaper2.0.package.img.storePackageImg', $package->id) }}" method="post"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>ตำแหน่ง</label>
                                <input type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>รูปหลัก</label>

                                <img id="blah" class="img-fluid my-2 mx-auto d-block"
                                    data-id="{{ $package->id }}package_main_imgs" width="150px">

                                <input type="file" class="form-control" data-id="{{ $package->id }}package_main_imgs"
                                    onchange="readURL(this);" accept="image/*" name="package_main_imgs">
                            </div>

                            <div class="form-group">
                                <label>รูป stamp</label>

                                <img id="blah" class="img-fluid my-2 mx-auto d-block"
                                    data-id="{{ $package->id }}package_stamp_imgs" width="150px">

                                <input type="file" class="form-control" data-id="{{ $package->id }}package_stamp_imgs"
                                    onchange="readURL(this);" accept="image/*" name="package_stamp_imgs">
                            </div>

                            <div class="form-group">
                                <label>Stamp Format</label>
                                {{-- <input type="text" name="stamp_format" class="form-control"> --}}
                                <textarea type="text" name="stamp_format" class="form-control"></textarea>

                            </div>

                            <div class="form-group">
                                <label>รายละเอียด</label>
                                <input type="text" name="detail" class="form-control">
                            </div>

                            <hr>
                            <button type="submit" class="btn btn-success w-100">
                                เพิ่มรูป
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered">
            <tr>
                <th class="w-25" style="background-color: #eeeeee">Name</th>
                <td class="w-50">{{ $package->name }}</td>
            </tr>

            <tr>
                <th class="w-25" style="background-color: #eeeeee">Description</th>
                <td class="w-50">{{ $package->desc }}</td>
            </tr>
            <tr>
                <th class="w-25" style="background-color: #eeeeee">Size</th>
                <td class="w-50">{{ $package->size }}</td>
            </tr>
            <tr>
                <th class="w-25" style="background-color: #eeeeee">Status</th>
                <td class="w-50">{{ $package->status }}</td>
            </tr>
        </table>


        @if (count($package->packagesImgs) > 0)
            @foreach ($package->packagesImgs as $packagesImg)
                <div class="row">
                    <div class="col">
                        <strong> ตำแหน่ง:</strong> {{ $packagesImg->name }}
                    </div>
                    <div class="col col-auto">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <button class="btn" data-toggle="modal"
                                data-target="#package_imgModal-{{ $packagesImg->id }}">
                                <i class='bx bx-edit-alt bx-sm'></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="package_imgModal-{{ $packagesImg->id }}" tabindex="-1"
                                role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">แก้ไข
                                                {{ $packagesImg->name }}</h5>
                                            <button type="button" class="btn close btn-close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <form
                                            action="{{ route('packPaper2.0.package.img.updatePackageImg', $packagesImg->id) }}"
                                            method="post" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                {{ method_field('PUT') }}
                                                {{ csrf_field() }}
                                                <label>ตำแหน่ง</label>
                                                <input type="text" name="name" class="form-control"
                                                    value="{{ $packagesImg->name }}">

                                                <!-- รูปหลัก -->
                                                <label>รูปหลัก</label>

                                                <div class="image-area">
                                                    @if ($packagesImg->main_img_path)
                                                        <img id="mainImagePreview-{{ $packagesImg->id }}"
                                                            src="{{ asset('storage/' . $packagesImg->main_img_path) }}"
                                                            class="img-fluid my-2 mx-auto d-block"
                                                            data-id="main-{{ $packagesImg->id }}" width="150px">

                                                        <!-- ปุ่มลบรูปภาพ -->

                                                        <button type="button" class="remove-image btn"
                                                            onclick="removeImage('main_img_path', 'mainImagePreview-{{ $packagesImg->id }}', 'existing_main_img_path')">
                                                            <i class="fa-solid fa-xmark"></i>
                                                        </button>
                                                    @else
                                                        <img id="mainImagePreview"
                                                            src="{{ asset('storage/icons/image.png') }}"
                                                            class="img-fluid my-2 mx-auto d-block"
                                                            data-id="main-{{ $packagesImg->id }}" width="150px">
                                                    @endif
                                                </div>

                                                <!-- Hidden input สำหรับเก็บค่า main_img_path -->
                                                <input type="hidden" id="existing_main_img_path"
                                                    name="existing_main_img_path"
                                                    value="{{ $packagesImg->main_img_path }}"
                                                    value="{{ $packagesImg->main_img_path }}">

                                                <!-- ฟอร์มสำหรับอัปโหลดรูปหลัก -->
                                                <input type="file" class="form-control"
                                                    data-id="main-{{ $packagesImg->id }}"
                                                    onchange="readURL(this, 'mainImagePreview');" accept="image/*"
                                                    name="main_img_path">


                                                <!-- รูป stamp -->
                                                <label>รูป stamp</label>
                                                @if ($packagesImg->stamp_img_paht)
                                                    <div class="image-area">
                                                        <img id="stampImagePreview-{{ $packagesImg->id }}"
                                                            class="mx-auto d-block my-2"
                                                            src="{{ asset('storage/' . $packagesImg->stamp_img_paht) }}"
                                                            data-id="stamp-{{ $packagesImg->id }}" width="150px" />
                                                        <!-- ปุ่มลบรูป stamp -->

                                                        <button type="button" class="btn btn-danger my-2 remove-image"
                                                            onclick="removeImage('stamp_img_paht', 'stampImagePreview-{{ $packagesImg->id }}', 'existing_stamp_img_paht')">
                                                            <i class="fa-solid fa-xmark"></i>
                                                        </button>
                                                    </div>
                                                @else
                                                    <img id="stampImagePreview" class="mx-auto d-block my-2"
                                                        src="{{ asset('storage/icons/image.png') }}"
                                                        data-id="stamp-{{ $packagesImg->id }}" width="150px" />
                                                @endif

                                                <!-- Hidden input สำหรับเก็บค่า stamp_img_paht -->
                                                <input type="hidden" id="existing_stamp_img_paht"
                                                    name="existing_stamp_img_paht"
                                                    value="{{ $packagesImg->stamp_img_paht }}"
                                                    value="{{ $packagesImg->stamp_img_paht }}">

                                                <!-- ฟอร์มสำหรับอัปโหลดรูป stamp -->
                                                <input type="file" class="img-fluid form-control"
                                                    data-id="stamp-{{ $packagesImg->id }}"
                                                    onchange="readURL(this, 'stampImagePreview');" accept="image/*"
                                                    name="stamp_img_paht" />


                                                <label>Stamp Format</label>
                                                <textarea name="stamp_format" class="form-control">{{ $packagesImg->stamp_format }}</textarea>

                                                {{-- <input type="text" name="stamp_format" class="form-control"
                                                    value="{{ $packagesImg->stamp_format }}"> --}}

                                                <label>รายละเอียด</label>
                                                <input type="text" name="detail" class="form-control"
                                                    value="{{ $packagesImg->detail }}">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">ปิด</button>
                                                <button type="submit" class="btn btn-primary">บันทึก</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <form action="{{ route('packPaper2.0.package.img.deletePackageImg', $packagesImg->id) }}"
                                method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)">
                                    <i class='bx bx-trash bx-tada-hover bx-sm'></i>

                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <strong> Stamp Format:</strong> {{ $packagesImg->stamp_format }}
                <strong>รายละเอียด:</strong> {{ $packagesImg->detail }}
                <div class="modal-img-container">
                    <div class="row">
                        <div class="col">

                            @if ($packagesImg->main_img_path)
                                <a type="button" data-toggle="modal"
                                    data-target="#main_img_path{{ $packagesImg->id }}">
                                    <img src="{{ asset('storage/' . $packagesImg->main_img_path) }}" width="500px">
                                </a>
                                <!-- main_img_path Modal -->
                                <div class="modal fade" id="main_img_path{{ $packagesImg->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    รูปหลัก</h5>
                                                <button type="button" class="close btn-close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="{{ asset('storage/' . $packagesImg->main_img_path) }}"
                                                    width="750px">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col">
                            @if ($packagesImg->stamp_img_paht)
                                <a type="button" data-toggle="modal"
                                    data-target="#stamp_img_paht{{ $packagesImg->id }}">
                                    <img src="{{ asset('storage/' . $packagesImg->stamp_img_paht) }}" width="500px">
                                </a>
                                <!-- stamp_img_paht Modal -->
                                <div class="modal fade" id="stamp_img_paht{{ $packagesImg->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    รูป Stamp </h5>
                                                <button type="button" class="close btn-close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="{{ asset('storage/' . $packagesImg->stamp_img_paht) }}"
                                                    width="750px">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <hr>
                </div>
            @endforeach
        @endif
    </div>

    @push('scripts')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
            $('.addNewPackageSelect2').select2({
                placeholder: 'Select an option',
                dropdownParent: '#addNewPackageModal'
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        var dataId = $(input).data('id');
                        $('img[data-id="' + dataId + '"]').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>


        <script>
            function removeImage(inputName, previewId, hiddenInputId) {
                // ตั้งค่า input ของรูปเป็น null
                document.querySelector(`input[name="${inputName}"]`).value = null;

                // ตั้งค่า hidden input เป็น null
                document.getElementById(hiddenInputId).value = null;

                // ซ่อนหรือเคลียร์รูปภาพที่แสดงอยู่
                const imgElement = document.getElementById(previewId);
                if (imgElement) {
                    imgElement.src = '';
                }

                // ซ่อนปุ่มลบ
                const removeButton = imgElement.nextElementSibling;
                if (removeButton && removeButton.classList.contains('remove-image')) {
                    removeButton.style.display = 'none';
                }
            }
        </script>
        <script>
            function removeImage(inputName, previewId, hiddenInputId) {
                // ตั้งค่า input ของรูปเป็น null
                document.querySelector(`input[name="${inputName}"]`).value = null;

                // ตั้งค่า hidden input เป็น null
                document.getElementById(hiddenInputId).value = null;

                // ซ่อนหรือเคลียร์รูปภาพที่แสดงอยู่
                const imgElement = document.getElementById(previewId);
                if (imgElement) {
                    imgElement.src = '';
                }

                // ซ่อนปุ่มลบ
                const removeButton = imgElement.nextElementSibling;
                if (removeButton && removeButton.classList.contains('remove-image')) {
                    removeButton.style.display = 'none';
                }
            }
        </script>
    @endpush

@endsection
