@extends('packPapers.layouts.app')

@section('content')
    <div class="container">

        <div class="m-2">
            <a href="{{ route('packPaper2.0.package') }}" class="btn btn-warning ">กลับ</a>

            <hr>
            <h4 class="mt-2">เพิ่ม Package ใหม่</h4>

            <form action="{{ route('packPaper2.0.package.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}


                <label>Package Type</label>

                <select name="package_type_id" class="form-select-sm select2" style="width: 100%">
                    @foreach ($package_types as $package_type_id => $package_type_name)
                        <option value="{{ $package_type_id }}">
                            {{ $package_type_name }}
                        </option>
                    @endforeach
                </select>
                <label>Name</label>
                <input type="text" name="name" class="form-control">

                <label>Description</label>
                <textarea name="desc" cols="30" rows="5" class="form-control"> </textarea>

                <label>Size</label>
                <input type="text" name="size" class="form-control">

                <label>Sapnote</label>
                <input type="text" name="sapnote" class="form-control">
                <hr>

                <h5 class="text-center">Stamp Date</h5>

                <input id="on_bag_MFG" name="mfg_status" type="checkbox" class="mx-2" value="Active">
                <label>วันที่ผลิต</label>
                <input type="text" name="mfg_front" type="text" class="form-control form-control-sm">
                <div class="input-group mt-2">
                    <div class="on_bag_MFG_stamp mx-2">
                        <input type="radio" name="mfg_era" value="AD" class="form-check-input on_bag_MFG_stamp mx-1"
                            checked>
                        ค.ศ.
                        <input type="radio" name="mfg_era" value="BE" class="form-check-input on_bag_MFG_stamp mx-1">
                        พ.ศ.
                    </div>

                    <select name="mfg_stampDateFormat" class="form-select-sm select2" style="width: 0px">
                        @foreach ($stamp_date_formats as $id => $name)
                            <option value="{{ $id }}">
                                {{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="text" name="mfg_back" type="text" class="form-control form-control-sm mt-2">
                <hr>
                <input id="on_bag_MFG" name="exp_status" type="checkbox" class="mx-2" value="Active">
                
                <label>วันที่หมดอายุ</label>
                <input type="text" name="exp_front" type="text" class="form-control form-control-sm">
                <div class="input-group mt-2">
                    <div class="on_bag_MFG_stamp mx-2">
                        <input type="radio" name="exp_era" value="AD" class="form-check-input mx-1" checked>
                        ค.ศ.
                        <input type="radio" name="exp_era" value="BE" class="form-check-input mx-1">
                        พ.ศ.
                    </div>
                    <select name="exp_stampDateFormat" class="form-select-sm select2">
                        @foreach ($stamp_date_formats as $id => $name)
                            <option value="{{ $id }}">
                                {{ $name }}</option>
                        @endforeach
                    </select>

                </div>

                <input type="text" name="exp_back" type="text" class="form-control form-control-sm mt-2">

                <button class="btn btn-success mt-2 w-100"
                    onclick="return confirm(&quot;ต้องการแก้ไข ?&quot;)">บันทึก</button>
            </form>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready(function() {
                $('.select2').select2({
                    theme: 'bootstrap-5',
                    placeholder: 'Select an option',
                });
            })
        </script>
    @endpush
@endsection
