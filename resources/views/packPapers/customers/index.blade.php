@extends('packPapers.layouts.app')

@section('content')
    <div class="m-2">
        <a href="{{ route('packPaper2.0') }}" class="btn btn-warning ">กลับ</a>

        <!-- Button to trigger modal -->
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createNewUnit">
            <i class="fa-solid fa-plus"></i> เพิ่ม Customer ใหม่
        </button>

        <!-- Modal -->
        <div class="modal fade" id="createNewUnit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">สร้าง Unit ใหม่</h5>
                        <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"></span>
                        </button>
                    </div>
                    <form action="{{ route('packPaper2.0.customer.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="modal-body">
                            <label>ชื่อ</label>
                            <input type="text" name="name" class="form-control" required
                                placeholder="ตัวอย่าง : Carton">

                            <label>รายละเอียด</label>
                            <input type="text" name="desc" class="form-control" required
                                placeholder="ตัวอย่าง : Carton is a light box or container">

                            <label>สถานะ</label>
                            <select class="form-select" name='status'>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                            </select>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <form action="{{ route('packPaper2.0.customer') }}" method="get">
            {{ csrf_field() }}

            <div class="input-group my-3">
                <input type="text" class="form-control" name="searchCustomer" placeholder="ค้นหา Customer. . . " required
                    value="{{ $searchCustomer }}">
                <a href="{{ route('packPaper2.0.customer') }}" class="btn btn-warning">คืนค่า</a>
                <button type="submit" class="btn btn-primary">ค้นหา</button>
            </div>
        </form>

        <table class="table">
            <thead>
                <tr class="table-primary">
                    <th>ชื่อ</th>
                    <th>รายละเอียด</th>
                    <th>สถานะ</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>

                @foreach ($customers as $customer)
                    <tr>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->desc }}</td>
                        <td>{{ $customer->status }}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <!-- Button to trigger modal -->
                                <button type="button" class="btn " data-toggle="modal"
                                    data-target="#editUnit-{{ $customer->id }}">
                                    <i class='bx bx-edit-alt  bx-sm'></i>
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="editUnit-{{ $customer->id }}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">แก้ไข Customer
                                                    {{ $customer->name }}</h5>
                                                <button type="button" class="close btn-close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <form action="{{ route('packPaper2.0.customer.update', $customer->id) }}"
                                                method="post" enctype="multipart/form-data">
                                                {{ method_field('PUT') }}
                                                {{ csrf_field() }}
                                                <div class="modal-body">
                                                    <label>ชื่อ</label>
                                                    <input type="text" name="name" class="form-control" required
                                                        value="{{ $customer->name }}">

                                                    <label>รายละเอียด</label>
                                                    <input type="text" name="desc" class="form-control" required
                                                        value="{{ $customer->desc }}">

                                                    <label>สถานะ</label>
                                                    <select class="form-select" name='status'>
                                                        <option value="Active"
                                                            @if ($customer->status == 'Active') selected @endif>Active
                                                        </option>
                                                        <option value="Inactive"
                                                            @if ($customer->status == 'Inactive') selected @endif>Inactive
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">ปิด</button>
                                                    <button type="submit" class="btn btn-primary">บันทึก</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <form method="POST" action="{{ route('packPaper2.0.customer.destroy', $customer->id) }}"
                                    style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn" title="Delete %%modelName%%"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                            class='bx bx-trash bx-sm'></i></button>

                                </form>

                            </div>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $customers->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}


    </div>
@endsection
