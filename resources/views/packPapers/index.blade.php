@extends('packPapers.layouts.app')

@section('content')
    <div class="m-2">
        @include('packPapers.components.newDeliveryPlan')

        <h4 class="mt-2">Delivery Plan</h4>
        <table class="table table table-striped text-center">
            <thead>
                <tr class="table-primary">
                    <th>Customer Name</th>
                    <th>Booking No.</th>
                    <th>Orders</th>
                    <th>Customer PO No.</th>
                    <th>Product</th>
                    <th>น้ำหนัก</th>
                    <th>จำนวน</th>
                    <th>สถานะใบแจ้งบรรจุ</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($deliveryPlans as $deliveryPlan)
                    <tr>
                        <td>{{ $deliveryPlan->customer->name or '-' }}</td>
                        <td>{{ $deliveryPlan->booking_no or '-' }}</td>
                        <td>{{ $deliveryPlan->orders or '-' }}</td>
                        <td>{{ $deliveryPlan->customer_po_no or '-' }}</td>


                        @foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
                            @if (isset($delivery_plan_product->product))
                                <td>
                                    {{ $delivery_plan_product->product->name }}
                                </td>
                                <td>
                                    {{ number_format($delivery_plan_product->weight, 2) }}
                                </td>
                                <td>
                                    {{ number_format($delivery_plan_product->quantity) }}
                                </td>
                            @endif
                        @endforeach


                        <td>
                            @include('packPapers.components.packpaperStatus')
                        </td>

                        <td style="width: 250px" class="text-center">
                            <a href="{{ route('packPaper2.0.deliveryPlan.view', [$deliveryPlan->id]) }}"
                                class="btn btn-primary" onclick="return confirm('ไปหน้า ทำใบแจ้งบรรจุ')">ทำใบแจ้งบรรจุ</a>
                            <div class="btn-group">
                                <form action="{{ route('packPaper2.0.deliveryPlan.delete', $deliveryPlan->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn" onclick="return confirm(&quot;ต้องการลบ ?&quot;)">
                                        <i class='bx bx-trash bx-tada-hover bx-sm'></i>

                                    </button>
                                </form>
                            </div>
                        </td>

                    </tr>
                @endforeach

            </tbody>
        </table>

        {{ $deliveryPlans->links('pagination::bootstrap-4', ['style' => 'margin-top: 20px;']) }}

    </div>


    @include('packPapers.components.script')
@endsection
 
