@push('scripts')


    <script>
        $(document).ready(function() {
            // Initialize Select2 for existing elements
            $('.first-select2').select2({
                theme: 'bootstrap-5',
                placeholder: 'Select an option',
                dropdownParent: $('#createDeliveryPlanModal')
            });

            // Initialize DataTable
            new DataTable('#deliveryPlanTable', {
                order: [
                    [3, 'desc']
                ]
            });

            // Initialize Select2 for editDeliveryPlan-select2 elements
            $('.editDeliveryPlan-select2').each(function() {
                var deliveryPlanId = $(this).data('id');
                $(this).select2({
                    theme: 'bootstrap-5',
                    placeholder: 'Select an option',
                    dropdownParent: $('#editDeliveryPlan-' + deliveryPlanId)
                });
            });

            function calculateBoxes() {
                var $parent = $(this).closest('.form-group');
                var selectedOption = $parent.find('.product-select option:selected');
                var weightWithBag = parseFloat(selectedOption.data('weight'));
                var totalWeight = parseFloat($parent.find('.weight-input').val());

                if (!isNaN(weightWithBag) && !isNaN(totalWeight) && weightWithBag > 0) {
                    var numberOfBoxes = Math.floor(totalWeight / weightWithBag);
                    $parent.find('.calculated-boxes').text(`จำนวนกล่อง: ${numberOfBoxes}`);
                } else {
                    $parent.find('.calculated-boxes').text('จำนวนกล่อง: ข้อมูลไม่ถูกต้อง');
                }
            }
        });
    </script>
@endpush
