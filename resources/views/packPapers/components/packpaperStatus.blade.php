@foreach ($deliveryPlan->delivery_plan_products as $delivery_plan_product)
    @if (isset($delivery_plan_product->product))
        @if (isset($delivery_plan_product->pack_paper))
            @if ($delivery_plan_product->pack_paper->status == 'Active')
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" style="width: 200px"
                    data-target="#pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                    ใบแจ้งบรรจุ เลขที่ {{ $delivery_plan_product->pack_paper->id }}
                </button>
            @else
                <button class="btn btn-danger btn-sm"
                    onclick="return alert('{{ $delivery_plan_product->pack_paper->remark }}')">
                    ใบแจ้งบรรจุเลขที่{{ $delivery_plan_product->pack_paper->id }} ถูกยกเลิก </button>
            @endif

            <div class="modal" id="pack_paper-{{ $delivery_plan_product->pack_paper->id }}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{ route('packPaper2.0.deliveryPlan.createPackPaperDoc', [$delivery_plan_product->pack_paper->id]) }}"
                            method="post" enctype="multipart/form-data" target="_blank">

                            <div class="modal-header">
                                <h5 class="modal-title">รายละเอียด วันที่ผลิต
                                    ของใบแจ้งบรรจุ
                                </h5>
                                <button type="button" class="close btn-close " data-dismiss="modal"></button>
                            </div>
                            <div class="modal-body">

                                {{ csrf_field() }}
                                @foreach ($delivery_plan_product->pack_paper->pack_paper_lots as $pack_paper_lot)
                                    <label>วันที่ผลิตของLot.
                                        <strong>{{ $pack_paper_lot->lot }}</strong></label>
                                    <div class="input-group mb-3">

                                        <input type="date" name="mfg_month[{{ $pack_paper_lot->id }}]"
                                            class="form-control form-control-sm"
                                            value="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}"
                                            min="{{ date('Y-m-d', strtotime($pack_paper_lot->mfg_date)) }}">

                                        <input type="text" name="remark[{{ $pack_paper_lot->id }}]"
                                            value="{{ $pack_paper_lot->remark }}" class="form-control form-control-sm"
                                            placeholder="หมายเหตุ">
                                    </div>
                                @endforeach
                                <input type="text" name="packPaperRemark" class="form-control form-control-sm"
                                    value="{{ $delivery_plan_product->pack_paper->remark }}">

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                    data-target="#cancelModal-{{ $delivery_plan_product->pack_paper->id }}">
                                    ยกเลิกใบแจ้งบรรจุ
                                </button>
                                <button type="submit" class="btn btn-success"
                                    onclick="return confirm('ไปหน้า ใบบรรจุ')">ใบแจ้งบรรจุ
                                    เลขที่
                                    {{ $delivery_plan_product->pack_paper->id }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="cancelModal-{{ $delivery_plan_product->pack_paper->id }}" tabindex="-1"
                aria-labelledby="nestedModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="nestedModalLabel">
                                ยกเลิกใบแจ้งบรรจุเลขที่
                                {{ $delivery_plan_product->pack_paper->id }}
                            </h5>
                            <button type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <form action="{{ route('packPaper2.0.deliveryPlan.cancelPackPaper', [$delivery_plan_product->pack_paper->id]) }}"
                            method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <label>รายละเอียด</label>
                                <input type="text" name="remark" class="form-control form-control-sm">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger my-2">บันทึก</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @else
            <div class="alert-danger">
                ยังไม่ได้ทำใบแจ้งบรรจุ
            </div>
        @endif
    @endif
@endforeach
