<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FT Form Alert</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body>
    <p><strong>TO..ALL</strong></p>
    <p><strong>แผนกำลังคน วันที่ {{$extrajobdataobj['selecteddate']}} กะ {{$extrajobdataobj['shiftname']}}</strong></p>

    @if (!empty($extrajobdataobj['logpackms']) || !empty($extrajobdataobj['logselectms']))
        <p>
            <h3>กำลังคน PK</h3>
            <table>
                <thead>
                    <tr>
                        <th>วันที่</th>
                        <th>ส่วนงาน</th>
                        <th>กะ</th>
                        <th>เป้าพนักงาน</th>
                        <th>ผู้ดูแล</th>
                        <th>พนักงาน PK</th>
                        <th>ผู้ช่วยงานจาก PF</th>
                        <th>ผู้ช่วยงานจาก RTE</th>
                        <th>ผลต่างจำนวนคน</th>
                        <th>สินค้า</th>
                        <th>Plan</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $totalPK = [];
                        $totalPK['staff_target'] = 0;
                        $totalPK['staff_pk'] = 0;
                        $totalPK['staff_pf'] = 0;
                        $totalPK['staff_pst'] = 0;
                        $totalPK['staff_diff'] = 0;
                    @endphp
                    @if (!empty($extrajobdataobj['logpackms']))
                    @foreach ($extrajobdataobj['logpackms'] as $item)
                    <tr>
                        <td>{{  $item->process_date }}</td>
                        <td>{{  $item->methodname }}</td>
                        <td>{{  $item->shiftname }}</td>
                        <td>{{  $item->staff_target }}</td>
                        <td>{{  $item->staff_operate }}</td>
                        <td>{{  $item->staff_pk }}</td>
                        <td>{{  $item->staff_pf }}</td>
                        <td>{{  $item->staff_pst }}</td>
                        <td>{{  $item->staff_diff }}</td>
                        <td>{{  $item->packagename }}</td>
                        <td>{{  number_format($item->Plan,2,".",",") }}</td>
                        <td>{{  $item->Remark }}</td>
                    </tr>
                    @php
                        $totalPK['staff_target'] += $item->staff_target;
                        $totalPK['staff_pk'] += $item->staff_pk;
                        $totalPK['staff_pf'] += $item->staff_pf;
                        $totalPK['staff_pst'] += $item->staff_pst;
                        $totalPK['staff_diff'] += $item->staff_diff;
                    @endphp
                    @endforeach    
                    @endif
                    @if (!empty($extrajobdataobj['logselectms']))
                    @foreach ($extrajobdataobj['logselectms'] as $item)
                    <tr>
                        <td>{{  $item->process_date }}</td>
                        <td>งานคัด</td>
                        <td>{{  $item->shiftname }}</td>
                        <td>{{  $item->staff_target }}</td>
                        <td>{{  $item->staff_operate }}</td>
                        <td>{{  $item->staff_pk }}</td>
                        <td>{{  $item->staff_pf }}</td>
                        <td>{{  $item->staff_pst }}</td>
                        <td>{{  $item->staff_diff }}</td>
                        <td>{{  $item->productname }}</td>
                        <td>{{  number_format($item->Plan,2,".",",") }}</td>
                        <td>{{  $item->Remark }}</td>
                    </tr>
                    @endforeach
                    @php
                        $totalPK['staff_target'] += $item->staff_target;
                        $totalPK['staff_pk'] += $item->staff_pk;
                        $totalPK['staff_pf'] += $item->staff_pf;
                        $totalPK['staff_pst'] += $item->staff_pst;
                        $totalPK['staff_diff'] += $item->staff_diff;
                    @endphp
                    @endif
                    <tr>
                        <td></td>
                        <td>รวม</td>
                        <td></td>
                        <td>{{  $totalPK['staff_target'] }}</td>
                        <td></td>
                        <td>{{  $totalPK['staff_pk'] }}</td>
                        <td>{{  $totalPK['staff_pf'] }}</td>
                        <td>{{  $totalPK['staff_pst'] }}</td>
                        <td>{{  $totalPK['staff_diff'] }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </p>
        <br/>
        <br/>
    @endif

    @if (!empty($extrajobdataobj['dataextrajob']['PK']))
        <p>
            <h3>Extra Job PK</h3>
            <table>
                <thead>
                    <tr>
                        <th>วันที่</th>
                        <th>แผนกงาน</th>
                        <th>กะ</th>
                        <th>งาน</th>
                        <th>Plan</th>
                        <th>Actual</th>
                        <th>Diff</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $totalExtJobPK = [];
                        $totalExtJobPK['plan_num'] = 0;
                        $totalExtJobPK['act_num'] = 0;
                        $totalExtJobPK['diff'] = 0;
                    @endphp
                     @foreach ($extrajobdataobj['dataextrajob']['PK'] as $logextram)
                        @foreach ($logextram->logextrads()->get() as $item)
                        <tr>
                            <td>{{  $logextram->process_date }}</td>
                            <td>{{  $logextram->dep }}</td>
                            <td>{{  $logextram->shift->name  }}</td>
                            <td>{{  $item->extrajob->name }}</td>
                            <td>{{  number_format($item->plan_num,2,".",",") }}</td>
                            <td>{{  number_format($item->act_num,2,".",",") }}</td>
                            <td>{{  number_format(($item->act_num - $item->plan_num),2,".",",") }}</td>
                            <td>{{ $item->note }}
                                                @if (!empty($item->ref_note))
                                                    <br/>{{ $item->ref_note }}
                                                @endif
                                                </td>
                        </tr>
                        @php
                             $totalExtJobPK['plan_num'] += $item->plan_num;
                        $totalExtJobPK['act_num'] += $item->act_num;
                        $totalExtJobPK['diff'] += ($item->act_num - $item->plan_num);
                        @endphp
                        @endforeach
                    @endforeach
                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>รวม</td>
                            <td>{{  number_format($totalExtJobPK['plan_num'],2,".",",") }}</td>
                            <td>{{  number_format( $totalExtJobPK['act_num'],2,".",",") }}</td>
                            <td>{{  number_format($totalExtJobPK['diff'],2,".",",") }}</td>
                        </tr>
                </tbody>
            </table>
        </p>
        <br/>
        <br/>
    @endif

    @if (!empty($extrajobdataobj['freezems']) || !empty($extrajobdataobj['logpreparems']))
        <p>
            <h3>กำลังคน PF + RTE</h3>
            <table>
                <thead>
                    <tr>
                        <th>วันที่</th>
                        <th>ส่วนงาน</th>
                        <th>กะ</th>
                        <th>เป้าพนักงาน</th>
                        <th>ผู้ดูแล</th>
                        <th>พนักงาน PF,RTE</th>
                        <th>ผู้ช่วยงานจาก PK</th>
                        <th>ผลต่างจำนวนคน</th>
                        <th>สินค้า</th>
                        <th>Plan</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $totalPFRTE = [];
                        $totalPFRTE['staff_target'] = 0;
                        $totalPFRTE['staff_pk'] = 0;
                        $totalPFRTE['staff_pf'] = 0;
                        $totalPFRTE['staff_pst'] = 0;
                        $totalPFRTE['staff_diff'] = 0;
                    @endphp
                    @if (!empty($extrajobdataobj['freezems']))
                    @foreach ($extrajobdataobj['freezems'] as $item)
                    <tr>
                        <td>{{  $item->process_date }}</td>
                        <td>งาน Freeze</td>
                        <td>{{  $item->shift_name }}</td>
                        <td>{{  $item->staff_target }}</td>
                        <td>{{  $item->staff_operate }}</td>
                        <td>{{  ($item->staff_pf + $item->staff_pst) }}</td>
                        <td>{{  $item->staff_pk }}</td>
                        <td>{{  $item->staff_diff }}</td>
                        <td>{{  $item->product_name }}</td>
                        <td>{{  number_format($item->Plan,2,".",",") }}</td>
                        <td>{{  $item->Remarks }}</td>
                    </tr>
                    @php
                        $totalPFRTE['staff_target'] += $item->staff_target;
                        $totalPFRTE['staff_pk'] += $item->staff_pk;
                        $totalPFRTE['staff_pf'] += $item->staff_pf;
                        $totalPFRTE['staff_pst'] += $item->staff_pst;
                        $totalPFRTE['staff_diff'] += $item->staff_diff;
                    @endphp
                    @endforeach    
                    @endif
                    @if (!empty($extrajobdataobj['logpreparems']))
                    @foreach ($extrajobdataobj['logpreparems'] as $item)
                    <tr>
                        <td>{{  $item->process_date }}</td>
                        <td>งานเตรียมการ</td>
                        <td>{{  $item->shift_name }}</td>
                        <td>{{  $item->staff_target }}</td>
                        <td>{{  $item->staff_operate }}</td>
                        <td>{{  ($item->staff_pf + $item->staff_pst) }}</td>
                        <td>{{  $item->staff_pk }}</td>
                        <td>{{  $item->staff_diff }}</td>
                        <td>{{  $item->pre_product }}</td>
                        <td>{{  number_format($item->Plan,2,".",",") }}</td>
                        <td>{{  $item->Remarks }}</td>
                    </tr>
                    @endforeach
                    @php
                        $totalPFRTE['staff_target'] += $item->staff_target;
                        $totalPFRTE['staff_pk'] += $item->staff_pk;
                        $totalPFRTE['staff_pf'] += $item->staff_pf;
                        $totalPFRTE['staff_pst'] += $item->staff_pst;
                        $totalPFRTE['staff_diff'] += $item->staff_diff;
                    @endphp
                    @endif
                    <tr>
                        <td></td>
                        <td>รวม</td>
                        <td></td>
                        <td>{{  $totalPFRTE['staff_target'] }}</td>
                        <td></td>
                        <td>{{  $totalPFRTE['staff_pf'] + $totalPFRTE['staff_pst'] }}</td>
                        <td>{{  $totalPFRTE['staff_pk']}}</td>
                        <td>{{  $totalPFRTE['staff_diff'] }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </p>
        <br/>
        <br/>
    @endif
    
    @if (!empty($extrajobdataobj['dataextrajob']['PF_RTE']))
        <p>
            <h3>Extra Job PF + RTE</h3>
            <table>
                <thead>
                    <tr>
                        <th>วันที่</th>
                        <th>แผนกงาน</th>
                        <th>กะ</th>
                        <th>งาน</th>
                        <th>Plan</th>
                        <th>Actual</th>
                        <th>Diff</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $totalExtJobPK = [];
                        $totalExtJobPK['plan_num'] = 0;
                        $totalExtJobPK['act_num'] = 0;
                        $totalExtJobPK['diff'] = 0;
                    @endphp
                     @foreach ($extrajobdataobj['dataextrajob']['PF_RTE'] as $logextram)
                        @foreach ($logextram->logextrads()->get() as $item)
                        <tr>
                            <td>{{  $logextram->process_date }}</td>
                            <td>{{  $logextram->dep }}</td>
                            <td>{{  $logextram->shift->name  }}</td>
                            <td>{{  $item->extrajob->name }}</td>
                            <td>{{  number_format($item->plan_num,2,".",",") }}</td>
                            <td>{{  number_format($item->act_num,2,".",",") }}</td>
                            <td>{{  number_format(($item->act_num - $item->plan_num),2,".",",") }}</td>
                            <td>{{ $item->note }}
                                                @if (!empty($item->ref_note))
                                                    <br/>{{ $item->ref_note }}
                                                @endif
                                                </td>
                        </tr>
                        @php
                             $totalExtJobPK['plan_num'] += $item->plan_num;
                        $totalExtJobPK['act_num'] += $item->act_num;
                        $totalExtJobPK['diff'] += ($item->act_num - $item->plan_num);
                        @endphp
                        @endforeach
                    @endforeach
                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>รวม</td>
                            <td>{{  number_format($totalExtJobPK['plan_num'],2,".",",") }}</td>
                            <td>{{  number_format( $totalExtJobPK['act_num'],2,".",",") }}</td>
                            <td>{{  number_format($totalExtJobPK['diff'],2,".",",") }}</td>
                            <td></td>
                        </tr>
                </tbody>
            </table>
        </p>
        <br/>
        <br/>
    @endif

    @if (!empty($extrajobdataobj['dataextrajob']['FS']))
        <p>
            <h3>Extra Job FS</h3>
            <table>
                <thead>
                    <tr>
                        <th>วันที่</th>
                        <th>แผนกงาน</th>
                        <th>กะ</th>
                        <th>งาน</th>
                        <th>Plan</th>
                        <th>Actual</th>
                        <th>Diff</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $totalExtJobPK = [];
                        $totalExtJobPK['plan_num'] = 0;
                        $totalExtJobPK['act_num'] = 0;
                        $totalExtJobPK['diff'] = 0;
                    @endphp
                     @foreach ($extrajobdataobj['dataextrajob']['FS'] as $logextram)
                        @foreach ($logextram->logextrads()->get() as $item)
                        <tr>
                            <td>{{  $logextram->process_date }}</td>
                            <td>{{  $logextram->dep }}</td>
                            <td>{{  $logextram->shift->name  }}</td>
                            <td>{{  $item->extrajob->name }}</td>
                            <td>{{  number_format($item->plan_num,2,".",",") }}</td>
                            <td>{{  number_format($item->act_num,2,".",",") }}</td>
                            <td>{{  number_format(($item->act_num - $item->plan_num),2,".",",") }}</td>
                            <td>{{ $item->note }}
                                                @if (!empty($item->ref_note))
                                                    <br/>{{ $item->ref_note }}
                                                @endif
                                                </td>
                        </tr>
                        @php
                             $totalExtJobPK['plan_num'] += $item->plan_num;
                        $totalExtJobPK['act_num'] += $item->act_num;
                        $totalExtJobPK['diff'] += ($item->act_num - $item->plan_num);
                        @endphp
                        @endforeach
                    @endforeach
                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>รวม</td>
                            <td>{{  number_format($totalExtJobPK['plan_num'],2,".",",") }}</td>
                            <td>{{  number_format( $totalExtJobPK['act_num'],2,".",",") }}</td>
                            <td>{{  number_format($totalExtJobPK['diff'],2,".",",") }}</td>
                            <td></td>
                        </tr>
                </tbody>
            </table>
        </p>
        <br/>
        <br/>
    @endif
    
</body>
</html>