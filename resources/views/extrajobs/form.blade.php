<div class="form-group col-md-3 {{ $errors->has('shift_id') ? 'has-error' : '' }}">
    <label for="shift_id" class="control-label">{{ 'กะ' }}</label>
    <select name="shift_id" class="form-control" id="shift_id" required>
        @foreach ($shiftlist as $optionKey => $optionValue)
            <option value="{{ $optionKey }}"
                {{ isset($extrajob->shift_id) && $extrajob->shift_id == $optionKey ? 'selected' : '' }}>
                {{ $optionValue }}</option>
        @endforeach
    </select>
    {!! $errors->first('shift_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-3 {{ $errors->has('dep') ? 'has-error' : '' }}">
    <label for="dep" class="control-label">{{ 'งาน' }}</label>
    <select name="dep" class="form-control" id="dep" required>
        @foreach ($deps as $optionKey => $optionValue)
            <option value="{{ $optionKey }}"
                {{ isset($extrajob->dep) && $extrajob->shift_id == $optionKey ? 'selected' : '' }}>
                {{ $optionValue }}</option>
        @endforeach
    </select>
    {!! $errors->first('dep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-3 {{ $errors->has('typegroup') ? 'has-error' : '' }}">
    <label for="typegroup" class="control-label">{{ 'ประเภท' }}</label>
    <select name="typegroup" class="form-control" id="shift_id" required>
        @foreach ($grouptypes as $optionKey => $optionValue)
            <option value="{{ $optionKey }}"
                {{ isset($extrajob->typegroup) && $extrajob->shift_id == $optionKey ? 'selected' : '' }}>
                {{ $optionValue }}</option>
        @endforeach
    </select>
    {!! $errors->first('typegroup', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group col-md-3 {{ $errors->has('status') ? 'has-error' : '' }}">
    <label for="status" class="control-label">{{ 'status' }}</label>
    <select name="status" class="form-control" id="shift_id" required>
        @foreach ($statuses as $optionKey => $optionValue)
            <option value="{{ $optionKey }}"
                {{ isset($extrajob->status) && $extrajob->shift_id == $optionKey ? 'selected' : '' }}>
                {{ $optionValue }}</option>
        @endforeach
    </select>
    {!! $errors->first('typegroup', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-md-6  {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $extrajob->name or '' }}"
        required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-md-6 {{ $errors->has('default_plan') ? 'has-error' : '' }}">
    <label for="default_plan" class="control-label">{{ 'จำนวนพื้นฐาน' }}</label>
    <input class="form-control" name="default_plan" type="number" id="default_plan" required value="{{ $extrajob->default_plan or '0' }}">
    {!! $errors->first('default_plan', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group  col-md-12 {{ $errors->has('desc') ? 'has-error' : '' }}">
    <label for="desc" class="control-label">{{ 'รายละเอียด' }}</label>
    <input class="form-control" name="desc" type="text" id="desc" value="{{ $extrajob->desc or '' }}">
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group col-md-12 ">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>