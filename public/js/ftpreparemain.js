$(document).ready(function () {
    $("#target_workhours").change(function () {
      //target_workhours 2
      sumnum();
    });

    $("#target_result").change(function () {
      //target_result 1
      sumnum();
    });
});

function sumnum(){
    var targetresult = $("#target_result").val();
    var targetworkhours = $("#target_workhours").val();
    if (
      targetresult &&
      targetworkhours &&
      targetresult > 0 &&
      targetworkhours > 0
    ) {
        var total = parseFloat(targetresult) / parseFloat(targetworkhours);
        $("#targetperhr").val(total.toFixed(2));
    } 
}
