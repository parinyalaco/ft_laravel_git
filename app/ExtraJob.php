<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraJob extends Model
{
    protected $fillable = [
        'shift_id','dep','name','typegroup','desc','default_plan','status'
    ];

    public function shift()
    {
        return $this->hasOne('App\Shift', 'id', 'shift_id');
    }
}
