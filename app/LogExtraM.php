<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogExtraM extends Model
{
    protected $fillable = [
        'process_date','shift_id','dep','note','status'
    ];

    public function shift()
    {
        return $this->hasOne('App\Shift', 'id', 'shift_id');
    }

    public function logextrads()
    {
        return $this->hasMany('App\LogExtraD', 'log_extra_m_id');
    }
}
