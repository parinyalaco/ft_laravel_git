<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Shift;
use App\LogExtraM;
use App\Mail\ExtraJobRptMail;

class GenDailyExtraJobReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gen:dailyextrajob2report {diff} {shift_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Extra Job Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '256M');

        $diff = $this->argument('diff');
        $shiftId = $this->argument('shift_id');

        $selecteddate = date('Y-m-d');
        if ($diff == 'Y') {
            $selecteddate = date('Y-m-d', strtotime("-1 days"));
        }

        $shiftObj = Shift::findOrFail($shiftId);
        // $selecteddate = '2023-09-08';

        $logextrajobms['PK'] = LogExtraM::where('process_date', $selecteddate)
            ->where('shift_id', $shiftId)
            ->where('dep', 'PK')
            ->get();
        $logextrajobms['FS'] = LogExtraM::where('process_date', $selecteddate)
            ->where('shift_id', $shiftId)
            ->where('dep', 'FS')
            ->get();
        $logextrajobms['PF_RTE'] = LogExtraM::where('process_date', $selecteddate)
            ->where('shift_id', $shiftId)
            ->whereIn('dep', array('PF','RTE'))
            ->get();

        //PK
        $logpackms = DB::table('log_pack_ms')
            ->leftjoin('log_pack_ds', 'log_pack_ms.id', '=', 'log_pack_ds.log_pack_m_id')
            ->leftjoin('methods', 'methods.id', '=', 'log_pack_ms.method_id')
            ->leftjoin('shifts', 'shifts.id', '=', 'log_pack_ms.shift_id')
            ->leftjoin('packages', 'packages.id', '=', 'log_pack_ms.package_id')
            ->leftjoin('orders', 'orders.id', '=', 'log_pack_ms.order_id')
            ->leftjoin('std_packs', 'std_packs.id', '=', 'log_pack_ms.std_pack_id')
            ->select(DB::raw("log_pack_ms.process_date,
    shifts.name as 'shiftname',
    methods.name as 'methodname',
    ISNULL(log_pack_ms.staff_target,0) as 'staff_target',
    log_pack_ms.staff_operate as 'staff_operate',
    ISNULL(log_pack_ms.staff_pk,0)  as 'staff_pk',
    ISNULL(log_pack_ms.staff_pf,0)  as 'staff_pf',
    ISNULL(log_pack_ms.staff_pst,0)  as 'staff_pst',
    (ISNULL(log_pack_ms.staff_pk,0)+ISNULL(log_pack_ms.staff_pf,0)+ISNULL(log_pack_ms.staff_pst,0)) 
    - ISNULL(log_pack_ms.staff_target,0)
    as 'staff_diff',
    packages.name as 'packagename',
    '-' as 'unit',
    ISNULL(log_pack_ms.targetperday,0) as 'Plan',
    ISNULL(sum(log_pack_ds.[output_pack]),0) as 'Actual',
    ISNULL(sum(log_pack_ds.[output_pack]),0) - ISNULL(log_pack_ms.targetperday,0) as 'diff',
    orders.order_no as 'Shipment',
    log_pack_ms.note as 'Remark'"))
            ->where('log_pack_ms.process_date', $selecteddate)
            ->where('log_pack_ms.shift_id', $shiftId)
            ->groupBy(DB::raw('log_pack_ms.process_date,
    shifts.name,methods.name,packages.name,orders.order_no,
    log_pack_ms.targetperday,packages.kgsperpack,log_pack_ms.note,
    log_pack_ms.staff_target,log_pack_ms.staff_operate,
    log_pack_ms.staff_pf,log_pack_ms.staff_pk,log_pack_ms.staff_pst'))
            ->get();


        //PF
        $logselectms = DB::table('log_select_ms')
            ->leftjoin('log_select_ds', 'log_select_ms.id', '=', 'log_select_ds.log_select_m_id')
            ->leftjoin('products', 'products.id', '=', 'log_select_ms.product_id')
            ->leftjoin('shifts', 'shifts.id', '=', 'log_select_ms.shift_id')
            ->select(DB::raw("log_select_ms.process_date,
    shifts.name as shiftname,
    '-' as jobtype,
    ISNULL(log_select_ms.staff_target,0) as staff_target,
    log_select_ms.staff_operate as staff_operate,
    ISNULL((select top 1 pk.num_pk from log_select_ds as pk where pk.log_select_m_id = log_select_ms.id order by pk.process_datetime),0)  as staff_pk,
    ISNULL((select top 1 pf.num_pf from log_select_ds as pf where pf.log_select_m_id = log_select_ms.id order by pf.process_datetime),0)   as staff_pf,
    ISNULL((select top 1 pst.num_pst from log_select_ds as pst where pst.log_select_m_id = log_select_ms.id order by pst.process_datetime),0)  as staff_pst,
    (
    (ISNULL((select top 1 pk.num_pk from log_select_ds as pk where pk.log_select_m_id = log_select_ms.id order by pk.process_datetime),0)
    +ISNULL((select top 1 pf.num_pf from log_select_ds as pf where pf.log_select_m_id = log_select_ms.id order by pf.process_datetime),0)
    +ISNULL((select top 1 pst.num_pst from log_select_ds as pst where pst.log_select_m_id = log_select_ms.id order by pst.process_datetime),0)) 
    ) - ISNULL(log_select_ms.staff_target,0) as staff_diff,
    products.name as productname,
    'kg' as unit,
    ISNULL(log_select_ms.targetperday,0) as 'Plan',
    ISNULL(sum(log_select_ds.output_kg),0) as Actual,
    ISNULL(sum(log_select_ds.output_kg),0) - ISNULL(log_select_ms.targetperday,0) as diff,
    '-'  as Shipment,
    log_select_ms.note as Remark"))
            ->where('log_select_ms.process_date', $selecteddate)
            ->where('log_select_ms.shift_id', $shiftId)
            ->groupBy(DB::raw('log_select_ms.process_date,
    shifts.name,
    log_select_ms.id,
    log_select_ms.staff_target,log_select_ms.staff_operate,
    log_select_ms.targetperday,
    log_select_ms.note,
    products.name'))
            ->get();

        //Prepare RTE log-prepare-ms
        $logpreparems = DB::table('log_prepare_ms')
            ->leftjoin('log_prepare_ds', 'log_prepare_ms.id', '=', 'log_prepare_ds.log_prepare_m_id')
            ->leftjoin('pre_prods', 'pre_prods.id', '=', 'log_prepare_ms.pre_prod_id')
            ->leftjoin('shifts', 'shifts.id', '=', 'log_prepare_ms.shift_id')
            ->select(DB::raw("log_prepare_ms.process_date,
shifts.name as shift_name,
'-' as jobtype,
ISNULL(log_prepare_ms.staff_target,0) as staff_target,
    ISNULL(log_prepare_ms.staff_operate,'-') as staff_operate,
	ISNULL(log_prepare_ms.staff_pf,0) as staff_pf,
	ISNULL(log_prepare_ms.staff_pk,0) as staff_pk,
	ISNULL(log_prepare_ms.staff_pst,0) as staff_pst,
	(ISNULL(log_prepare_ms.staff_pf,0) +
	ISNULL(log_prepare_ms.staff_pk,0) +
	ISNULL(log_prepare_ms.staff_pst,0) - ISNULL(log_prepare_ms.staff_target,0) ) as staff_diff,
	pre_prods.name as pre_product,
	isnull(log_prepare_ms.target_result,0) as 'Plan',
	isnull(sum(log_prepare_ds.output),0) as 'Actual',
	isnull(sum(log_prepare_ds.output),0) - log_prepare_ms.target_result as diff,
	'-' as Shipments,
	log_prepare_ms.note as Remarks,
	log_prepare_ms.target_workhours"))
            ->where('log_prepare_ms.process_date', $selecteddate)
            ->where('log_prepare_ms.shift_id', $shiftId)
            ->groupBy(DB::raw('log_prepare_ms.process_date,
    shifts.name,
    log_prepare_ms.id,
    log_prepare_ms.staff_target,log_prepare_ms.staff_operate,
    log_prepare_ms.target_result,
    log_prepare_ms.staff_pf,
    log_prepare_ms.staff_pk,
    log_prepare_ms.staff_pst,
    log_prepare_ms.note,
    pre_prods.name,
	log_prepare_ms.target_workhours'))
            ->get();
        
        //Prepare PF Freeze
        $freezems = DB::table('freeze_ms')
        ->leftjoin('freeze_ds', 'freeze_ms.id', '=', 'freeze_ds.freeze_m_id')
            ->leftjoin('iqf_jobs', 'iqf_jobs.id', '=', 'freeze_ms.iqf_job_id')
            ->leftjoin('shifts', 'shifts.id', '=', 'freeze_ds.shift_id')
            ->select(DB::raw("freeze_ms.process_date,
shifts.name as shift_name,
'-' as jobtype,
ISNULL(freeze_ms.staff_target,0) as staff_target,
    ISNULL(freeze_ms.staff_operate,'-') as staff_operate,
	ISNULL(freeze_ms.staff_pf,0) as staff_pf,
	ISNULL(freeze_ms.staff_pk,0) as staff_pk,
	ISNULL(freeze_ms.staff_pst,0) as staff_pst,
	(ISNULL(freeze_ms.staff_pf,0) +
	ISNULL(freeze_ms.staff_pk,0) +
	ISNULL(freeze_ms.staff_pst,0) - ISNULL(freeze_ms.staff_target,0) ) as staff_diff,
	iqf_jobs.name as product_name,
	isnull(freeze_ms.targets,0)*isnull(freeze_ms.target_hr,0) as 'Plan',
	isnull(sum(freeze_ds.output_custom1),0) as 'Actual',
	isnull(sum(freeze_ds.output_custom1),0) -( isnull(freeze_ms.targets,0)*isnull(freeze_ms.target_hr,0) )as diff,
	'-' as Shipments,
	freeze_ms.note as Remarks"))
            ->where('freeze_ms.process_date', $selecteddate)
            ->where('freeze_ds.shift_id', $shiftId)
            ->groupBy(DB::raw('freeze_ms.process_date,
    shifts.name,
    freeze_ms.id,
    freeze_ms.staff_target,freeze_ms.staff_operate,
    freeze_ms.staff_pf,
    freeze_ms.staff_pk,
    freeze_ms.staff_pst,
    freeze_ms.targets,
    freeze_ms.note,
    iqf_jobs.name,freeze_ms.target_hr'))
            ->get();

        if (!empty($logextrajobms) || !empty($logpackms) || !empty($logselectms) || !empty($logpreparems) || !empty($freezems)) {
            $ftStaff = config('myconfig.emailnewpllist');
            //$ftStaff = config('myconfig.emailtestlist');

            $mailObj['selecteddate'] = $selecteddate;
            $mailObj['shiftname'] = $shiftObj->name;

            $mailObj['dataextrajob'] = $logextrajobms;
            $mailObj['logpackms'] = $logpackms;
            $mailObj['logselectms'] = $logselectms;
            $mailObj['logpreparems'] = $logpreparems;
            $mailObj['freezems'] = $freezems;

            //var_dump($datapl);
            $mailObj['subject'] = "แผนกำลังคน วันที่ " . $selecteddate . " กะ " . $shiftObj->name;

            // Mail::to(['PKP'=>'parinya.k@lannaagro.com'])->send(new ExtraJobRptMail($mailObj));

            Mail::to($ftStaff)->send(new ExtraJobRptMail($mailObj));
        }
    }
}
