<?php

namespace App\Console\Commands;

use App\LogAlc;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendEmailLogAlc extends Command
{

    protected $signature = 'command:sendEmailLogAlc';
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
    
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));
        $perPage = 10;
        $log_alcs = LogAlc::where(DB::raw('CONVERT(date, production_date)'), $today)->orderBy('production_date', 'DESC')->get();

        $allStaff = config('myconfig.emailtestlist');


        Mail::send(
            'log-alc.emails.report',
            compact('yesterday', 'log_alcs'),
            function ($message) use ($allStaff) {
                foreach ($allStaff as $name => $email) {
                    $message->to($email, $name);
                    $message->cc($email, $name);
                }
                $message->subject('รายงาน ข้อมูลแอลกอฮอล์ และ คลอรีนที่ใช้ รายวัน ' . date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d')))));
            }
        );
        $this->info('Email sent successfully.');
    }
}
