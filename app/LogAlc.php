<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAlc extends Model
{
    protected $table = 'log_alc';

    protected $fillable = [
        'production_date',
        'onshift',
        'department',
        'note',
        'type',
        'amount_used',
        'amount_broked',
        'details',
    ];
}
