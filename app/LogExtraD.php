<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogExtraD extends Model
{
    protected $fillable = [
        'log_extra_m_id','extra_job_id','plan_num','act_num','ref_note','note'
    ];

    public function logextram()
    {
        return $this->hasOne('App\LogExtraM', 'id', 'log_extra_m_id');
    }

    public function extrajob()
    {
        return $this->hasOne('App\ExtraJob', 'id', 'extra_job_id');
    }
}
