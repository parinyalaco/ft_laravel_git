<?php

namespace App\Http\Controllers;

use App\Shift;
use App\ExtraJob;
use App\LogExtraM;
use App\LogExtraD;
use Illuminate\Http\Request;

class LogExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = 'Active';
        $keyword = $request->get('search');
        if (!empty($request->get('status'))) {
            $status = $request->get('status');
        }

        $perPage = 25;

        if (!empty($keyword)) {

            $logextrams = LogExtraM::where('note', 'like', '%' . $keyword . '%')
                    ->orderBy('process_date', 'DESC')
                    ->paginate($perPage);
            
        } else {
            $logextrams = LogExtraM::where('status', $status)->orderBy('process_date', 'DESC')->paginate($perPage);
        }

        return view('logextras.index', compact('logextrams', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $deps = config('myconfig.deps');
        $statuses = config('myconfig.statuses');
        $shiftlist = Shift::orderBy('name')->pluck('name', 'id');
        return view('logextras.create', compact('statuses', 'deps', 'shiftlist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $requestData['status'] = 'Active';

        LogExtraM::create($requestData);

        return redirect('logextras')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $logextram = LogExtraM::findOrFail($id);

        return view('logextras.show', compact('logextram'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $logextram = LogExtraM::findOrFail($id);

        $deps = config('myconfig.deps');
        $statuses = config('myconfig.statuses');
        $shiftlist = Shift::orderBy('name')->pluck('name', 'id');

        return view('logextras.edit', compact('logextram', 'deps', 'statuses', 'shiftlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $logextram = LogExtraM::findOrFail($id);
        $logextram->update($requestData);

        return redirect('logextras')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        LogExtraM::destroy($id);

        return redirect('logextras')->with('flash_message', ' deleted!');
    }

    public function createDetail($log_extra_m_id)
    {
        $logextram = LogExtraM::findOrFail($log_extra_m_id);
        $extrajobs = ExtraJob::where('status','Active')
        ->where('shift_id', $logextram->shift_id)
        ->where('dep', $logextram->dep)
        ->pluck('name', 'id');
        return view('logextras.createDetail', compact('logextram', 'extrajobs'));
    }

    public function storeDetail(Request $request, $log_extra_m_id)
    {
        $requestData = $request->all();
        //CheckDup
        $chkcount = LogExtraD::where('log_extra_m_id', $log_extra_m_id)
        ->where('extra_job_id', $requestData['extra_job_id'])
        ->count();
        if($chkcount == 0){
            $requestData['log_extra_m_id'] = $log_extra_m_id;
            LogExtraD::create($requestData);
            return redirect('logextras/' . $log_extra_m_id)->with('flash_message', ' added!');
        }else{
            return redirect('logextras/' . $log_extra_m_id)->with('flash_message', ' Duplicate Job!');
        }

        
    }

    public function editDetail($id)
    {
        $logextrad = LogExtraD::findOrFail($id);
        $logextram = $logextrad->logextram;
        $extrajobs = ExtraJob::where('status', 'Active')
        ->where('shift_id', $logextram->shift_id)
        ->where('dep', $logextram->dep)
        ->pluck('name', 'id');

        return view('logextras.editDetail', compact('logextrad', 'logextram', 'extrajobs'));
    }

    public function updateDetail(Request $request, $id)
    {
        $requestData = $request->all();
        $logextrad = LogExtraD::findOrFail($id);

        $chkcount = LogExtraD::where('log_extra_m_id', $logextrad->log_extra_m_id)
        ->where('extra_job_id', $requestData['extra_job_id'])
        ->where('id','<>', $id)
        ->count();
        if ($chkcount == 0) {
            $logextrad->update($requestData);
            return redirect('logextras/' . $logextrad->log_extra_m_id)->with('flash_message', ' updated!');
        }else{
            return redirect('logextras/' . $logextrad->log_extra_m_id)->with('flash_message', ' can\'t updated! duplicate job.');
        }

    }

    public function changestatus($log_extra_m_id)
    {
        $logextram = LogExtraM::findOrFail($log_extra_m_id);

        $status = 'Active';
        if ($logextram->status == 'Active') {
            $logextram->status = 'Closed';
            $status = 'Closed';
        } else {
            $logextram->status = 'Active';
        }
        //var_dump($logpreparem);
        $logextram->update();

        // return redirect('freeze-ms?status='. $status, compact('freezem'));
        return redirect('logextras/?status=' . $status)->with('flash_message', ' updated!');
    }

    public function deleteDetail($id, $log_extra_m_id)
    {
        LogExtraD::destroy($id);

        return redirect('logextras/' . $log_extra_m_id)->with('flash_message', ' deleted!');
    }

    public function initialDetails($log_extra_m_id)
    {
        $logextram = LogExtraM::findOrFail($log_extra_m_id);
        $extrajobs = ExtraJob::where('status', 'Active')
            ->where('typegroup', 'Main')
            ->where('shift_id', $logextram->shift_id)
            ->where('dep', $logextram->dep)
            ->get();
        return view('logextras.initialDetail', compact('logextram', 'extrajobs'));
    }

    public function storeInitialDetails(Request $request, $log_extra_m_id)
    {
        $requestData = $request->all();
        foreach ($requestData['extra_job_id'] as $jobid => $value) {
            $tmpsub['log_extra_m_id'] = $log_extra_m_id;
            $tmpsub['extra_job_id'] = $jobid;
            $tmpsub['plan_num'] = $requestData['plan_num'][$jobid];
            $tmpsub['act_num'] = $requestData['act_num'][$jobid];
            $tmpsub['note'] = $requestData['note'][$jobid];
            $tmp[] = $tmpsub;
        }
        LogExtraD::insert($tmp);

        return redirect('logextras/' . $log_extra_m_id)->with('flash_message', ' added!');
    }
}
