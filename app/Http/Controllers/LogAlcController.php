<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LogAlc;


class LogAlcController extends Controller
{
    public function index()
    {

        $perPage = 10;
        $log_alcs = LogAlc::orderBy('production_date', 'DESC')->paginate($perPage);
        return view('log-alc.index', compact('log_alcs'));
    }

    public function store(Request $request)
    {
        $status = false;
        if ($request->get('Alc_type') == 'on') {
            $status = true;
            LogAlc::create([
                'production_date' => $request->get('production_date'),
                'onshift' => $request->get('onshift'),
                'department' => $request->get('department'),
                'note' => $request->get('note'),
                'type' => 'Alc',
                'amount_used' => $request->get('Alc_amount_used'),
                'amount_broked' => $request->get('Alc_amount_broked'),
                'details' => $request->get('Alc_details'),
            ]);
        }

        if ($request->get('Cl_type') == 'on') {
            $status = true;
            LogAlc::create([
                'production_date' => $request->get('production_date'),
                'onshift' => $request->get('onshift'),
                'department' => $request->get('department'),
                'note' => $request->get('note'),
                'type' => 'Cl',
                'amount_used' => $request->get('Cl_amount_used'),
                'amount_broked' => $request->get('Cl_amount_broked'),
                'details' => $request->get('Cl_details'),
            ]);
        }

        if ($request->get('Cl_type') != 'on' and $request->get('Alc_type') != 'on') {
            $status = false;
        }

        if ($status) {
            return redirect()->back()->with('success', 'บันทึกสำเร็จ');
        }

        return redirect()->back()->with('error', 'ไม่ได้ทำการบันทึก');
    }
    public function edit($id)
    {
        $logAlc = LogAlc::find($id);
        return view('log-alc.edit', compact('logAlc'));
    }

    public function update(Request $request, $id)
    {
        $logAlc = LogAlc::find($id);
        $logAlc->update($request->all());
        return redirect()->route('log-alc.index')->with('success', 'แก้ไขสำเร็จ');
    }

    public function destroy(LogAlc $logAlc)
    {
        $logAlc->delete();

        return redirect()->back()->with('success', 'ลบสำเร็จ');
    }
}
