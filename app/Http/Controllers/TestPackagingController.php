<?php

namespace App\Http\Controllers;

use App\Models\PackPapers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\DeliveryPlanProducts;
use App\models\DeliveryPlans;
use App\Models\Package;
use App\Models\PackageType;
use App\Models\Packaging;
use App\Models\PackagingPackage;
use App\Models\PackagingPackageImgs;
use App\Models\PackPaperLots;
use App\Models\PackPaperStandards;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\StampDateFormats;
use App\Models\StampFormatDetails;
use App\Models\StampFormats;

use Illuminate\Support\Facades\Auth;

class TestPackagingController extends Controller
{
    public function products(Request $request)
    {
        $perPage = 10;
        $products = Product::orderBy('id', 'desc')->paginate($perPage);
        $packages = Package::get();
        $package_types = PackageType::get();
        $searchProduct = null;
        if ($request->get('searchProduct')) {
            $searchProduct = $request->get('searchProduct');
            $products = Product::where('name', 'LIKE', '%' . $searchProduct . '%')->orderBy('id', 'desc')->paginate($perPage);
        }
        // for create new product 
        $customers = Customer::orderBy('name', 'ASC')->pluck('name', 'id');
        $product_groups = ProductGroup::pluck('name', 'id');


        return view('TestPackages.product', compact(
            'products',
            'packages',
            'package_types',
            'searchProduct',
            'customers',
            'product_groups'
        ));
    }

    public function testsPackageByProduct($product_id, Request $request)
    {
        $product = Product::find($product_id);

        $packages = Package::get();
        $package_types = PackageType::get();
        // for create new product 
        $customers = Customer::orderBy('name', 'ASC')->pluck('name', 'id');
        $product_groups = ProductGroup::pluck('name', 'id');
        return view('TestPackages.packageByproduct', compact(
            'product',
            'packages',
            'package_types',
            'customers',
            'product_groups'
        ));
    }

    public function addNewCustomer(Request $request)
    {
        Customer::create([
            'name' => $request->get('name'),
            'desc' => $request->get('desc'),
            'status' => "Active",
        ]);

        return redirect()->back()->with('success', 'เพิ่ม Customer สำเร็จ');
    }

    public function addNewPackage($product_id, $packaging_id, $version, Request $request)
    {
        if ($version == 'new') {
            $version = "01";
        }

        if ($packaging_id == 'new') {
            $packaging = Packaging::create([
                'product_id' => $product_id,
                'version' => $version,
                'inner_weight_g' => $request->input('inner_weight_g'),
                'number_per_pack' => $request->input('outer_weight_kg'),
                'outer_weight_kg' => $request->input('number_per_pack'),
                'status' => 'Active',
            ]);

            $packaging_id = $packaging->id;
        }

        $product_name = Product::find($product_id)->value('name');

        $package = Package::create([
            'package_type_id' => $request->input('package_type_id'),
            'name' => $request->input('package-name'),
            'desc' => $request->input('desc'),
            'size' => $request->input('package-size'),
            'status' => 'Active',
            'sapnote' => $request->input('sapnote'),
        ]);

        PackagingPackage::create([
            'packaging_id' => $packaging_id,
            'package_id' => $package->id,
        ]);

        return redirect()->back()->with('success', "สร้าง package สำหรับ " . $product_name);
    }

    public function addPackagebySelect($product_id, $packaging_id, $version, Request $request) // เหมือนเป็นการเพิ่ม version ใหม่ 
    {

 
        if ($version == 'new') {
            $version = "01";
        }

        $product_name = Product::find($product_id)->value('name');

        if ($packaging_id == 'new') {
            $packaging = Packaging::create([
                'product_id' => $product_id,
                'version' => $version,
                'inner_weight_g' => $request->input('inner_weight_g'),
                'number_per_pack' => $request->input('number_per_pack'),
                'outer_weight_kg' => $request->input('outer_weight_kg'),
                'inner_weight_g_es' =>$request->input('inner_weight_g_es'),
                'status' => 'Active',
            ]);

            $packaging_id = $packaging->id;
        }

        PackagingPackage::create([
            'packaging_id' =>  $packaging_id,
            'package_id' => $request->input('package_select'),
        ]);

        return redirect()->back()->with('success', "เพิ่ม package สำหรับ " . $product_name);
    }

    public function pacgakes()
    {
    }


    public function deliveryPlan(Request $request)
    {
        $perpage = 10;
        $deliveryPlans = DeliveryPlans::orderBy('created_at', 'desc')->paginate($perpage);

        $products = Product::orderBy('name', 'asc')->pluck('name', 'id');
        $customers = Customer::orderBy('name', 'asc')->pluck('name', 'id');

        $searchDeliveryPlan = null;
        if ($request->get('searchDeliveryPlan')) {

            $searchDeliveryPlan = $request->get('searchDeliveryPlan');
            $deliveryPlans = DeliveryPlans::where('name', 'LIKE', '%' . $searchDeliveryPlan . '%')->orderBy('created_at', 'desc')->paginate($perpage);
        }

        return view('TestPackages.deliveryPlan.index', compact(
            'deliveryPlans',
            'searchDeliveryPlan',
            'products',
            'customers'
        ));
    }

    public function addTestDeliveryPlan(Request $request)
    {

        if ($request) {
            $deliveryPlan = DeliveryPlans::create([
                'customer_id' => $request->customer_id,
                'booking_no' => $request->booking_no,
                'orders' => $request->orders,
                'customer_po_no' => $request->customer_po_no,
            ]);

            foreach ($request->get('product_id') as $key => $value) {
                $product = Product::findOrFail($request->product_id[$key]);

                $tmptmpDeliveryProduct = [];
                $tmptmpDeliveryProduct['delivery_plan_id'] = $deliveryPlan->id;
                $tmptmpDeliveryProduct['product_id'] = $product->id;
                $tmptmpDeliveryProduct['product_name'] = $product->product_fac;

                $tmptmpDeliveryProduct['weight'] = $request->weight[$key];
                $quantity = floor($request->weight[$key] / $product->weight_with_bag);

                $tmptmpDeliveryProduct['quantity'] = $quantity;
                $unitName = $product->unit->name;
                $tmptmpDeliveryProduct['unit'] = $unitName;

                $tmptmpDeliveryProduct['loading_date'] = $request->loading_date[$key];
                $tmptmpDeliveryProduct['packing_date'] = date('Y-m-d');
                $tmptmpDeliveryProduct['production_date'] = $request->production_date[$key];

                DeliveryPlanProducts::create($tmptmpDeliveryProduct);
            }

            return redirect()->route('test_productByDeliveryPlan', $deliveryPlan->id)->with('success', 'เพิ่ม Delivery Plan สำเร็จ');
        }
        return redirect()->back()->with('error', 'Can not Create');
    }

    public function productByDeliveryPlan($deliveryPlan_id)
    {
        $deliveryPlan = DeliveryPlans::findOrFail($deliveryPlan_id);
        $products = Product::orderBy('name', 'asc')->pluck('name', 'id');

        $stamp_date_formats = StampDateFormats::pluck('name', 'id');

        return view('TestPackages.productByDeliveryPlan.index', compact(
            'deliveryPlan',
            'products',
            'stamp_date_formats'
        ));
    }

    public function addPackaging_package(Request $request)
    {
        $request->all();
        return redirect()->back()->with('success', 'เพิ่มรูปสำเร็จ');
    }

    public function addPackaging_package_imgs($packaging_package_id, Request $request)
    {
        $storage_path = storage_path('app/public/Packaging_package_imgs/');

        if (!file_exists($storage_path)) {
            mkdir($storage_path, 0777, true);
        }

        if ($request->hasFile('packaging_package_main_imgs')) {
            foreach ($request->file('packaging_package_main_imgs') as $key => $mainImage) {

                $newMainImageName = 'main-' . time() . $key . '.' . $mainImage->getClientOriginalExtension();
                $mainImage->move($storage_path, $newMainImageName);

                $main_img_path = "Packaging_package_imgs/" . $newMainImageName;
                $stamp_img_paht = null;
                if ($request->hasFile('packaging_package_stamp_imgs.' . $key)) {
                    $stampImage = $request->file('packaging_package_stamp_imgs.' . $key);
                    $newStampImageName = 'stamp-' . time() . $key . '.' . $stampImage->getClientOriginalExtension();
                    $stampImage->move($storage_path, $newStampImageName);
                    $stamp_img_paht = "Packaging_package_imgs/" . $newStampImageName;
                }

                PackagingPackageImgs::create([
                    'packaging_package_id' => $packaging_package_id,
                    'name' => $request->get('name')[$key],
                    'main_img_path' => $main_img_path,
                    'stamp_img_paht' => $stamp_img_paht,
                    'stamp_format' => $request->get('stamp_format')[$key],
                    'detail' => $request->get('detail')[$key],
                ]);
            }
            return redirect()->back()->with('success', 'เพิ่มรูปสำเร็จ');
        } else {
            return redirect()->back()->with('error', 'เพิ่มรูปไม่สำเร็จ');
        }
    }

    public function test_pack_paper_newStandard($deliveryPlans_id, $packaging_id, Request $request)
    {

        if ($request) {
            $mfg_data = [];
            $exp_data = [];

            if (!empty($request->get('mfg'))) {
                $mfg = $request->get('mfg');
                foreach ($mfg as $package_id => $value) {
                    $tmp = [];
                    $stamp_date_format_id = isset($request->get('mfg-stampDateFormat')[$package_id]) ? $request->get('mfg-stampDateFormat')[$package_id] : '';
                    $front_text = isset($request->get('mfg-front')[$package_id]) ? $request->get('mfg-front')[$package_id] : '';
                    $date_era_format = isset($request->get('mfg-era')[$package_id]) ? $request->get('mfg-era')[$package_id] : '';
                    $lot = isset($request->get('mfg-lot')[$package_id]) ? $request->get('mfg-lot')[$package_id] : '';

                    $tmp['stamp_format_detail_mfg_id'] = StampFormatDetails::create([
                        'stamp_date_format_id' => $stamp_date_format_id,
                        'front_text' => $front_text,
                        'date_era_format' => $date_era_format,
                        'lot' => $lot,
                    ])->id;

                    $mfg_data[$package_id] = $tmp;
                }
            }

            if (!empty($request->get('exp'))) {
                $exp = $request->get('exp');

                foreach ($exp as $package_id => $value) {
                    $tmp = [];
                    $stamp_date_format_id = isset($request->get('exp-stampDateFormat')[$package_id]) ? $request->get('exp-stampDateFormat')[$package_id] : '';
                    $front_text = isset($request->get('exp-front')[$package_id]) ? $request->get('exp-front')[$package_id] : '';
                    $date_era_format = isset($request->get('exp-era')[$package_id]) ? $request->get('exp-era')[$package_id] : '';
                    $lot = isset($request->get('exp-lot')[$package_id]) ? $request->get('exp-lot')[$package_id] : '';

                    $tmp['stamp_format_detail_exp_id'] = StampFormatDetails::create([
                        'stamp_date_format_id' => $stamp_date_format_id,
                        'front_text' => $front_text,
                        'date_era_format' => $date_era_format,
                        'lot' => $lot,
                    ])->id;

                    $exp_data[$package_id] = $tmp;
                }
            }

            $merge = [];

            foreach ($mfg_data as $package_id => $value) {

                $merge[$package_id]['stamp_format_detail_mfg_id'] = $value['stamp_format_detail_mfg_id'];
            }

            foreach ($exp_data as $package_id => $value) {
                $merge[$package_id]['stamp_format_detail_exp_id'] = $value['stamp_format_detail_exp_id'];
            }


            $stamp_format_detail_mfg_id = null;
            $stamp_format_detail_exp_id = null;

            foreach ($merge as $package_id => $value) {
                $stamp_format_detail_mfg_id = isset($value['stamp_format_detail_mfg_id']) ? $value['stamp_format_detail_mfg_id'] : null;
                $stamp_format_detail_exp_id = isset($value['stamp_format_detail_exp_id']) ? $value['stamp_format_detail_exp_id'] : null;

                $stamp_format_id = StampFormats::create([
                    'mfg_stamp_format_detail_id' => $stamp_format_detail_mfg_id,
                    'exp_stamp_format_detail_id' => $stamp_format_detail_exp_id,
                ])->id;

                Package::findOrFail($package_id)->update([
                    'stamp_format' => $stamp_format_id,
                ]);
            }

            $deliveryPlans_id = intval($deliveryPlans_id);
            $packaging_id = intval($packaging_id);
            $pack_paper_standard = PackPaperStandards::create([
                'exp_month' => $request->get('exp_month'),
                'products_per_lot' => $request->get('products_per_lot'),
                'weight_with_bag' => $request->get('weight_with_bag'),
                'cable' => !empty($request->get('cable')) ? $request->get('cable') : 'ไม่มีการรัดสาย',
                'pallet_base' => intval($request->get('pallet_low') * $request->get('pallet_height')),
                'pallet_low' => $request->get('pallet_low'),
                'pallet_height' => $request->get('pallet_height'),

            ]);


            $this->pack_paper($deliveryPlans_id, $packaging_id, $pack_paper_standard);
            return redirect()->back()->with('success', 'สร้างใบแจ้งบรรจุสำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถทำใบแจ้งบรรจุได้ !');
        }
    }

    public function pack_paper($deliveryPlans_id, $packaging_id, $pack_paper_standard)
    {

        $packaging = Packaging::findOrFail($packaging_id);

        $product_id = $packaging->product->id;

        $deliveryPlansProduct = DeliveryPlanProducts::where('delivery_plan_id', $deliveryPlans_id)->where('product_id', $product_id)->firstOrFail();

        $numberOfProducts = intval($deliveryPlansProduct->quantity);

        $productsPerLot = $pack_paper_standard->products_per_lot;

        $lastPackPaperLot = PackPaperLots::where('packaging_id', $packaging_id)
            ->where('remark', '!=', 'cancel')
            ->pluck('lot')->last();

        $pack_paper = PackPapers::create([
            'delivery_plan_product_id' => $deliveryPlansProduct->id,
            'pack_paper_standard_id' => $pack_paper_standard->id,
            'quantity' => $numberOfProducts,
            'create_by' => Auth::user()->id,
            'status' => 'Active',
        ]);



        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($lastPackPaperLot) {

            $startLot = chr(ord($lastPackPaperLot) + 1);
        } else {
            $startLot = 'A';
        }

        $numberOfLots = ceil($numberOfProducts / $productsPerLot);


        for ($i = 0; $i < $numberOfLots; $i++) {
            $index = ($i + ord($startLot) - ord('A')) % strlen($alphabet);
            $lot = $alphabet[$index];
            $remainingProducts = $numberOfProducts - ($i * $productsPerLot);
            $quantity = $remainingProducts > 0 ? min($remainingProducts, $productsPerLot) : 0;

            // $sequence = $i * $productsPerLot + 1 . '-' . min($numberOfProducts, ($i + 1) * $productsPerLot);
            $sequence =  '1 - ' . $quantity;
            $Fractional = 0;

            if ($quantity % $pack_paper_standard->pallet_base !== 0) {
                $Fractional = 1;
            }

            PackPaperLots::create([
                'pack_paper_id' => $pack_paper->id,
                'packaging_id' => $packaging_id,
                'lot' => $lot,
                'quantity_boxes' => $quantity,
                'quantity_bags' => $quantity * $packaging->number_per_pack,
                'sequence_boxes' => $sequence,
                'weight_per_piece' => null,
                'weight_per_full' => null,
                'number_of_pallets' => intval(intdiv($quantity, $pack_paper_standard->pallet_base) + $Fractional),
                'last_pallet_boxes' => $quantity % $pack_paper_standard->pallet_base,
                'mfg_date' => date('Y-m-d', strtotime($deliveryPlansProduct->production_date)),
                'remark',
            ]);
        }
    }
    public function pack_paper_doc($pack_paper_id, Request $request)
    {

        $pack_paper = PackPapers::findOrFail($pack_paper_id);
        if ($request) {
            if (!empty($request->all())) {

                $exp_date_standard = $pack_paper->pack_paper_standard->exp_month;
                foreach ($request->get('mfg_month') as $key => $mfg_month) {
                    $remark =  isset($request->get('remark')[$key]) ? $request->get('remark')[$key] : '';

                    PackPaperLots::findOrFail($key)->update([
                        'mfg_date' => date('Y-m-d', strtotime($mfg_month)),
                        'remark' => $remark,
                    ]);
                }

                $user_id = Auth::user()->id;


                return view('TestPackages.pack_paper', compact(
                    'pack_paper',
                    'pack_paper_id'
                ));
            }
        } else {
            return view('TestPackages.pack_paper', compact(
                'pack_paper',
                'pack_paper_id'
            ));
        }
    }

    public function cancel_pack_paper($pack_paper_id, Request $request)
    {
        if ($request) {
            PackPapers::findOrFail($pack_paper_id)->update([
                'cancel_by' =>  Auth::user()->id,
                'status' => 'cancel',
                'remark' => $request->get('remark')
            ]);

            PackPaperLots::where('pack_paper_id', $pack_paper_id)->update([
                'remark' => 'cancel',
            ]);

            return redirect()->back()->with('error', 'ยกเลิกใบแจ้งบรรจุเลขที่ ' . $pack_paper_id);
        }
    }
}
