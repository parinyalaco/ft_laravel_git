<?php

namespace App\Http\Controllers;

use App\ExtraJob;
use App\Shift;
use Illuminate\Http\Request;

class ExtraJobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $extrajobs = ExtraJob::latest()->paginate($perPage);
        } else {
            $extrajobs = ExtraJob::latest()->paginate($perPage);
        }

        return view('extrajobs.index', compact('extrajobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $shiftlist = Shift::orderBy('name')->pluck('name', 'id');
        $deps = config('myconfig.deps');
        $statuses = config('myconfig.statuses');
        $grouptypes = config('myconfig.grouptypes');
        return view('extrajobs.create', compact('shiftlist', 'statuses', 'grouptypes', 'deps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        
        ExtraJob::create($requestData);

        return redirect('extrajobs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $extrajob = ExtraJob::findOrFail($id);

        return view('extrajobs.show', compact('extrajob'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $extrajob = ExtraJob::findOrFail($id);
        $shiftlist = Shift::orderBy('name')->pluck('name', 'id');
        $deps = config('myconfig.deps');
        $statuses = config('myconfig.statuses');
        $grouptypes = config('myconfig.grouptypes');
        return view('extrajobs.edit', compact('extrajob', 'shiftlist', 'statuses', 'grouptypes', 'deps'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $extrajob = ExtraJob::findOrFail($id);
        $extrajob->update($requestData);

        return redirect('extrajobs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ExtraJob::destroy($id);

        return redirect('extrajobs')->with('flash_message', ' deleted!');
    }
}
