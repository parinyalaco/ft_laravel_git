<?php

namespace App\Http\Controllers\Packaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Package;
use App\Models\PackagesImgs;
use App\Models\PackageType;
use App\Models\Packaging;
use App\Models\PackagingPackage;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\StampDateFormats;
use App\Models\StampFormatDetails;
use App\Models\StampFormats;

class PackagesController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 10;
        $packages = Package::orderBy('id', 'desc')->paginate($perPage);
        $package_types = PackageType::get();
        $customers = Customer::orderBy('name', 'ASC')->pluck('name', 'id');
        $product_groups = ProductGroup::pluck('name', 'id');
        $stamp_date_formats = StampDateFormats::pluck('name', 'id');
        $searchPackage = null;

        if ($request->get('searchPackage')) {
            $searchPackage = $request->get('searchPackage');
            $packages = Package::where('name', 'LIKE', '%' . $searchPackage . '%')->orderBy('id', 'desc')->paginate($perPage);
        }

        return view('TestPackages.packages.index', compact(
            'packages',
            'packages',
            'package_types',
            'customers',
            'product_groups',
            'stamp_date_formats',
            'searchPackage'
        ));
    }
    public function view($id)
    {
        $package = Package::findOrFail($id);
        $stamp_date_formats = StampDateFormats::pluck('name', 'id');
        $package_types = PackageType::get();

        return view('TestPackages.packages.view.index', compact(
            'package',
            'package_types',
            'stamp_date_formats'
        ));
    }
    public function store(Request $request)
    {
        $stampFormatId = null;
        $mfgId = null;
        $expId = null;

        if ($request->mfg_status == 'Active') {
            $mfgId = StampFormatDetails::create([
                'stamp_date_format_id' => $request->mfg_stampDateFormat,
                'front_text' => $request->mfg_front,
                'date_era_format' => $request->mfg_era,
                'lot' => $request->mfg_lot or null,
                'status' => 'Active',
            ])->id;
        }

        if ($request->exp_status == 'Active') {
            $expId = StampFormatDetails::create([
                'stamp_date_format_id' => $request->exp_stampDateFormat,
                'front_text' => $request->exp_front,
                'date_era_format' => $request->exp_era,
                'lot' => $request->exp_lot or null,
                'status' => 'Active',
            ])->id;
        }

        $stampFormats = StampFormats::create([
            'mfg_stamp_format_detail_id' => $mfgId,
            'exp_stamp_format_detail_id' => $expId,
        ]);

        $stampFormatId = $stampFormats->id;

        Package::create([
            'package_type_id' => $request->input('package_type_id'),
            'name' => $request->input('name'),
            'desc' => $request->input('desc'),
            'size' => $request->input('size'),
            'status' => "Active",
            'stamp_format' => $stampFormatId,
            'sapnote' => $request->input('sapnote'),

        ]);

        return redirect()->back()->with('success', 'เพิ่ม Package สำเร็จ');
    }


    public function update(Request $request, $id)
    {

        // dd($request->all());
        $package = Package::find($id);
        $stampFormatId = null;

        if ($request->mfg_status == 'Active' or $request->exp_status == 'Active') {
            if (!empty($package->stamp_format)) { //$package->stamp_format is a ID.
                $stampFormats = StampFormats::findOrFail($package->stamp_format);
                $stampFormatId = $stampFormats->id;

                $mfgId = null;
                $expId = null;

                if ($request->mfg_status == 'Active') {
                    if (!empty($stampFormats->mfg)) {
                        $mfgData = StampFormatDetails::findOrFail($stampFormats->mfg->id);
                        $mfgData->update([
                            'stamp_date_format_id' => $request->mfg_stampDateFormat,
                            'front_text' => $request->mfg_front,
                            'date_era_format' => $request->mfg_era,
                            'lot' => $request->mfg_lot,
                            'status' => 'Active',
                        ]);
                        $mfgId = $stampFormats->mfg->id;
                    } else {
                        $mfgId = StampFormatDetails::create([
                            'stamp_date_format_id' => $request->mfg_stampDateFormat,
                            'front_text' => $request->mfg_front,
                            'date_era_format' => $request->mfg_era,
                            'lot' => $request->mfg_lot,
                            'status' => 'Active',
                        ])->id;
                    }
                }

                if ($request->exp_status == 'Active') {
                    if (!empty($stampFormats->exp)) {
                        $expData = StampFormatDetails::findOrFail($stampFormats->exp->id);
                        $expData->update([
                            'stamp_date_format_id' => $request->exp_stampDateFormat,
                            'front_text' => $request->exp_front,
                            'date_era_format' => $request->exp_era,
                            'lot' => $request->exp_lot,
                            'status' => 'Active',
                        ]);

                        $expId = $stampFormats->exp->id;
                    } else {
                        $expId = StampFormatDetails::create([
                            'stamp_date_format_id' => $request->exp_stampDateFormat,
                            'front_text' => $request->exp_front,
                            'date_era_format' => $request->exp_era,
                            'lot' => $request->exp_lot,
                            'status' => 'Active',
                        ])->id;
                    }
                }

                $stampFormats->update([
                    'mfg_stamp_format_detail_id' => $mfgId,
                    'exp_stamp_format_detail_id' => $expId
                ]);
            } else {

                $mfgId = null;
                $expId = null;

                if ($request->mfg_status == 'Active') {
                    $mfgId = StampFormatDetails::create([
                        'stamp_date_format_id' => $request->mfg_stampDateFormat,
                        'front_text' => $request->mfg_front,
                        'date_era_format' => $request->mfg_era,
                        'lot' => $request->mfg_lot,
                        'status' => 'Active',
                    ])->id;
                }

                if ($request->exp_status == 'Active') {
                    $expId = StampFormatDetails::create([
                        'stamp_date_format_id' => $request->exp_stampDateFormat,
                        'front_text' => $request->exp_front,
                        'date_era_format' => $request->exp_era,
                        'lot' => $request->exp_lot,
                        'status' => 'Active',
                    ])->id;
                }

                $stampFormats = StampFormats::create([
                    'mfg_stamp_format_detail_id' => $mfgId,
                    'exp_stamp_format_detail_id' => $expId,
                ]);

                $stampFormatId = $stampFormats->id;
            }
        }


        if ($package) {
            $package->update([
                'package_type_id' => $request->package_type_id,
                'name' => $request->name,
                'desc' => $request->desc,
                'size' => $request->size,
                'stamp_format' => $stampFormatId,
                'sapnote' => $request->sapnote,
            ]);
            return redirect()->back()->with('success', 'Package แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลที่ต้องการอัปเดต');
        }
    }

    public function destroy($id)
    {
        $package = Package::find($id);
        if ($package) {
            $package->delete();
            return redirect()->back()->with('success', 'Package ลบ สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลที่ต้องการ');
        }
    }


    public function storePackageImg($package_id, Request $request)
    {
        $storage_path = storage_path('app/public/package_imgs/');

        if (!file_exists($storage_path)) {
            mkdir($storage_path, 0777, true);
        }

        if ($request->hasFile('package_main_imgs')) {

            $mainImage = $request->file('package_main_imgs');
            $newMainImageName = 'main-' . time() .   '.' . $mainImage->getClientOriginalExtension();
            $mainImage->move($storage_path, $newMainImageName);

            $main_img_path = "package_imgs/" . $newMainImageName;
            $stamp_img_paht = null;

            if ($request->hasFile('package_stamp_imgs')) {
                $stampImage = $request->file('package_stamp_imgs');

                $newStampImageName = 'stamp-' . time() . '.' . $stampImage->getClientOriginalExtension();
                $stampImage->move($storage_path, $newStampImageName);
                $stamp_img_paht = "package_imgs/" . $newStampImageName;
            }

            PackagesImgs::create([
                'package_id' => $package_id,
                'name' => $request->get('name'),
                'main_img_path' => $main_img_path,
                'stamp_img_paht' => $stamp_img_paht,
                'stamp_format' => $request->get('stamp_format'),
                'detail' => $request->get('detail'),
            ]);


            return redirect()->back()->with('success', 'เพิ่มรูปสำเร็จ');
        } else {
            return redirect()->back()->with('error', 'เพิ่มรูปไม่สำเร็จ');
        }
    }

    public function updatePackageImg(Request $request, $id)
    {
        $package_img = PackagesImgs::find($id);

        if ($package_img) {

            $storage_path = storage_path('app/public/Packaging_package_imgs/');
            if (!file_exists($storage_path)) {
                mkdir($storage_path, 0777, true);
            }

            $main_img_path = $package_img->main_img_path;
            $stamp_img_paht = $package_img->stamp_img_paht;

            if ($request->hasFile('main_img_path')) {
                if (!empty($package_img->main_img_path) && file_exists(storage_path('app/public/' . $package_img->main_img_path))) {
                    @unlink(storage_path('app/public/' . $package_img->main_img_path));
                }

                $mainImage = $request->file('main_img_path');
                $newMainImageName = 'main-' . time() . '.' . $mainImage->getClientOriginalExtension();
                $mainImage->move($storage_path, $newMainImageName);

                $main_img_path = "Packaging_package_imgs/" . $newMainImageName;
            }

            if ($request->hasFile('stamp_img_paht')) {
                if (!empty($package_img->stamp_img_paht) && file_exists(storage_path('app/public/' . $package_img->stamp_img_paht))) {
                    @unlink(storage_path('app/public/' . $package_img->stamp_img_paht));
                }

                $stampImage = $request->file('stamp_img_paht');
                $newStampImageName = 'stamp-' . time() . '.' . $stampImage->getClientOriginalExtension();
                $stampImage->move($storage_path, $newStampImageName);
                $stamp_img_paht = "Packaging_package_imgs/" . $newStampImageName;
            }

            $package_img->update([
                'name' => $request->name,
                'main_img_path' => $main_img_path,
                'stamp_img_paht' => $stamp_img_paht,
                'stamp_format' => $request->stamp_format,
                'detail' => $request->detail,
            ]);

            return redirect()->back()->with('success', 'Packaging แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลที่ต้องการอัปเดต');
        }
    }

    public function deletePackageImg($id)
    {
        $package_img = PackagesImgs::find($id);

        if ($package_img) {
            if (!empty($package_img->main_img_path) && file_exists(storage_path('app/public/' . $package_img->main_img_path))) {
                @unlink(storage_path('app/public/' . $package_img->main_img_path));
            }

            if (!empty($package_img->stamp_img_paht) && file_exists(storage_path('app/public/' . $package_img->stamp_img_paht))) {
                @unlink(storage_path('app/public/' . $package_img->stamp_img_paht));
            }

            $package_img->delete();

            return redirect()->back()->with('success', 'ลบรูปภาพและข้อมูลเรียบร้อย');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลรูปภาพ');
        }
    }

    public function addNewPackage($product_id, $packaging_id, $version, Request $request)
    {
        $packagingByProduct = Packaging::where('product_id', $product_id)
            ->orderBy('version', 'asc')
            ->latest()
            ->first();

        if ($version == 'new') {
            $version = 1;
        }

        if ($packagingByProduct) {
            $version = intval($packagingByProduct->version) + 1;
        }

        if ($packaging_id == 'new') {
            $packaging = Packaging::create([
                'product_id' => $product_id,
                'version' => $version,
                'inner_weight_g' => $request->input('inner_weight_g'),
                'number_per_pack' => $request->input('number_per_pack'),
                'outer_weight_kg' => $request->input('outer_weight_kg'),
                'inner_weight_g_es' => $request->input('inner_weight_g_es'),

                'status' => 'Active',
            ]);

            $packaging_id = $packaging->id;
        }


        $stampFormatId = null;
        $mfgId = null;
        $expId = null;

        if ($request->mfg_status == 'Active') {
            $mfgId = StampFormatDetails::create([
                'stamp_date_format_id' => $request->mfg_stampDateFormat,
                'front_text' => $request->mfg_front,
                'date_era_format' => $request->mfg_era,
                'lot' => $request->mfg_lot or null,
                'status' => 'Active',
            ])->id;
        }

        if ($request->exp_status == 'Active') {
            $expId = StampFormatDetails::create([
                'stamp_date_format_id' => $request->exp_stampDateFormat,
                'front_text' => $request->exp_front,
                'date_era_format' => $request->exp_era,
                'lot' => $request->exp_lot or null,
                'status' => 'Active',
            ])->id;
        }

        $stampFormats = StampFormats::create([
            'mfg_stamp_format_detail_id' => $mfgId,
            'exp_stamp_format_detail_id' => $expId,
        ]);

        $stampFormatId = $stampFormats->id;

        $product_name = Product::find($product_id)->value('name');

        $package = Package::create([
            'package_type_id' => $request->input('package_type_id'),
            'name' => $request->input('name'),
            'desc' => $request->input('desc'),
            'size' => $request->input('size'),
            'status' => "Active",
            'stamp_format' => $stampFormatId,
            'sapnote' => $request->input('sapnote'),

        ]);

        PackagingPackage::create([
            'packaging_id' => $packaging_id,
            'package_id' => $package->id,
        ]);

        return redirect()->back()->with('success', "สร้าง package สำหรับ " . $product_name);
    }


    public function addPackagebySelect($product_id, $packaging_id, $version, Request $request) 
    {
        $packagingByProduct = Packaging::where('product_id', $product_id)
            ->orderBy('version', 'asc')
            ->latest()
            ->first();

        if ($version == 'new') {
            $version = 1;
        }

        if ($packagingByProduct) {
            $version = intval($packagingByProduct->version) + 1;
        }

        $product_name = Product::find($product_id)->value('name');

        if ($packaging_id == 'new') {
            $packaging = Packaging::create([
                'product_id' => $product_id,
                'version' => $version,
                'inner_weight_g' => $request->input('inner_weight_g'),
                'number_per_pack' => $request->input('number_per_pack'),
                'outer_weight_kg' => $request->input('outer_weight_kg'),
                'inner_weight_g_es' => $request->input('inner_weight_g_es'),
                'status' => 'Active',
            ]);

            $packaging_id = $packaging->id;
        }

        PackagingPackage::create([
            'packaging_id' =>  $packaging_id,
            'package_id' => $request->input('package_select'),
        ]);

        return redirect()->back()->with('success', "เพิ่ม package สำหรับ " . $product_name);
    }
}
