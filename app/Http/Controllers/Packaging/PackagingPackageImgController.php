<?php

namespace App\Http\Controllers\Packaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PackagingPackageImgs;

class PackagingPackageImgController extends Controller
{
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $packaging_package_img = PackagingPackageImgs::find($id);

        $storage_path = storage_path('app/public/Packaging_package_imgs/');
        if (!file_exists($storage_path)) {
            mkdir($storage_path, 0777, true);
        }

        $main_img_path = $packaging_package_img->main_img_path;
        $stamp_img_paht = $packaging_package_img->stamp_img_paht;

        if ($request->hasFile('main_img_path')) {
            if (!empty($packaging_package_img->main_img_path) && file_exists(storage_path('app/public/' . $packaging_package_img->main_img_path))) {
                @unlink(storage_path('app/public/' . $packaging_package_img->main_img_path));
            }

            $mainImage = $request->file('main_img_path');
            $newMainImageName = 'main-' . time() . '.' . $mainImage->getClientOriginalExtension();
            $mainImage->move($storage_path, $newMainImageName);

            $main_img_path = "Packaging_package_imgs/" . $newMainImageName;
        }

        if ($request->hasFile('stamp_img_paht')) {
            if (!empty($packaging_package_img->stamp_img_paht) && file_exists(storage_path('app/public/' . $packaging_package_img->stamp_img_paht))) {
                @unlink(storage_path('app/public/' . $packaging_package_img->stamp_img_paht));
            }

            $stampImage = $request->file('stamp_img_paht');
            $newStampImageName = 'stamp-' . time() . '.' . $stampImage->getClientOriginalExtension();
            $stampImage->move($storage_path, $newStampImageName);
            $stamp_img_paht = "Packaging_package_imgs/" . $newStampImageName;
        }

        $packaging_package_img->update([
            'name' => $request->name,
            'main_img_path' => $main_img_path,
            'stamp_img_paht' => $stamp_img_paht,
            'stamp_format' => $request->stamp_format,
            'detail' => $request->detail,
        ]);

        return redirect()->back()->with('success', 'Packaging แก้ไข สำเร็จ');
    }

    public function destroy($id)
    {
        $packaging_package_img = PackagingPackageImgs::find($id);
        if ($packaging_package_img) {
            if (!empty($packaging_package_img->main_img_path) && file_exists(storage_path('app/public/' . $packaging_package_img->main_img_path))) {
                @unlink(storage_path('app/public/' . $packaging_package_img->main_img_path));
            }

            if (!empty($packaging_package_img->stamp_img_paht) && file_exists(storage_path('app/public/' . $packaging_package_img->stamp_img_paht))) {
                @unlink(storage_path('app/public/' . $packaging_package_img->stamp_img_paht));
            }

            $packaging_package_img->delete();

            return redirect()->back()->with('success', 'ลบรูปภาพและข้อมูลเรียบร้อย');
        } else {
            return redirect()->back()->with('error', 'ไม่พบข้อมูลรูปภาพ');
        }
    }
}
