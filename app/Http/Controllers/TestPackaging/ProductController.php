<?php

namespace App\Http\Controllers\TestPackaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Package;
use App\Models\PackageType;
use App\Models\ProductGroup;
use App\Models\StampDateFormats;
use App\Models\Unit;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 10;
        $products = Product::orderBy('id', 'desc')->paginate($perPage);
        $packages = Package::get();
        $package_types = PackageType::get();
        $searchProduct = null;
        if ($request->get('searchProduct')) {
            $searchProduct = $request->get('searchProduct');
            $products = Product::where('name', 'LIKE', '%' . $searchProduct . '%')->orderBy('id', 'desc')->paginate($perPage);
        }
        // for create new product 
        $customers = Customer::orderBy('name', 'ASC')->pluck('name', 'id');
        $product_groups = ProductGroup::pluck('name', 'id');
        $stamp_date_formats = StampDateFormats::pluck('name', 'id');

        $units = Unit::pluck('name', 'id');


        return view('TestPackages.products.index', compact(
            'products',
            'packages',
            'package_types',
            'searchProduct',
            'customers',
            'product_groups',
            'stamp_date_formats',
            'units'
        ));
    }
    public function createProduct(Request $request)
    {
        if ($request) {
            Product::create([
                'customer_id' => $request->get('customer_id'),
                'product_group_id' => $request->get('product_group_id'),
                'product_fac' => $request->get('product_fac'),
                'name' => $request->get('name'),
                'desc' => $request->get('desc'),
                'SAP' => $request->get('SAP'),
                'shelf_life' => $request->get('shelf_life'),
                'status' => 'Active',
                'weight_with_bag' => $request->get('weight_with_bag'),
                'unit_id' => $request->get('unit_id'),
            ]);
        }
        
        return back()->with('success', 'สร้าง Product สำเร็จ');
    }

    public function update(Request $request, $id)
    {

        $product = Product::find($id);
        $product->update([
            'customer_id' => $request->customer_id,
            'product_group_id' => $request->product_group_id,
            'product_fac' => $request->product_fac,

            'name' => $request->name,
            'desc' => $request->desc,
            'SAP' => $request->SAP,
            'shelf_life' => $request->shelf_life,
            'weight_with_bag' => $request->weight_with_bag,
            'unit_id' => $request->unit_id,
        ]);

        return redirect()->back()->with('success', 'Product แก้ไข สำเร็จ');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect()->back()->with('success', 'Product ลบ สำเร็จ');
    }
}
