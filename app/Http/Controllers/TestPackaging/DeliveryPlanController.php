<?php

namespace App\Http\Controllers\TestPackaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\DeliveryPlanProducts;
use App\models\DeliveryPlans;
use App\Models\Product;
use App\Models\ProductStocks;
use App\Models\StampDateFormats;

class DeliveryPlanController extends Controller
{
    public function index()
    {
        $perpage = 10;
        $deliveryPlans = DeliveryPlans::orderBy('created_at', 'desc')
            ->paginate($perpage);

        $products = Product::orderBy('name', 'asc')
            ->get();
        $customers = Customer::orderBy('name', 'asc')
            ->pluck('name', 'id');

        $searchDeliveryPlan = null;

        return view('TestPackages.deliveryPlan.index', compact(
            'deliveryPlans',
            'searchDeliveryPlan',
            'products',
            'customers'
        ));
    }

    public function addDeliveryPlan(Request $request)
    {


        if ($request) {
            $deliveryPlan = DeliveryPlans::create([
                'customer_id' => $request->customer_id,
                'booking_no' => $request->booking_no,
                'orders' => $request->orders,
                'customer_po_no' => $request->customer_po_no,
            ]);

            foreach ($request->get('product_id') as $key => $value) {
                $product = Product::findOrFail($request->product_id[$key]);

                $tmptmpDeliveryProduct = [];
                $tmptmpDeliveryProduct['delivery_plan_id'] = $deliveryPlan->id;
                $tmptmpDeliveryProduct['product_id'] = $product->id;
                $tmptmpDeliveryProduct['product_name'] = $product->product_fac;

                $tmptmpDeliveryProduct['weight'] = $request->weight[$key];
                $quantity = floor($request->weight[$key] / $product->weight_with_bag);

                $tmptmpDeliveryProduct['quantity'] = $quantity;
                $unitName = $product->unit->name;
                $tmptmpDeliveryProduct['unit'] = $unitName;

                $tmptmpDeliveryProduct['loading_date'] = $request->loading_date[$key];
                $tmptmpDeliveryProduct['packing_date'] = date('Y-m-d');
                $tmptmpDeliveryProduct['production_date'] = $request->production_date[$key];
                $tmptmpDeliveryProduct['remark'] = $request->remark[$key];

                // ================================= Is Product Stock =================================
                $tmptmpDeliveryProduct['product_stock_id'] = null;

                if (isset($request->is_stock[$key])) {
                    $month = date('m', strtotime($request->is_stock_date[$key]));
                    $year = date('Y', strtotime($request->is_stock_date[$key]));

                    $productStock = ProductStocks::where('product_id', $product->id)
                        ->where('month', $month)
                        ->where('year', $year)
                        ->orderBy('seq', 'desc')
                        ->first();

                    $seq = $productStock ? $productStock->seq + 1 : 1;

                    $tmptmpDeliveryProduct['product_stock_id'] = ProductStocks::create([
                        'product_id' => $product->id,
                        'seq' => $seq,
                        'month' => $month,
                        'year' => $year,
                    ])->id;
                }

                DeliveryPlanProducts::create($tmptmpDeliveryProduct);
            }

            return redirect()->route('test_productByDeliveryPlan', $deliveryPlan->id)->with('success', 'เพิ่ม Delivery Plan สำเร็จ');
        }
        return redirect()->back()->with('error', 'Can not Create');
    }



    public function update(Request $request, $id)
    {


        if ($request) {
            $deliveryPlan = DeliveryPlans::find($id);
            $deliveryPlan->update($request->all());

            foreach ($request->get('product_id') as $delivery_plan_product_id => $value) {
                $delivery_plan_product = $deliveryPlan->delivery_plan_products->where('id', $delivery_plan_product_id)->first();

                if ($delivery_plan_product) {
                    $product = Product::findOrFail($value); // $value ใช้เป็น $request->product_id[$delivery_plan_product_id]

                    $tmptmpDeliveryProduct = [
                        'delivery_plan_id' => $deliveryPlan->id,
                        'product_id' => $product->id,
                        'product_name' => $product->product_fac,
                        'weight' => $request->weight[$delivery_plan_product_id],
                        'quantity' => floor($request->weight[$delivery_plan_product_id] / $product->weight_with_bag),
                        'unit' => $product->unit->name,
                        'loading_date' => $request->loading_date[$delivery_plan_product_id],
                        'packing_date' => $request->production_date[$delivery_plan_product_id],
                        'production_date' => $request->production_date[$delivery_plan_product_id],
                        'remark' => $request->remark[$delivery_plan_product_id],
                        'product_stock_id' => null,
                    ];

                    if (isset($request->is_stock[$delivery_plan_product_id])) {
                        $month = date('m', strtotime($request->is_stock_date[$delivery_plan_product_id]));
                        $year = date('Y', strtotime($request->is_stock_date[$delivery_plan_product_id]));

                        $product_stock = ProductStocks::where('product_id', $product->id)
                            ->where('month', $month)
                            ->where('year', $year)
                            ->orderBy('seq', 'desc')
                            ->first();

                        $seq = $product_stock ? $product_stock->seq + 1 : 1;

                        if ($delivery_plan_product->productStock) {
                            $delivery_plan_product->productStock->update([
                                'product_id' => $product->id,
                                'month' => $month,
                                'year' => $year,
                                'seq' => $seq,
                            ]);
                            $tmptmpDeliveryProduct['product_stock_id'] = $delivery_plan_product->productStock->id;
                        } else {
                            $tmptmpDeliveryProduct['product_stock_id'] = ProductStocks::create([
                                'product_id' => $product->id,
                                'seq' => $seq,
                                'month' => $month,
                                'year' => $year,
                            ])->id;
                        }
                    }

                    $delivery_plan_product->update($tmptmpDeliveryProduct);
                }
            }

            return redirect()->back()->with('success', 'Delivery Plan แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถ แก้ไข Delivery Plan ได้');
        }
    }

    public function destroy($id)
    {
        $deliveryPlan = DeliveryPlans::find($id);
        $deliveryPlanProduct = DeliveryPlanProducts::where('delivery_plan_id', $id);

        if ($deliveryPlanProduct->count() > 0) {
            $deliveryPlanProduct->delete();
        }
        $deliveryPlan->delete();
        return redirect()->back()->with('success', 'ลบ Delivery Plan สำเร็จ');
    }

    public function productByDeliveryPlan($deliveryPlan_id)
    {
        $deliveryPlan = DeliveryPlans::findOrFail($deliveryPlan_id);
        $products = Product::orderBy('name', 'asc')->pluck('name', 'id');

        $stamp_date_formats = StampDateFormats::pluck('name', 'id');

        return view('TestPackages.productByDeliveryPlan.index', compact(
            'deliveryPlan',
            'products',
            'stamp_date_formats'
        ));
    }
}
