<?php

namespace App\Http\Controllers\TestPackaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Unit;

class UnitController extends Controller
{
    public function index()
    {
        $units = Unit::orderBy('id', 'desc')->paginate(20);
        return view('TestPackages.units.index', compact('units'));
    }

    public function store(Request $request)
    {
        if ($request) {
            Unit::create($request->all());
            return redirect()->back()->with('success', 'สร้าง unit สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถสร้าง unit');
        }
    }

    public function update(Request $request, $id)
    {
        if ($request) {
            $unit = Unit::find($id);
            $unit->update($request->all());
            return redirect()->back()->with('success', 'Unit แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถ แก้ไข unit');
        }
    }

    public function destroy($id)
    {
        $unit = Unit::find($id);
        $unit->delete();
        return redirect()->back()->with('success', 'Unit ลบ สำเร็จ');
    }
}
