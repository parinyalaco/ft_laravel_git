<?php

namespace App\Http\Controllers\TestPackaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeliveryPlanProducts;
use App\Models\Packaging;
use App\Models\PackPaperLots;
use App\Models\PackPaperStandards;
use App\Models\PackPapers;
use Illuminate\Support\Facades\Auth;

class PackPaperPlanController extends Controller
{
    public function createPackPaper($deliveryPlanProduct_id, $packaging_id, Request $request)
    {
        if ($request) {
            $packaging = Packaging::findOrFail($packaging_id);
            $product = $packaging->product;

            $pack_paper_standard = PackPaperStandards::create([
                'exp_month' => $product->shelf_life,
                'products_per_lot' => $request->get('products_per_lot'),
                'weight_with_bag' => $product->weight_with_bag,
                'cable' => !empty($request->get('cable')) ? $request->get('cable') : 'ไม่มีการรัดสาย',
                'pallet_base' => intval($request->get('pallet_low') * $request->get('pallet_height')),
                'pallet_low' => $request->get('pallet_low'),
                'pallet_height' => $request->get('pallet_height'),
            ]);


            $this->pack_paper(intval($deliveryPlanProduct_id), $packaging, $product, $pack_paper_standard);

            return redirect()->back()->with('success', 'สร้างใบแจ้งบรรจุสำเร็จ');
            // return redirect()->route('test_pack_paper_doc', $pack_paper->id);
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถทำใบแจ้งบรรจุได้ !');
        }
    }

    public function pack_paper($deliveryPlanProduct_id, $packaging, $product, $pack_paper_standard)
    {

        $deliveryPlansProduct = DeliveryPlanProducts::where('product_id', $product->id)
            ->where('id', $deliveryPlanProduct_id)
            ->firstOrFail();

        $numberOfProducts = intval($deliveryPlansProduct->quantity);

        $productsPerLot = $pack_paper_standard->products_per_lot;

        $lastPackPaperLot = PackPaperLots::where('packaging_id', $packaging->id)
            ->where(function ($query) {
                $query->where('remark', '!=', 'cancel')
                    ->orWhereNull('remark');
            })
            // ->orderBy('lot', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->pluck('lot')
            ->last();


        $pack_paper = PackPapers::create([
            'delivery_plan_product_id' => $deliveryPlansProduct->id,
            'pack_paper_standard_id' => $pack_paper_standard->id,
            'quantity' => $numberOfProducts,
            'create_by' => Auth::user()->id,
            'status' => 'Active',
            'remark' => $deliveryPlansProduct->remark,
        ]);


        $alphabet = 'ABCDEFGHJKLMNPRSTWXYZ';

        // if ($lastPackPaperLot) {
        //     $startLot = chr(ord($lastPackPaperLot));
        // } else {
        //     $startLot = 'A';
        // }


        if ($lastPackPaperLot) {
            $startIndex = strpos($alphabet, $lastPackPaperLot);
            $startIndex = ($startIndex !== false) ? $startIndex + 1 : 0;
        } else {
            $startIndex = 0;
        }


        // $startLot = 10;
        // if ($startLot) {
        //     // $startIndex = $startIndex + 1;
        //     $sda = ($startIndex) % strlen($alphabet);
        //     $lotss = $alphabet[$sda];
        //     $quantityFirstLot = $startLot;

        //     // $sequence = $i * $productsPerLot + 1 . '-' . min($numberOfProducts, ($i + 1) * $productsPerLot);
        //     $sequence =  '1 - ' . $quantityFirstLot;
        //     $Fractional = 0;

        //     if ($quantityFirstLot % $pack_paper_standard->pallet_base !== 0) {
        //         $Fractional = 1;
        //     }

        //     PackPaperLots::create([
        //         'pack_paper_id' => $pack_paper->id,
        //         'packaging_id' => $packaging->id,
        //         'lot' => $lotss,
        //         'quantity_boxes' => $quantityFirstLot,
        //         'quantity_bags' => $quantityFirstLot * $packaging->number_per_pack,
        //         'sequence_boxes' => $sequence,
        //         'weight_per_piece' => null,
        //         'weight_per_full' => null,
        //         'number_of_pallets' => intval(intdiv($quantityFirstLot, $pack_paper_standard->pallet_base) + $Fractional),
        //         'last_pallet_boxes' => $quantityFirstLot % $pack_paper_standard->pallet_base,
        //         'mfg_date' => date('Y-m-d', strtotime($deliveryPlansProduct->production_date)),
        //         'remark',
        //     ]);

        //     $startIndex = $startIndex + 1;
        //     $numberOfProducts = $numberOfProducts - $startLot;
        // }



        $numberOfLots = ceil($numberOfProducts / $productsPerLot);



        for ($i = 0; $i < $numberOfLots; $i++) {
            // $index = ($i + ord($startLot) - ord('A')) % strlen($alphabet);
            // $lot = $alphabet[$index];

            $index = ($startIndex + $i) % strlen($alphabet);
            $lot = $alphabet[$index];

            // $test[] = $lot;
            $remainingProducts = $numberOfProducts - ($i * $productsPerLot);
            $quantity = $remainingProducts > 0 ? min($remainingProducts, $productsPerLot) : 0;

            // $sequence = $i * $productsPerLot + 1 . '-' . min($numberOfProducts, ($i + 1) * $productsPerLot);
            $sequence =  '1 - ' . $quantity;
            $Fractional = 0;

            if ($quantity % $pack_paper_standard->pallet_base !== 0) {
                $Fractional = 1;
            }

            PackPaperLots::create([
                'pack_paper_id' => $pack_paper->id,
                'packaging_id' => $packaging->id,
                'lot' => $lot,
                'quantity_boxes' => $quantity,
                'quantity_bags' => $quantity * $packaging->number_per_pack,
                'sequence_boxes' => $sequence,
                'weight_per_piece' => null,
                'weight_per_full' => null,
                'number_of_pallets' => intval(intdiv($quantity, $pack_paper_standard->pallet_base) + $Fractional),
                'last_pallet_boxes' => $quantity % $pack_paper_standard->pallet_base,
                'mfg_date' => date('Y-m-d', strtotime($deliveryPlansProduct->production_date)),
                'remark',
            ]);
        }

        return $pack_paper;
    }
    public function pack_paper_doc($pack_paper_id, Request $request)
    {

        $pack_paper = PackPapers::findOrFail($pack_paper_id);

        $exp_minusOne = [
            'FBANCCBLASE001680',
            'FBLUCCULASE001200',
            'FSTWCCSLAFM001200',
            'FBANCCBLASC000840',
            'FBANCCBTNCP003840',
            'FBANCPBTNCP003360',
            'FGSB075LEMT015000',
            'FGSB075LEMT004000',
            'FGSB075LUAE010000',
        ];

        if ($request) {
            if (!empty($request->all())) {
                foreach ($request->get('mfg_month') as $key => $mfg_month) {
                    $remark =  isset($request->get('remark')[$key]) ? $request->get('remark')[$key] : '';

                    PackPaperLots::findOrFail($key)->update([
                        'mfg_date' => date('Y-m-d', strtotime($mfg_month)),
                        'remark' => $remark,
                    ]);
                }

                $pack_paper->update(['remark' => $request->get('packPaperRemark')]);


                return view('TestPackages.PackPapers.index', compact(
                    'pack_paper',
                    'pack_paper_id',
                    'exp_minusOne'
                ));
            }
        } else {
            return view('TestPackages.PackPapers.index', compact(
                'pack_paper',
                'pack_paper_id',
                'exp_minusOne'
            ));
        }
    }
}
