<?php

namespace App\Http\Controllers\TestPackaging;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductGroup;

class ProductGroupController extends Controller
{

    public function index(Request $request)
    {
        $perPage = 20;

        $productGroups = ProductGroup::orderBy('id', 'desc')->paginate($perPage);

        $searchProductGroup = null;
        if ($request->get('searchProductGroup')) {
            $searchProductGroup = $request->get('searchProductGroup');
            $productGroups = ProductGroup::where('name', 'LIKE', '%' . $searchProductGroup . '%')->orderBy('id', 'desc')->paginate($perPage);
        }

        return view('TestPackages.productGroup.index', compact('productGroups', 'searchProductGroup'));
    }

    public function store(Request $request)
    {
        if ($request) {
            ProductGroup::create($request->all());
            return redirect()->back()->with('success', 'สร้าง Product Group สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถสร้าง Product Group');
        }
    }

    public function update(Request $request, $id)
    {
        if ($request) {
            $productGroup = ProductGroup::find($id);
            $productGroup->update($request->all());
            return redirect()->back()->with('success', 'Product Group แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถ แก้ไข Product Group');
        }
    }

    public function destroy($id)
    {
        $productGroup = ProductGroup::find($id);
        $productGroup->delete();
        return redirect()->back()->with('success', 'Product Group ลบ สำเร็จ');
    }
}
