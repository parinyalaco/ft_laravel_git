<?php

namespace App\Http\Controllers\PackPaper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Package;
use App\Models\PackageType;
use App\Models\Packaging;
use App\Models\PackagingPackage;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\StampDateFormats;
use App\Models\StampFormatDetails;
use App\Models\StampFormats;

class PackagingPackagesController extends Controller
{
    public function index(Product $product)
    {
        $package_types = PackageType::get();
        $customers = Customer::orderBy('name', 'ASC')->pluck('name', 'id');
        $product_groups = ProductGroup::pluck('name', 'id');

        $stamp_date_formats = StampDateFormats::pluck('name', 'id');
        $packages = Package::pluck('name', 'id');


        return view('packPapers.products.packagingPackages.index', compact(
            'product',
            'package_types',
            'product_groups',
            'stamp_date_formats',
            'packages'
        ));
    }

    public function addNewPackage(Product $product, Packaging $packaging)
    {
        $product_groups = ProductGroup::pluck('name', 'id');

        $stamp_date_formats = StampDateFormats::pluck('name', 'id');
        $packages = Package::pluck('name', 'id');
        $package_types = PackageType::pluck('name', 'id');

        return view('packPapers.products.packagingPackages.addNewPackage.index', compact(
            'product',
            'packaging',
            'package_types',
            'stamp_date_formats',
            'packages'

        ));
    }

    public function storeNewPackage(Product $product, Packaging $packaging, $type, Request $request)
    {
        if ($request) {
            switch ($type) {
                case 'new':
                    $stampFormatId = null;
                    $mfgId = null;
                    $expId = null;

                    if ($request->mfg_status == 'Active') {
                        $mfgId = StampFormatDetails::create([

                            'stamp_date_format_id' => $request->mfg_stampDateFormat,
                            'front_text' => $request->mfg_front,
                            'date_era_format' => $request->mfg_era,
                            'lot' => $request->mfg_back,
                            'status' => 'Active',
                        ])->id;
                    }

                    if ($request->exp_status == 'Active') {
                        $expId = StampFormatDetails::create([
                            'stamp_date_format_id' => $request->exp_stampDateFormat,
                            'front_text' => $request->exp_front,
                            'date_era_format' => $request->exp_era,
                            'lot' => $request->exp_back,
                            'status' => 'Active',
                        ])->id;
                    }

                    $stampFormats = StampFormats::create([
                        'mfg_stamp_format_detail_id' => $mfgId,
                        'exp_stamp_format_detail_id' => $expId,
                    ]);

                    $stampFormatId = $stampFormats->id;

                    $package = Package::create([
                        'package_type_id' => $request->input('package_type_id'),
                        'name' => $request->input('name'),
                        'desc' => $request->input('desc'),
                        'size' => $request->input('size'),
                        'status' => "Active",
                        'stamp_format' => $stampFormatId,
                        'sapnote' => $request->input('sapnote'),
                    ]);

                    PackagingPackage::create([
                        'packaging_id' => $packaging->id,
                        'package_id' => $package->id,
                    ]);

                    return redirect()->route('packPaper2.0.product.packagingPackaging', [$product->id, $packaging->id])->with('success', 'บันทึกสำเร็จ');

                case 'select':
                    PackagingPackage::create([
                        'packaging_id' => $packaging->id,
                        'package_id' => $request->package_select,
                    ]);

                    return redirect()->route('packPaper2.0.product.packagingPackaging', [$product->id, $packaging->id])->with('success', 'บันทึกสำเร็จ');
            }
        }
        return redirect()->route('packPaper2.0.product.packagingPackaging', [$product->id, $packaging->id])->with('error', 'ไม่สามารถบันทึกได้');
    }

    public function addNewVersion(Product $product)
    {
        $product_groups = ProductGroup::pluck('name', 'id');

        $stamp_date_formats = StampDateFormats::pluck('name', 'id');
        $packages = Package::pluck('name', 'id');
        $package_types = PackageType::pluck('name', 'id');

        return view('packPapers.products.packagingPackages.addNewVersion.index', compact(
            'product',

            'package_types',
            'stamp_date_formats',
            'packages'

        ));
    }


    public function storeNewVersion(Product $product, $type, Request $request)
    {
        $packagingByProduct = Packaging::where('product_id', $product->id)
            ->orderBy('version', 'DESC')
            ->latest()
            ->first();

        $version = 1;

        if ($packagingByProduct) {
            $version = intval($packagingByProduct->version) + 1;
        }

        $packaging = Packaging::create([
            'product_id' => $product->id,
            'version' => $version,
            'inner_weight_g' => $request->input('inner_weight_g'),
            'number_per_pack' => $request->input('number_per_pack'),
            'outer_weight_kg' => $request->input('outer_weight_kg'),
            'inner_weight_g_es' => $request->input('inner_weight_g_es'),
            'status' => 'Active',
        ]);

        switch ($type) {
            case 'new':
                $stampFormatId = null;
                $mfgId = null;
                $expId = null;

                if ($request->mfg_status == 'Active') {
                    $mfgId = StampFormatDetails::create([

                        'stamp_date_format_id' => $request->mfg_stampDateFormat,
                        'front_text' => $request->mfg_front,
                        'date_era_format' => $request->mfg_era,
                        'lot' => $request->mfg_back,
                        'status' => 'Active',
                    ])->id;
                }

                if ($request->exp_status == 'Active') {
                    $expId = StampFormatDetails::create([
                        'stamp_date_format_id' => $request->exp_stampDateFormat,
                        'front_text' => $request->exp_front,
                        'date_era_format' => $request->exp_era,
                        'lot' => $request->exp_back,
                        'status' => 'Active',
                    ])->id;
                }

                $stampFormats = StampFormats::create([
                    'mfg_stamp_format_detail_id' => $mfgId,
                    'exp_stamp_format_detail_id' => $expId,
                ]);

                $stampFormatId = $stampFormats->id;

                $package = Package::create([
                    'package_type_id' => $request->input('package_type_id'),
                    'name' => $request->input('name'),
                    'desc' => $request->input('desc'),
                    'size' => $request->input('size'),
                    'status' => "Active",
                    'stamp_format' => $stampFormatId,
                    'sapnote' => $request->input('sapnote'),
                ]);

                PackagingPackage::create([
                    'packaging_id' => $packaging->id,
                    'package_id' => $package->id,
                ]);

                return redirect()->route('packPaper2.0.product.packagingPackaging', [$product->id])->with('success', 'บันทึกสำเร็จ');

            case 'select':
                PackagingPackage::create([
                    'packaging_id' => $packaging->id,
                    'package_id' => $request->package_select,
                ]);

                return redirect()->route('packPaper2.0.product.packagingPackaging', [$product->id])->with('success', 'บันทึกสำเร็จ');
        }
    }
    public function switchVersion(Product $product, Packaging $packaging)
    {
        $packaging->status = $packaging->status === 'Active' ? 'Inactive' : 'Active';
        $packaging->save();

        return redirect()->back();
    }

    public function updateVersion(Product $product, Packaging $packaging, Request $request)
    {
        $packaging->update($request->all());
        return redirect()->back()->with('success', 'แก้ไข สำเร็จ');
    }

    public function deleteVersion(Product $product, Packaging $packaging)
    {
        foreach ($packaging->packaging_packages as $packagingPackage) {
            $packagingPackage->delete();
        }

        $packaging->delete();
        return redirect()->back()->with('success', 'ลบ สำเร็จ');
    }
    public function changePackage(Product $product, PackagingPackage $packaging_package, Request $request)
    {
        if ($request) {
            $packaging_package->update(['package_id' => $request->package]);

            return redirect()->back()->with('success', 'แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถ แก้ไข');
        }
    }

    public function deletePackage(Product $product, PackagingPackage $packaging_package)
    {
        $packaging_package->delete();
        return redirect()->back()->with('success', 'ลบ สำเร็จ');
    }
}
