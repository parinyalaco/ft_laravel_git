<?php

namespace App\Http\Controllers\PackPaper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\DeliveryPlanProducts;
use App\models\DeliveryPlans;
use App\Models\Product;
use App\Models\Packaging;
use App\Models\PackPaperStandards;
use App\Models\PackPaperLots;
use App\Models\PackPapers;
use Illuminate\Support\Facades\Auth;

class PackPaperController extends Controller
{
    public function index(Request $request)
    {
        $perpage = 10;
        $deliveryPlans = DeliveryPlans::orderBy('created_at', 'desc')->paginate($perpage);

        $products = Product::orderBy('name', 'asc')->get();
        $customers = Customer::orderBy('name', 'asc')->pluck('name', 'id');

        $searchDeliveryPlan = null;

        return view('packPapers.index', compact(
            'customers',
            'products',
            'deliveryPlans'
        ));
    }

    public function createPackPaper(DeliveryPlanProducts $deliveryPlanProduct, Packaging $packaging, $type, Request $request)
    {

        if ($request) {

            $packPaperStandard = PackPaperStandards::create([
                'exp_month' => $deliveryPlanProduct->product->shelf_life,
                'products_per_lot' => $request->get('products_per_lot'),
                'weight_with_bag' => $deliveryPlanProduct->product->weight_with_bag,
                'cable' => !empty($request->get('cable')) ? $request->get('cable') : 'ไม่มีการรัดสาย',
                'pallet_base' => intval($request->get('pallet_low') * $request->get('pallet_height')),
                'pallet_low' => $request->get('pallet_low'),
                'pallet_height' => $request->get('pallet_height'),
            ]);

            if ($type == 'auto') {
                $this->autoGenerateLotPackPaper($deliveryPlanProduct, $packaging, $packPaperStandard);
            }

            if ($type == 'manual') {
                $quantityArray = $request->quantity;
                $this->manualGenerateLotPackPaper($deliveryPlanProduct, $packaging, $packPaperStandard, $quantityArray);
            }

            return redirect()->back()->with('success', 'สร้างใบแจ้งบรรจุสำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถทำใบแจ้งบรรจุได้ !');
        }
    }

    private function autoGenerateLotPackPaper($deliveryPlanProduct, $packaging, $packPaperStandard)
    {

        $numberOfProducts = intval($deliveryPlanProduct->quantity);

        $productsPerLot = $packPaperStandard->products_per_lot;

        $lastPackPaperLot = PackPaperLots::where('packaging_id', $packaging->id)
            ->where(function ($query) {
                $query->where('remark', '!=', 'cancel')
                    ->orWhereNull('remark');
            })
            ->orderBy('created_at', 'ASC')
            ->pluck('lot')
            ->last();

        $pack_paper = PackPapers::create([
            'delivery_plan_product_id' => $deliveryPlanProduct->id,
            'pack_paper_standard_id' => $packPaperStandard->id,
            'packaging_id' => $packaging->id,
            'quantity' => $numberOfProducts,
            'create_by' => Auth::user()->id,
            'status' => 'Active',
            'remark' => $deliveryPlanProduct->remark,
        ]);

        $alphabet = 'ABCDEFGHJKLMNPRSTWXYZ';

        if ($lastPackPaperLot) {
            $startIndex = strpos($alphabet, $lastPackPaperLot);
            $startIndex = ($startIndex !== false) ? $startIndex + 1 : 0;
        } else {
            $startIndex = 0;
        }

        $numberOfLots = ceil($numberOfProducts / $productsPerLot); // จำนวน lots ทั้งหมดที่มี

        for ($i = 0; $i < $numberOfLots; $i++) {

            $index = ($startIndex + $i) % strlen($alphabet);
            $lot = $alphabet[$index];

            $remainingProducts = $numberOfProducts - ($i * $productsPerLot);
            $quantity = $remainingProducts > 0 ? min($remainingProducts, $productsPerLot) : 0;

            $sequence =  '1 - ' . $quantity;
            $Fractional = 0;

            if ($quantity % $packPaperStandard->pallet_base !== 0) {
                $Fractional = 1;
            }

            PackPaperLots::create([
                'pack_paper_id' => $pack_paper->id,
                'packaging_id' => $packaging->id,
                'lot' => $lot,
                'quantity_boxes' => $quantity,
                'quantity_bags' => $quantity * $packaging->number_per_pack,
                'sequence_boxes' => $sequence,
                'weight_per_piece' => null,
                'weight_per_full' => null,
                'number_of_pallets' => intval(intdiv($quantity, $packPaperStandard->pallet_base) + $Fractional),
                'last_pallet_boxes' => $quantity % $packPaperStandard->pallet_base,
                'mfg_date' => date('Y-m-d'),
                'remark',
            ]);
        }

        return $pack_paper;
    }

    private function manualGenerateLotPackPaper($deliveryPlanProduct, $packaging, $packPaperStandard, $quantityArray)
    {
        $pack_paper = PackPapers::create([
            'delivery_plan_product_id' => $deliveryPlanProduct->id,
            'pack_paper_standard_id' => $packPaperStandard->id,
            'packaging_id' => $packaging->id,
            'quantity' => 0,
            'create_by' => Auth::user()->id,
            'status' => 'Active',
            'remark' => $deliveryPlanProduct->remark,
        ]);

        foreach ($quantityArray as $lot => $quantity) {


            $sequence =  '1 - ' . $quantity;
            $Fractional = 0;

            if ($quantity % $packPaperStandard->pallet_base !== 0) {
                $Fractional = 1;
            }

            PackPaperLots::create([
                'pack_paper_id' => $pack_paper->id,
                'packaging_id' => $packaging->id,
                'lot' => $lot,
                'quantity_boxes' => $quantity,
                'quantity_bags' => $quantity * $packaging->number_per_pack,
                'sequence_boxes' => $sequence,
                'weight_per_piece' => null,
                'weight_per_full' => null,
                'number_of_pallets' => intval(intdiv($quantity, $packPaperStandard->pallet_base) + $Fractional),
                'last_pallet_boxes' => $quantity % $packPaperStandard->pallet_base,
                'mfg_date' => date('Y-m-d'),
                'remark',
            ]);
        }

        return $pack_paper;
    }

    public function cancelPackPaper(PackPapers $packPaper, Request $request)
    {
        if ($request) {
            $packPaper->update([
                'cancel_by' =>  Auth::user()->id,
                'status' => 'cancel',
                'remark' => $request->get('remark')
            ]);

            PackPaperLots::where('pack_paper_id', $packPaper->id)->update([
                'remark' => 'cancel',
            ]);

            return redirect()->back()->with('error', 'ยกเลิกใบแจ้งบรรจุเลขที่ ' . $packPaper->id);
        }
    }

    public function createPackPaperDoc(PackPapers $pack_paper, Request $request)
    {
        $exp_minusOne = [
            'FBANCCBLASE001680',
            'FBLUCCULASE001200',
            'FSTWCCSLAFM001200',
            'FBANCCBLASC000840',
            'FBANCCBTNCP003840',
            'FBANCPBTNCP003360',
            'FGSB075LEMT015000',
            'FGSB075LEMT004000',
            'FGSB075LUAE010000',
        ];

        $pack_paper_id = $pack_paper->id;
        if ($request) {
            if (!empty($request->all())) {
                foreach ($request->get('mfg_month') as $key => $mfg_month) {
                    $remark =  isset($request->get('remark')[$key]) ? $request->get('remark')[$key] : '';

                    PackPaperLots::findOrFail($key)->update([
                        'mfg_date' => date('Y-m-d', strtotime($mfg_month)),
                        'remark' => $remark,
                    ]);
                }

                $pack_paper->update(['remark' => $request->get('packPaperRemark')]);

                return view('packPapers.PackPapers.index', compact(
                    'pack_paper',
                    'pack_paper_id',
                    'exp_minusOne'
                ));
            }
        } else {

            return view('packPapers.PackPapers.index', compact(
                'pack_paper',
                'pack_paper_id',
                'exp_minusOne'
            ));
        }
    }
}
