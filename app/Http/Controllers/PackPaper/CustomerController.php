<?php

namespace App\Http\Controllers\PackPaper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 20;
        $customers = Customer::orderBy('id', 'desc')->paginate($perPage);
        $searchCustomer = null;
        if ($request->get('searchCustomer')) {
            $searchCustomer = $request->get('searchCustomer');
            $customers = Customer::where('name', 'LIKE', '%' . $searchCustomer . '%')->orderBy('id', 'desc')->paginate($perPage);
        }


        return view('packPapers.customers.index', compact('customers', 'searchCustomer'));
    }

    public function store(Request $request)
    {
        if ($request) {
            Customer::create($request->all());
            return redirect()->back()->with('success', 'สร้าง Customer สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถสร้าง Customer');
        }
    }

    public function update(Request $request, Customer $customer)
    {
        if ($request) {
            $customer->update($request->all());
            return redirect()->back()->with('success', 'Customer แก้ไข สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถ แก้ไข Customer');
        }
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect()->back()->with('success', 'Customer ลบ สำเร็จ');
    }
}
