<?php

namespace App\Http\Controllers\PackPaper;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Package;
use App\Models\PackageType;
use App\Models\ProductGroup;
use App\Models\StampDateFormats;
use App\Models\Unit;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 10;
        $products = Product::orderBy('id', 'desc')
            ->paginate($perPage);
        $searchProduct = null;
        if ($request->get('searchProduct')) {
            $searchProduct = $request->get('searchProduct');
            $products = Product::where('name', 'LIKE', '%' . $searchProduct . '%')
                ->orderBy('id', 'desc')
                ->paginate($perPage);
        }
        return view('packPapers.products.index', compact(
            'products',
            'searchProduct',

        ));
    }

    public function create()
    {
        $customers = Customer::orderBy('name', 'ASC')
            ->pluck('name', 'id');

        $packages = Package::get();
        $stamp_date_formats = StampDateFormats::pluck('name', 'id');

        $product_groups = ProductGroup::pluck('name', 'id');
        $units = Unit::pluck('name', 'id');


        return view('packPapers.products.create.index', compact(
            'customers',
            'packages',
            'stamp_date_formats',
            'product_groups',
            'units'
        ));
    }

    public function store(Request $request)
    {

        if ($request) {
            Product::create([
                'customer_id' => $request->get('customer_id'),
                'product_group_id' => $request->get('product_group_id'),
                'product_fac' => $request->get('product_fac'),
                'name' => $request->get('name'),
                'desc' => $request->get('desc'),
                'SAP' => $request->get('SAP'),
                'shelf_life' => $request->get('shelf_life'),
                'status' => 'Active',
                'weight_with_bag' => $request->get('weight_with_bag'),
                'unit_id' => $request->get('unit_id'),
            ]);
        }

        return redirect()->route('packPaper2.0.product')->with('success', 'สร้าง Product สำเร็จ');
    }

    public function edit(Product $product)
    {

        $customers = Customer::orderBy('name', 'ASC')
            ->pluck('name', 'id');

        $packages = Package::get();
        $stamp_date_formats = StampDateFormats::pluck('name', 'id');

        $product_groups = ProductGroup::pluck('name', 'id');
        $units = Unit::pluck('name', 'id');


        return view('packPapers.Products.edit.index', compact(
            'customers',
            'packages',
            'stamp_date_formats',
            'product_groups',
            'units',
            'product'
        ));
    }
    public function update(Request $request, Product $product)
    {

        $product->update([
            'customer_id' => $request->customer_id,
            'product_group_id' => $request->product_group_id,
            'product_fac' => $request->product_fac,

            'name' => $request->name,
            'desc' => $request->desc,
            'SAP' => $request->SAP,
            'shelf_life' => $request->shelf_life,
            'weight_with_bag' => $request->weight_with_bag,
            'unit_id' => $request->unit_id,
        ]);

        return redirect()->route('packPaper2.0.product')->with('success', 'Product แก้ไข สำเร็จ');
    }

    public function destroy(Product $product)
    {

        $product->delete();
        return redirect()->back()->with('success', 'Product ลบ สำเร็จ');
    }
}
