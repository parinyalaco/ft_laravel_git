<?php

namespace App\Http\Controllers\PackPaper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Unit;

class UnitController extends Controller
{
    public function index(Request $request)
    {
        $perPage = 20;
        $units = Unit::orderBy('id', 'desc')->paginate($perPage);
        $searchTxt = null;
        if ($request->get('searchTxt')) {
            $searchTxt = $request->get('searchTxt');
            $units = Unit::where('name', 'LIKE', '%' . $searchTxt . '%')
                ->orderBy('id', 'desc')
                ->paginate($perPage);
        }

        return view('packPapers.units.index', compact(
            'units',
            'searchTxt'
        ));
    }

    public function store(Request $request)
    {
        if ($request) {
            Unit::create($request->all());
            return redirect()->back()->with('success', 'บันทึก สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถสร้าง unit');
        }
    }

    public function update(Request $request, Unit $unit)
    {
        if ($request) {

            $unit->update($request->all());
            return redirect()->back()->with('success', 'บันทึก สำเร็จ');
        } else {
            return redirect()->back()->with('error', 'ไม่สามารถ แก้ไข unit');
        }
    }

    public function destroy(Unit $unit)
    {
        $unit->delete();
        return redirect()->back()->with('success', 'ลบ สำเร็จ');
    }
}
