<?php

namespace App\Http\Controllers\PackPaper;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DeliveryPlanProducts;
use App\models\DeliveryPlans;
use App\Models\Product;
use App\Models\ProductStocks;
use App\Models\PackPaperLots;


class DeliveryPlanController extends Controller
{
    public function store(Request $request)
    {
        if ($request) {
            $deliveryPlan = DeliveryPlans::create([
                'customer_id' => $request->customer_id,
                'booking_no' => $request->booking_no,
                'orders' => $request->orders,
                'customer_po_no' => $request->customer_po_no,
            ]);


            $product = Product::findOrFail($request->product_id);

            $tmptmpDeliveryProduct = [];
            $tmptmpDeliveryProduct['delivery_plan_id'] = $deliveryPlan->id;
            $tmptmpDeliveryProduct['product_id'] = $product->id;
            $tmptmpDeliveryProduct['product_name'] = $product->product_fac;

            $tmptmpDeliveryProduct['weight'] = $request->weight;
            $quantity = floor($request->weight / $product->weight_with_bag);

            $tmptmpDeliveryProduct['quantity'] = $quantity;
            $unitName = $product->unit->name;
            $tmptmpDeliveryProduct['unit'] = $unitName;

            $tmptmpDeliveryProduct['loading_date'] = $request->loading_date;
            $tmptmpDeliveryProduct['packing_date'] = date('Y-m-d');
            $tmptmpDeliveryProduct['production_date'] = $request->production_date;
            $tmptmpDeliveryProduct['remark'] = $request->remark;

            // ================================= Is Product Stock =================================
            $tmptmpDeliveryProduct['product_stock_id'] = null;

            if (isset($request->is_stock)) {
                $month = date('m', strtotime($request->is_stock_date));
                $year = date('Y', strtotime($request->is_stock_date));

                $productStock = ProductStocks::where('product_id', $product->id)
                    ->where('month', $month)
                    ->where('year', $year)
                    ->orderBy('seq', 'desc')
                    ->first();

                $seq = $productStock ? $productStock->seq + 1 : 1;

                $tmptmpDeliveryProduct['product_stock_id'] = ProductStocks::create([
                    'product_id' => $product->id,
                    'seq' => $seq,
                    'month' => $month,
                    'year' => $year,
                ])->id;
            }

            DeliveryPlanProducts::create($tmptmpDeliveryProduct);


            return redirect()->route('packPaper2.0.deliveryPlan.view', $deliveryPlan->id)->with('success', 'เพิ่ม Delivery Plan สำเร็จ');
        }
        return redirect()->back()->with('error', 'Can not Create');
    }

    public function view(DeliveryPlans $deliveryPlan)
    {
        $alphabet = 'ABCDEFGHJKLMNPRSTWXYZ';

        $quantity = intval($deliveryPlan->delivery_plan_product->quantity); // จำนวนเริ่มต้น
        $startLot = 'A';
        if ($deliveryPlan->delivery_plan_product->product->packaging) {
            $lastPackPaperLot = PackPaperLots::where('packaging_id', $deliveryPlan->delivery_plan_product->product->packaging->id)
                ->where(function ($query) {
                    $query->where('remark', '!=', 'cancel')
                        ->orWhereNull('remark');
                })->orderBy('created_at', 'ASC')->pluck('lot')->last();

            // dd($deliveryPlan->delivery_plan_product->product->packaging->id);

            if (isset($lastPackPaperLot)) {
                $startLot = $lastPackPaperLot;
            }

            $index = strpos($alphabet, $startLot); // หาตำแหน่งของตัวอักษรเริ่มต้นใน alphabet

            return view('packPapers.deliveryplan.index', compact(
                'deliveryPlan',
                'quantity',
                'startLot',
                'alphabet',
                'index'
            ));
        } else {
            return redirect()
                ->route('packPaper2.0.product.packagingPackaging.addNewVersion', [$deliveryPlan->delivery_plan_product->product->id])
                ->with('error', 'สร้าง version ของ Product นี้  ' . $deliveryPlan->delivery_plan_product->product->name);
        }
    }

    public function destroy(DeliveryPlans $deliveryPlan)
    {
        $deliveryPlanProducts = DeliveryPlanProducts::where('delivery_plan_id', $deliveryPlan->id)->get();

        if ($deliveryPlanProducts->count() > 0) {
            foreach ($deliveryPlanProducts as $deliveryPlanProduct) {
                // ลบ pack_paper_standard ที่เกี่ยวข้อง
                if ($deliveryPlanProduct->pack_paper->pack_paper_standard) {
                    $deliveryPlanProduct->pack_paper->pack_paper_standard->delete();
                }

                // ลบ pack_paper_lots ที่เกี่ยวข้อง
                if ($deliveryPlanProduct->pack_paper->pack_paper_lots->count() > 0) {
                    foreach ($deliveryPlanProduct->pack_paper->pack_paper_lots as $packPaperLot) {
                        $packPaperLot->delete();
                    }
                }

                // ลบ pack_paper
                if ($deliveryPlanProduct->pack_paper) {
                    $deliveryPlanProduct->pack_paper->delete();
                }

                // ลบ DeliveryPlanProduct
                $deliveryPlanProduct->delete();
            }
        }

        // ลบ DeliveryPlan
        $deliveryPlan->delete();

        return redirect()->back()->with('success', 'ลบ สำเร็จ');
    }
}
