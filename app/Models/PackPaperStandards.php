<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackPaperStandards extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'pack_paper_standards';

    protected $fillable = [
        'exp_month',
        'products_per_lot',
        'weight_with_bag',
        'cable',
        'pallet_base',
        'pallet_low',
        'pallet_height',
        'stamp_format_id'
    ];

    public function packPaper()
    {
        return $this->belongsTo('App\Models\PackPapers', 'id', 'pack_paper_standard_id');
    }

    public function stamp_format()
    {
        return $this->hasOne('App\Models\StampFormats', 'id', 'stamp_format_id');
    }
}
