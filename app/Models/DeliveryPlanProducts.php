<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryPlanProducts extends Model
{
    protected $connection = 'sqlpackagesrv';

    protected $table = 'delivery_plan_products';

    protected $fillable = [
        'delivery_plan_id',
        'product_id',
        'product_name',
        'weight',
        'quantity',
        'unit',
        'remark',
        'product_stock_id',
        'loading_date',
        'packing_date',
        'production_date',
    ];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function delivery_plan()
    {
        return $this->belongsTo('App\Models\DeliveryPlans', 'delivery_plan_id');
    }

    public function pack_paper()
    {
        return $this->hasOne('App\Models\PackPapers', 'delivery_plan_product_id', 'id');
    }

    public function productStock()
    {
        return $this->hasOne('App\Models\ProductStocks', 'id', 'product_stock_id');
    }
}
