<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStocks extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'product_stocks';
    protected $fillable = [
        'product_id',
        'seq',
        'month',
        'year',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'product_id');
    }

    public function deliveryPlanProduct()
    {
        return $this->belongsTo('App\Models\DeliveryPlanProducts');
    }
}
