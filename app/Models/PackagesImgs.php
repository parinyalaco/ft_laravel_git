<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackagesImgs extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'packages_imgs';

    protected $fillable = [
        'package_id',
        'name',
        'main_img_path',
        'stamp_img_paht',
        'stamp_format',
        'detail',
    ];

    public function package()
    {
        return $this->belongsTo('App\Models\Package', 'package_id');
    }
}
