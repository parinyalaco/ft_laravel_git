<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PackagingPackage extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $fillable = ['packaging_id', 'package_id'];


    public function packaging()
    {
        return $this->belongsTo('App\Models\Packaging', 'packaging_id');
    }

    public function package()
    {
        return $this->belongsTo('App\Models\Package', 'package_id');
    }

    public function packaging_package_imgs()
    {
        return $this->hasMany('App\Models\PackagingPackageImgs', 'packaging_package_id');
    }
}
