<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampDateFormats extends Model
{
    protected $connection = 'sqlpackagesrv';

    protected $table = 'stamp_date_formats';

    protected $fillable = [
        'stamp_date_format',
        'name'
    ];

    public function stampFormatDetails()
    {
        return $this->hasMany('App\Models\StampFormatDetails', 'stamp_date_format_id');
    }
}
