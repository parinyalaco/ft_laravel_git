<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'sqlpackagesrv';

    protected $fillable = [
        'customer_id',
        'product_group_id',
        'product_fac',
        'name',
        'desc',
        'SAP',
        'shelf_life',
        'status',
        'weight_with_bag',
        'unit_id'

    ];

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function productgroup()
    {
        return $this->hasOne('App\Models\ProductGroup', 'id', 'product_group_id');
    }

    public function packagings()
    {
        return $this->hasMany('App\Models\Packaging', 'product_id');
    }

    public function packaging()
    {
        return $this->hasOne('App\Models\Packaging', 'product_id', 'id');
    }

    public function unit()
    {
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }
}
