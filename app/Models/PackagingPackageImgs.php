<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackagingPackageImgs extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'packaging_package_imgs';

    protected $fillable = [
        'packaging_package_id',
        'name',
        'main_img_path',
        'stamp_img_paht',
        'stamp_format',
        'detail',
    ];

    public function packaging_package()
    {
        return $this->belongsTo('App\Models\PackagingPackage', 'packaging_package_id');
    }
}
