<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampFormats extends Model
{
    protected $connection = 'sqlpackagesrv';

    protected $table = 'stamp_formats';

    protected $fillable = [
        'mfg_stamp_format_detail_id',
        'exp_stamp_format_detail_id',
    ];

    public function mfg()
    {
        return $this->hasOne('App\Models\StampFormatDetails', 'id', 'mfg_stamp_format_detail_id');
    }

    public function exp()
    {
        return $this->hasOne('App\Models\StampFormatDetails', 'id', 'exp_stamp_format_detail_id');
    }
}
