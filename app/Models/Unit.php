<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'units';

    protected $fillable = [
        'name',
        'desc'
    ];
}
