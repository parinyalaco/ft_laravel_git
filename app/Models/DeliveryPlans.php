<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class DeliveryPlans extends Model
{
    protected $connection = 'sqlpackagesrv';

    protected $table = 'delivery_plans';

    protected $fillable = [
        'customer_id',
        'booking_no',
        'orders',
        'customer_po_no',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public function delivery_plan_products()
    {
        return $this->hasMany('App\Models\DeliveryPlanProducts', 'delivery_plan_id');
    }
    public function delivery_plan_product()
    {
        return $this->hasOne('App\Models\DeliveryPlanProducts', 'delivery_plan_id', 'id');
    }
}
