<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackPapers extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'pack_papers';

    protected $fillable = [
        'delivery_plan_product_id',
        'pack_paper_standard_id',
        'packaging_id',
        'quantity',
        'create_by',
        'cancel_by',
        'status',
        'remark'
    ];

    public function pack_paper_standard()
    {
        return $this->belongsTo('App\Models\PackPaperStandards', 'pack_paper_standard_id');
    }

    public function pack_paper_lots()
    {
        return $this->hasMany('App\Models\PackPaperLots', 'pack_paper_id');
    }

    public function delivery_plan_product()
    {
        return $this->hasOne('App\Models\DeliveryPlanProducts', 'id', 'delivery_plan_product_id');
    }
 
    public function packaging()
    {
        return $this->hasOne('App\Models\Packaging', 'id', 'packaging_id');
    }
}
