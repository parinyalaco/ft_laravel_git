<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampFormatDetails extends Model
{
    protected $connection = 'sqlpackagesrv';

    protected $table = 'stamp_format_details';

    protected $fillable = [
        'stamp_date_format_id',
        'front_text',
        'date_era_format',
        'lot',
        'status'
    ];

    public function stamp_format()
    {
        return $this->hasOne('App\Models\StampFormats', 'id', 'stamp_format_id');
    }

    public function stamp_date_format()
    {
        return $this->hasOne('App\Models\StampDateFormats', 'id', 'stamp_date_format_id');
    }
}
