<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackPaperLots extends Model
{
    protected $connection = 'sqlpackagesrv';
    protected $table = 'pack_paper_lots';

    protected $fillable = [
        'pack_paper_id',
        'packaging_id',
        'lot',
        'quantity_boxes',
        'quantity_bags',
        'sequence_boxes',
        'weight_per_piece',
        'weight_per_full',
        'number_of_pallets',
        'last_pallet_boxes',
        'remark',
        'mfg_date',
    ];

    public function pack_papers()
    {
        return $this->belongsTo('App\Models\PackPapers', 'id', 'pack_paper_id');
    }
}
