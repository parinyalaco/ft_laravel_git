<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExtraJobRptMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $extrajobdataobj;
    public function __construct($obj)
    {
        $this->extrajobdataobj = $obj;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        ini_set('memory_limit', '256M');
        $extrajobdataobj = $this->extrajobdataobj;

        return $this->view('emails.ftextrajobdatamail', compact('extrajobdataobj'))->subject($this->extrajobdataobj['subject']);
    }
}
